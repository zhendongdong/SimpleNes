#include "common.h"
#include "NesPPU.h"
#include "Rom.h"
#include <cassert>
#include "Famicom.h"
#include "Devices.h"

#ifndef max
#define max(a, b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a, b)            (((a) < (b)) ? (a) : (b))
#endif

NesPPU::NesPPU(Famicom* fc) {
    this->famicom = fc;
    this->mPPUMask |= BGEN;
    this->mPPUMask |= SPEN;
    //初始化为13号颜色，通常是黑色
    memset(mScreenMemory, 13, sizeof(mScreenMemory));
}



uint8_t NesPPU::RegReadViaCpu(uint16_t address) {
    //if(address != 0x2002)
    //DebugPrintf("regReadViaCpu %x \n", address);
    uint8_t data = 0;
    if (address < 0x4000) {
        switch (address & 0x7) {
        case 0:
        case 1:
        case 3:
        case 5:
        case 6:
            //因为“open bus”，即便是只写寄存器，也会返回上次写入的数据
            data = mPreviousData;
            break;
        case 2:
            data = this->mPPUStatus | (mPreviousData & 0x1F);
            mPPUStatus &= ~PPU_VBLANK;
            mRegs.w = 0;//重置双写切换
            break;
        case 4:
            data = this->mOAM[mOamaddr];
            break;
        case 7:
            data = ReadByteViaCPU(mRegs.v & 0x3FFF);
            mPreviousData = data;
            mRegs.v += ((mPPUCtrl & VINC32) ? 32 : 1);
            break;
        }
    }
    return data;
}

void NesPPU::RegWriteViaCpu(uint16_t address, uint8_t data) {
    //mCache = data;
    if (address < 0x4000) {
        //if(address != 2007)
        mPreviousData = data;
        switch (address & 0x7) {
        case 0:
            //如果当前 VBANK 置1且要求触发一次NMI 中断并且上次尚未触发
            if ((mPPUStatus & PPU_VBLANK) && (data & 0x80) && ((mPPUCtrl & NMI_EN) == 0)) {
                //这里写触发一次中断
                famicom->GetDevices()->cpu()->TryNmi();
            }
            //t: ...GH.. ........ <- d: ......GH
            //   <used elsewhere> < -d: ABCDEF..

            mPPUCtrl = data;

            mRegs.t = (mRegs.t & 0xF3FF) | ((data & 0x03) << 10);

            break;
        case 1:
            mPPUMask = data;
            break;
        case 3:
            mOamaddr = data;
            break;
        case 4:
            mOAM[mOamaddr++] = data;
            break;
        case 5:
            if (mRegs.w == 0) {
                /*
                * 引用自NESDEV:
                t: ....... ...ABCDE <- d: ABCDE...
                x:              FGH <- d: .....FGH
                w:                  <- 1
                */
                mRegs.t = (mRegs.t & 0xFFE0) | ((uint16_t)data >> 3);
                mRegs.x = data & 0x07;

            }
            else {
                /*
                * * 引用自NESDEV:
                t: FGH..AB CDE..... <- d: ABCDEFGH
                w:                  <- 0
                */
                mRegs.t = (mRegs.t & 0x8fff) |
                    (((uint16_t)data & 0x07) << 12);//0x8fff  1000 1111 1111 1111
                mRegs.t = (mRegs.t & 0xfc1f) |
                    (((uint16_t)data & 0xF8) << 2);//0xfc1f  1111 1100 0001 1111
            }
            mRegs.w ^= 1; //双写寄存器取反
            break;
        case 6:
            if (mRegs.w == 0) {
                /*
                t: .CDEFGH ........ <- d: ..CDEFGH
                       <unused>     <- d: AB......
                t: Z...... ........ <- 0 (bit Z is cleared)
                w:                  <- 1
                */
                mRegs.t = (mRegs.t & 0x00FF) | (((uint16_t)data & 0x3F) << 8);
            }
            else {
                /*
                t: ....... ABCDEFGH <- d: ABCDEFGH
                v: <...all bits...> <- t: <...all bits...>
                w:                  <- 0
                */
                mRegs.t = (mRegs.t & 0xFF00) | data;
                mRegs.v = mRegs.t;
            }
            mRegs.w ^= 1; //取反
            break;
        case 7:
            WriteByte(mRegs.v & 0x3FFF, data);
            mRegs.v += ((mPPUCtrl & VINC32) ? 32 : 1);
            break;
        }
    }
    else if (address == 0x4014) {
        const uint16_t src = (data << 8) & 0xff00;
        famicom->GetDevices()->bus()->CpuMemcpy(mOAM, src, 256);
        //https://wiki.nesdev.com/w/index.php?title=PPU_OAM
        //OAM复制需要有513个CPU周期，如果是奇数CPU周期则+1
        //相比之下，展开 LDA/STA 循环通常需要四倍的时间
        famicom->mCpuCycleCount += 513;
        famicom->mCpuCycleCount += famicom->mCpuCycleCount & 1;

    }
}


inline void NesPPU::WriteByte(uint16_t address, uint8_t data) {
    if (address < 0x2000)
        famicom->GetDevices()->cartridge()->WriteViaPPU(address, data);
    else if (address < 0x3F00)
        mNametables[(address >> 10) & 3][address & 0x3ff] = data;
    else {
        if (address & 0x03) {
            mPalette[address & 0x1f] = data;
        }
        else {
            uint16_t offset = address & 0x0f;
            mPalette[offset] = data;
            mPalette[offset | 0x10] = data;
        }
    }
}

inline uint8_t NesPPU::ReadByte(uint16_t address) {
    if (address < 0x2000)
        return famicom->GetDevices()->cartridge()->ReadViaPPU(address);
    else if (address < 0x3F00)
        return mNametables[(address >> 10) & 3][address & 0x3ff];
    else //调色板镜像
        return mPalette[address & 0x1f];
}


void NesPPU::Reset() {
}


uint8_t* NesPPU::GetCanvas() {
    return this->mScreenMemory;
}

#define MASTER_CYCLE_PER_SCANLINE 1364 //每个扫描线所使用的主时钟数
#define MASTER_CYCLE_PER_CPU 12 //CPU一个周期所使用的时钟数（相当于主时钟）
#define MASTER_CYCLE_PER_PPU 4 //CPU一个周期所使用的时钟数（相当于主时钟）

/// <summary>
/// 根据https://wiki.nesdev.com/w/index.php?title=PPU_scrolling
/// 描述编写 我看不懂，但我大受震撼！
/// 如果启用渲染，精细 Y 将在每个扫描线的点 256 上增量，
/// 溢出到粗糙 Y，最后调整为垂直包裹在名称表之间。
/// 位12 - 14是精细Y. 位5 - 9是粗糙Y.位11选择垂直名称表
/// </summary>
void NesPPU::IncVerticalPosition() {
    //抄自NESDEV 哈哈哈
    if ((mRegs.v & 0x7000) != 0x7000) {
        mRegs.v += 0x1000;
    }
    else {
        mRegs.v &= 0x8FFF;
        uint16_t y = (mRegs.v & 0x03E0) >> 5;
        if (y == 29) {
            y = 0;
            mRegs.v ^= 0x0800;
        }
        else if (y == 31)
            y = 0;
        else
            y++;
        // put coarse Y back into v
        mRegs.v = (mRegs.v & 0xFC1F) | (y << 5);
    }
}


 void NesPPU::IncHorizontalPosition() {
    if ((mRegs.v & 0x001F) == 31) {
        mRegs.v &= ~0x001F;
        mRegs.v ^= 0x0400;
    }
    else {
        mRegs.v += 1;
    }
}


/// <summary>
/// 到达下一个磁贴时，需要增加v的粗X组件。
/// 位 0-4 是增量的，溢出拖拉位 10。
/// 这意味着位 0-4 在单个名称表中从 0 到 31 计数，
/// 并且位 10 水平选择当前名称可选。
/// </summary>
 void NesPPU::CopyHorizontalBits() {
    // v: .....F.. ...EDCBA = t: .....F.. ...EDCBA
    mRegs.v = (mRegs.v & 0xFBE0) | (mRegs.t & 0x041F);
}


/// <summary>
/// 在预渲染扫描线的点 280 到 304 期间 （vblank 结束）
/// 如果启用渲染，在 vblank 结束时，在水平位从t复制到v点 257 后不久，
/// PPU 将反复复制从t到v从点 280 到 304的垂直位，完成 v 从t：
/// </summary>
void NesPPU::CopyVerticalBits() {
    // v: GHIA.BC DEF..... <- t: GHIA.BC DEF.....
    mRegs.v = (mRegs.v & 0x841F) | (mRegs.t & 0x7BE0);
}



/// <summary>
/// 扫描当前扫描线上的精灵
/// </summary>
/// <param name="line"></param>
void NesPPU::ScanSpirits(uint16_t line) {
    //TODO: 这种模式效率太低 有时间优化下 先凑合
    mPPUStatus &= ~PPU_SPOVER;
    mScanlineSpiritsCount = 0;
    for (uint8_t i = 0; i < 64; i++) {
        const uint8_t* spOAM = &mOAM[i << 2]; // * 4
        const uint16_t spY = spOAM[0] + 1;

        //精灵的Y坐标不在扫描线的范围内则跳过
        if (!(spY <= line && line < spY + mSpHeight))
            continue;

        Spirit* spInfo = &mSpirits[mScanlineSpiritsCount++];
        spInfo->id = i;
        spInfo->x = spOAM[3];
        spInfo->info = spOAM[2];
        const uint8_t title = spOAM[1];
        if (mSpHeight == 16) {    //如果精灵的高度是16，则奇数title使用0x1000的图样表
            //反之使用0x0000起始的图样表
            if (title & 0x1)
                spInfo->pattern = 0x1000 + ((title & 0xfe) << 4);//注意括号啊！一把辛酸泪！
            else
                spInfo->pattern = title << 4;
        }
        else {
            spInfo->pattern = mSpPatternTables + (title << 4);
        }

        const uint8_t offset = (spInfo->info & 0x80)
            ? mSpHeight - 1 - (line - spY)
            : line - spY;

        spInfo->pattern += offset + (offset & 0x8);
    }
    //虽然标记为溢出但是这里继续渲染，不然部分游戏会出现闪烁
    if (mScanlineSpiritsCount > 8) {
        mPPUStatus |= PPU_SPOVER;
    }
}


void NesPPU::RenderPattern(uint8_t* PatternMem) {
    uint8_t bit0, bit1;
    for (size_t row = 0; row < 32; row++) {
        for (size_t col = 0; col < 16; col++) {
            for (size_t y = 0; y < 8; y++) {
                uint16_t address = (row * 16 + col) * 16 + y;
                bit0 = famicom->GetDevices()->cartridge()->ReadViaPPU(address);
                bit1 = famicom->GetDevices()->cartridge()->ReadViaPPU(address + 8);
                //ReadPatternBytes((row * 16 + col) * 16 + y, &bit0, &bit1);
                DrawPixelUnit(bit0, bit1, 0, PatternMem + ((row * 8 + y) * 128), col * 8);
            }
        }
    }
}

/// <summary>
/// 渲染（或者说模拟）一帧
/// 每帧有0~261条扫描线
/// 0~239位可见扫描线 也就是屏幕的可见部分
/// 240 为空白扫描线
/// 241 ~ 260 为20条VBLANK扫描线 CPU在VBLANK期间可以访问PPU内存
/// 260扫描线后结束VBLANK
/// 261 为预渲染扫描线
/// </summary>




void NesPPU::PreEmulationFrame() {

    //初始化一些变量
    mPatternTables = (mPPUCtrl & BGUSE1000) ? 0x1000 : 0x0;
    mSpPatternTables = (mPPUCtrl & SPUSE1000) ? 0x1000 : 0x0;
    mSpHeight = (mPPUCtrl & SPH16) ? 16 : 8;
}

/// <summary>
/// 渲染每一条扫描线
/// </summary>
void NesPPU::RenderScanline(const uint16_t line) {
    //mPPUStatus |= PPU_SP0HIT;
    uint8_t* pBGMASK = mBGMask;
    if (mPPUMask & BGEN) {
        const uint8_t nametableSelect = (mRegs.v >> 10) & 0x3;
        const uint16_t scrolly = ((nametableSelect & 0x2) ? 240 : 0)
            + (((mRegs.v >> 5) & 0x1f) << 3) //Y * 8
            + (mRegs.v >> 12)
            //	+ thisScanline  //之所以这里不需要加是因为Y会自增
            ;

        const uint16_t scrollyIndex = scrolly / 240;
        const uint16_t scrollyOffset = scrolly % 240;

        const uint8_t curNametable = (nametableSelect & 0x1) | ((scrollyIndex & 1) << 1);


        uint8_t* const nametables[2] = {
                mNametables[curNametable],
                mNametables[curNametable ^ 1]
        };

        const uint8_t voffset = scrollyOffset & 7; //计算图样表垂直使用第几个(相当于 line % 8 )
        uint8_t attr4bit, attr2bit, ntOffset;
        uint16_t pattOffset;
        uint8_t bit0, bit1;

        int pixelOffset = -mRegs.x;

        const uint16_t start = mRegs.v & 0x1f;
        const uint16_t end = start + 33;
        //属性位移判断，实际就是判断使用前四位还是后四位
        const uint8_t attrBitShift = ((scrollyOffset & 31) >> 4) ? 4 : 0;
        //命名表基础偏移 thisScanline/8 * 32;
        const uint16_t ntBaseOffset = (scrollyOffset >> 3 << 5);
        //属性表基础偏移 thisScanline/32 * 8;
        const uint16_t atBaseOffset = (scrollyOffset >> 5 << 3);

        uint8_t* nametablebase, * nametable, * attrtable;


        for (uint16_t i = start; i < end; i++) {
            nametablebase = nametables[i >> 5]; //计算当前所使用的命名表
            nametable = nametablebase + ntBaseOffset; //加上基础偏移，计算出本行属性命名表偏移
            attrtable = nametablebase + 960 + atBaseOffset;  //属性表+属性表基础偏移

            ntOffset = i & 0x1f;
            //属性表偏移
            attr4bit = (attrtable[ntOffset >> 2] >> attrBitShift) & 0x0f;
            attr2bit = (attr4bit >> ((ntOffset % 4) > 1 ? 2 : 0)) & 0x3;

            //计算图样表偏移
            pattOffset = ((mPPUCtrl & BGUSE1000) ? 0x1000 : 0x0) +
                ((nametable[ntOffset] << 4) + voffset);
            //pattOffset = mPatternTables + ((nametable[ntOffset] << 4) + voffset);

            bit0 = famicom->GetDevices()->cartridge()->ReadViaPPU(pattOffset);
            bit1 = famicom->GetDevices()->cartridge()->ReadViaPPU(pattOffset + 8);

            *pBGMASK = bit0 | bit1;

            DrawPixelUnit(bit0, bit1, attr2bit, mScreenMemory + (line << 8), pixelOffset);

            pixelOffset += 8;

            pBGMASK++;
        }
    }
    else {
        //关闭渲染背景则填充黑色
        memset(mScreenMemory + ((uint16_t)line << 8), 13, 256);
        memset(mBGMask, 0, sizeof(mBGMask));
    }

    if (mPPUMask & SPEN) {
        ScanSpirits(line);
        DrawSpirits(line);
    }
}


void NesPPU::DrawSpirits(uint16_t line) {
    uint8_t bit0, bit1;
    for (uint8_t i = 0; i != mScanlineSpiritsCount; i++) {
        Spirit* spInfo = &mSpirits[mScanlineSpiritsCount - i - 1];

        bit0 = famicom->GetDevices()->cartridge()->ReadViaPPU(spInfo->pattern);
        bit1 = famicom->GetDevices()->cartridge()->ReadViaPPU(spInfo->pattern + 8);

        if (spInfo->info & 0x40) //判断是否水平反转
        {
            //查表反转像素点 空间换时间的做法
            bit0 = gBitReverseTable256[bit0];
            bit1 = gBitReverseTable256[bit1];
        }
        //测试是否击中
        if (spInfo->id == 0 && !(mPPUStatus & PPU_SP0HIT)) {
            const uint16_t spOffsetX = spInfo->x + mRegs.x;
            //X/8判断掩码的位置
            uint8_t* pMaskpos = &mBGMask[spOffsetX >> 3];
            //因为精灵坐标不对齐，因此需要左移来对齐
            // 例如
            //mask: 7 6 5 4 3 2 1 0
            //精灵：- - - X X X X X
            //因此mask左移3位 丢弃前三位
            //mask: 4 3 2 1 0
            //精灵：- - - X X X X X
            //又因为精灵本身就是D7位起始，因此实际 如下
            //mask: 4 3 2 1 0
            //精灵  X X X X X //获取的精灵掩码
            //之所以不直接对比索引号是因为这样有BUG...
            uint8_t maskcode = pMaskpos[0] << (spOffsetX & 0x7);

            //因为如果精灵可能横跨两个Tile，因此多获取一个
            maskcode |= pMaskpos[1] >> (8 - (spOffsetX & 0x7));

            //测试
            if ((bit0 | bit1) & maskcode)
                mPPUStatus |= PPU_SP0HIT;
        }
        //渲染精灵
        DrawSpiritsPixelUnit(
            bit0, bit1, spInfo->x, line, spInfo->info);
    }
}


void NesPPU::DrawSpiritsPixelUnit(uint8_t p1, uint8_t p2, uint8_t x, uint8_t y, uint8_t attr) {

    uint8_t bit, bith, colorOffset, bitShift;

    bitShift = 7;

    uint8_t* pScreen = mScreenMemory + ((uint16_t)y << 8);
    uint16_t pixelX = x;
    uint16_t end = min(x + 8, 256);

    for (; pixelX != end; pixelX++) {

        bit = (p1 >> bitShift) & 0x1;       //低位颜色值
        bit |= ((p2 >> bitShift) & 0x1) << 1;//高位颜色值

        //此像素透明不渲染
        if (bit == 0) goto _draw_spirits_pixel_unit_skip;

        //如果是P精灵则如果存在背景则不输出，模拟精灵在“背景后”的效果
        //这里的判断还是比较敷衍的 但是暂时够用
        if (attr & 0x20) {
            if (pScreen[pixelX] != mPalette[0])
                goto _draw_spirits_pixel_unit_skip;
        };

        //计算高位
        bith = ((attr & 0x3) << 2) & 0x0c;
        bith |= bit;

        //不能直接访问色板
        colorOffset = ReadByte(0x3F10 + bith);


        pScreen[pixelX] = colorOffset;
    _draw_spirits_pixel_unit_skip:
        bitShift--;
    }
}

/// <summary>
/// 以8像素为一组进行背景绘制
/// </summary>
/// <param name="p1">低位颜色值</param>
/// <param name="p2">高位颜色值</param>
/// <param name="attrl2bit"></param>
/// <param name="x"></param>
/// <param name="y"></param>
void NesPPU::DrawPixelUnit(uint8_t p1, uint8_t p2, uint8_t attrl2bit, uint8_t* pScreen, int x) {
    uint8_t bit, bith, colorOffset, bitShift = 7;
    //uint8_t* pCanvas = mCanvas + (y << 8);
    int16_t pixelX = x;//注意使用有符号数，因为可能出现负数
    uint32_t end = min(x + 8, 256);

    for (; pixelX != end; pixelX++) {
        if (pixelX < 0)
            goto _draw_pixel_skip;

        bit = (p1 >> bitShift) & 0x1;       //低位颜色值
        bit |= ((p2 >> bitShift) & 0x1) << 1;//高位颜色值

        if (!bit) { //透明则渲染为全局背景色
            pScreen[pixelX] = mPalette[0];
        }
        else {
            //计算高位
            bith = (attrl2bit << 2) & 0x0c;
            bith = (bith | bit) & 0xf;

            //不能直接访问色板
            colorOffset = ReadByte(0x3F00 + bith);
            //assert(pixelX >= 0 && pixelX < 61440);
            pScreen[pixelX] = colorOffset;
        }
    _draw_pixel_skip:
        bitShift--;
    }
}


/// <summary>
/// 通过CPU读取PPU的内存
/// 返回的是上一次读取的值
/// </summary>
/// <param name="address"></param>
/// <returns></returns>
inline uint8_t NesPPU::ReadByteViaCPU(uint16_t address) {

    if (address < 0x3F00) {
        uint8_t data = mReadBuffer;

        mReadBuffer = ReadByte(address);
        return data;
    }
    else {
        mReadBuffer = ReadByte(address - 0x1000);
        return ReadByte(address);
    }
}


/// <summary>
/// 切换屏幕的镜像方式
/// </summary>
/// <param name="mode"></param>
void NesPPU::SwitchMirroring(Mirror mode) {
    //	DebugPrintf("SwitchMirroring %d\n", mode);
    switch (mode) {
    case Mirror::SINGLE_SCREEN_LOWER:
        mNametables[0] = mVRAM[0];
        mNametables[1] = mVRAM[0];
        mNametables[2] = mVRAM[0];
        mNametables[3] = mVRAM[0];
        break;
    case Mirror::SINGLE_SCREEN_UPPER:
        mNametables[0] = mVRAM[1];
        mNametables[1] = mVRAM[1];
        mNametables[2] = mVRAM[1];
        mNametables[3] = mVRAM[1];
        break;
    case Mirror::VERTICAL:
        mNametables[0] = mVRAM[0];
        mNametables[1] = mVRAM[1];
        mNametables[2] = mVRAM[0];
        mNametables[3] = mVRAM[1];
        break;
    case Mirror::HORIZONTAL:
        mNametables[0] = mVRAM[0];
        mNametables[1] = mVRAM[0];
        mNametables[2] = mVRAM[1];
        mNametables[3] = mVRAM[1];
        break;
    case Mirror::FOUR_SCREEN:
        //暂时不支持
        break;
    }
}

void NesPPU::Save(SaveBundle* bundle) {
    bundle->SaveBlock(mVRAM[0], 1024);
    bundle->SaveBlock(mVRAM[1], 1024);
    bundle->SaveBlock(mPalette, sizeof(mPalette));
    bundle->SaveBlock(mOAM, sizeof(mOAM));

    bundle->SaveWord(mNametables[0] - (uint8_t*)mVRAM);
    bundle->SaveWord(mNametables[1] - (uint8_t*)mVRAM);
    bundle->SaveWord(mNametables[2] - (uint8_t*)mVRAM);
    bundle->SaveWord(mNametables[3] - (uint8_t*)mVRAM);

    bundle->SaveByte(mPreviousData);
    bundle->SaveByte(mReadBuffer);

    bundle->SaveBlock((uint8_t*)&mRegs, sizeof(mRegs));

    bundle->SaveByte(mPPUCtrl);
    bundle->SaveByte(mPPUStatus);
    bundle->SaveByte(mPPUMask);
    bundle->SaveByte(mOamaddr);


}

void NesPPU::Restore(SaveBundle* bundle) {
    bundle->RestoreBlock(mVRAM[0], 1024);
    bundle->RestoreBlock(mVRAM[1], 1024);
    bundle->RestoreBlock(mPalette, sizeof(mPalette));
    bundle->RestoreBlock(mOAM, sizeof(mOAM));

    mNametables[0] = bundle->RestoreWord() + (uint8_t*)mVRAM;
    mNametables[1] = bundle->RestoreWord() + (uint8_t*)mVRAM;
    mNametables[2] = bundle->RestoreWord() + (uint8_t*)mVRAM;
    mNametables[3] = bundle->RestoreWord() + (uint8_t*)mVRAM;

    mPreviousData = bundle->RestoreByte();
    mReadBuffer = bundle->RestoreByte();

    bundle->RestoreBlock((uint8_t*)&mRegs, sizeof(mRegs));

    mPPUCtrl = bundle->RestoreByte();
    mPPUStatus = bundle->RestoreByte();
    mPPUMask = bundle->RestoreByte();
    mOamaddr = bundle->RestoreByte();
}
