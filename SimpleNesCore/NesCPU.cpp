#include "common.h"
#include "NesCPU.h"
#include "Famicom.h"
#include <cassert>
#include "Devices.h"

#define addracc  &NesCPU::_addrAcc //累加器寻址
#define addrimm &NesCPU::_addrImm
#define addrzp &NesCPU::_addrZp
#define addrzpx &NesCPU::_addrZpx
#define addrzpy &NesCPU::_addrZpy
#define addrizx &NesCPU::_addrIzx
#define addrizy &NesCPU::_addrIzy
#define addrabs &NesCPU::_addrAbs
#define addrabx &NesCPU::_addrAbsX
#define addraby &NesCPU::_addrAbsY
#define addrind &NesCPU::_addrInd
#define addrrel &NesCPU::_addrRel
#define addrnone NULL

#define _OP(var0, var1, var2, var3, var4, var5) \
            this->mOpcodes[var0] = { &NesCPU::var1, var2, var3, var4 ,var5 }

NesCPU::NesCPU(Famicom* fc) :famicom(fc) {
	for (size_t i = 0; i < 256; i++) {
		_OP(i, OPUNK, addrnone, 2, 0, "_NO_");
	}

	//初始化命令
	//_OP(0x0, OpBrk, addrnone, 7, 0, "brk");

	_OP(0x03, OpSlo, addrizx, 8, 0, "slo");
	_OP(0x07, OpSlo, addrzp, 5, 0, "slo");
	_OP(0x0f, OpSlo, addrabs, 6, 0, "slo");
	_OP(0x13, OpSlo, addrizy, 8, 0, "slo");
	_OP(0x17, OpSlo, addrzpx, 6, 0, "slo");
	_OP(0x1b, OpSlo, addraby, 7, 0, "slo");
	_OP(0x1f, OpSlo, addrabx, 7, 0, "slo");

	_OP(0x80, OpNop, addrimm, 2, 0, "nop");
	_OP(0x82, OpNop, addrimm, 2, 0, "nop");
	_OP(0xc2, OpNop, addrimm, 2, 0, "nop");
	_OP(0xe2, OpNop, addrimm, 2, 0, "nop");

	_OP(0x04, OpNop, addrzp, 3, 0, "nop");
	_OP(0x0c, OpNop, addrabs, 4, 0, "nop");
	_OP(0x1c, OpNop, addrabx, 4, 0, "nop");
	_OP(0x14, OpNop, addrzpx, 4, 0, "nop");
	_OP(0x34, OpNop, addrzpx, 4, 0, "nop");
	_OP(0x44, OpNop, addrzpx, 4, 0, "nop");
	_OP(0x54, OpNop, addrzpx, 4, 0, "nop");
	_OP(0x64, OpNop, addrzpx, 4, 0, "nop");
	_OP(0x74, OpNop, addrzpx, 4, 0, "nop");
	_OP(0xd4, OpNop, addrzpx, 4, 0, "nop");
	_OP(0xf4, OpNop, addrzpx, 4, 0, "nop");

	_OP(0x89, OpNop, addrimm, 2, 0, "nop");

	_OP(0x1a, _opNop, addrnone, 2, 0, "nop");
	_OP(0x3a, _opNop, addrnone, 2, 0, "nop");
	_OP(0x5a, _opNop, addrnone, 2, 0, "nop");
	_OP(0x7a, _opNop, addrnone, 2, 0, "nop");
	_OP(0xda, _opNop, addrnone, 2, 0, "nop");
	_OP(0xea, _opNop, addrnone, 2, 0, "nop");
	_OP(0xfa, _opNop, addrnone, 2, 0, "nop");

	_OP(0x0c, OpNop, addrabs, 4, 0, "nop");
	_OP(0x1c, OpNop, addrabx, 4, 1, "nop");
	_OP(0x3c, OpNop, addrabx, 4, 1, "nop");
	_OP(0x5c, OpNop, addrabx, 4, 1, "nop");
	_OP(0x7c, OpNop, addrabx, 4, 1, "nop");
	_OP(0xdc, OpNop, addrabx, 4, 1, "nop");
	_OP(0xfc, OpNop, addrabx, 4, 1, "nop");

	_OP(0x23, OpRla, addrizx, 8, 0, "RLA");
	_OP(0x33, OpRla, addrizy, 8, 0, "RLA");
	_OP(0x27, OpRla, addrzp, 5, 0, "RLA");
	_OP(0x37, OpRla, addrzpx, 6, 0, "RLA");
	_OP(0x3b, OpRla, addraby, 7, 0, "RLA");
	_OP(0x2f, OpRla, addrabs, 6, 0, "RLA");
	_OP(0x3f, OpRla, addrabx, 7, 0, "RLA");

	_OP(0x43, OpSre, addrizx, 8, 0, "SRE");
	_OP(0x53, OpSre, addrizy, 8, 0, "SRE");
	_OP(0x47, OpSre, addrzp, 5, 0, "SRE");
	_OP(0x57, OpSre, addrzpx, 6, 0, "SRE");
	_OP(0x5b, OpSre, addraby, 7, 0, "SRE");
	_OP(0x4f, OpSre, addrabs, 6, 0, "SRE");
	_OP(0x5f, OpSre, addrabx, 7, 0, "SRE");

	_OP(0x63, OpRra, addrizx, 8, 0, "RRA");
	_OP(0x73, OpRra, addrizy, 8, 0, "RRA");
	_OP(0x67, OpRra, addrzp, 5, 0, "RRA");
	_OP(0x77, OpRra, addrzpx, 6, 0, "RRA");
	_OP(0x7b, OpRra, addraby, 7, 0, "RRA");
	_OP(0x6f, OpRra, addrabs, 6, 0, "RRA");
	_OP(0x7f, OpRra, addrabx, 7, 0, "RRA");

	_OP(0x83, OpSax, addrizx, 6, 0, "SAX");
	_OP(0x87, OpSax, addrzp, 3, 0, "SAX");
	_OP(0x97, OpSax, addrzpy, 4, 0, "SAX");
	_OP(0x8f, OpSax, addrabs, 4, 0, "SAX");

	_OP(0xa3, OpLax, addrizx, 6, 0, "LAX");
	_OP(0xb3, OpLax, addrizy, 5, 1, "LAX");
	_OP(0xa7, OpLax, addrzp, 3, 0, "LAX");
	_OP(0xb7, OpLax, addrzpy, 4, 0, "LAX");
	_OP(0xab, OpLax, addrimm, 2, 0, "LAX");
	_OP(0xaf, OpLax, addrabs, 4, 0, "LAX");
	_OP(0xbf, OpLax, addraby, 4, 1, "LAX");

	_OP(0xc3, OpDcp, addrizx, 8, 0, "DCP");
	_OP(0xd3, OpDcp, addrizy, 8, 0, "DCP");
	_OP(0xc7, OpDcp, addrzp, 5, 0, "DCP");
	_OP(0xd7, OpDcp, addrzpx, 6, 0, "DCP");
	_OP(0xcf, OpDcp, addrabs, 6, 0, "DCP");
	_OP(0xdf, OpDcp, addrabx, 7, 0, "DCP");
	_OP(0xdb, OpDcp, addraby, 7, 0, "DCP");

	_OP(0xe3, OpIsc, addrizx, 8, 0, "ISC");
	_OP(0xf3, OpIsc, addrizy, 8, 0, "ISC");
	_OP(0xe7, OpIsc, addrzp, 5, 0, "ISC");
	_OP(0xf7, OpIsc, addrzpx, 6, 0, "ISC");
	_OP(0xfb, OpIsc, addraby, 7, 0, "ISC");
	_OP(0xef, OpIsc, addrabs, 6, 0, "ISC");
	_OP(0xff, OpIsc, addrabx, 7, 0, "ISC");

	_OP(0x0b, OpAnc, addrimm, 2, 0, "ANC");
	_OP(0x2b, OpAnc, addrimm, 2, 0, "ANC");

	_OP(0x4b, OpAlr, addrimm, 2, 0, "ALR");
	_OP(0x6b, OpArr, addrimm, 2, 0, "ARR");
	_OP(0x8b, OpXaa, addrimm, 2, 0, "XAA");
	// opcodes[0xab,_FP(opAlr,addrimm,2,0);
	_OP(0xcb, OpAxs, addrimm, 2, 0, "AXS");
	_OP(0xeb, OpSbc, addrimm, 2, 0, "SBC");

	_OP(0x93, OpAhx, addrizy, 6, 0, "AHX");
	_OP(0x9f, OpAhx, addraby, 5, 0, "AHX");
	_OP(0x9c, OpShy, addrabx, 5, 0, "SHY");
	_OP(0x9e, OpShx, addraby, 5, 0, "SHX");
	_OP(0x9b, OpTas, addraby, 5, 0, "TAS");
	_OP(0xbb, OpLas, addraby, 4, 1, "LAS");

	_OP(0x8b, OpXaa, addrimm, 2, 1, "XAA");

	//ADC
	_OP(0x69, OpAdc, addrimm, 2, 0, "ADC");
	_OP(0x65, OpAdc, addrzp, 3, 0, "ADC");
	_OP(0x75, OpAdc, addrzpx, 4, 0, "ADC");
	_OP(0x6d, OpAdc, addrabs, 4, 0, "ADC");
	_OP(0x7d, OpAdc, addrabx, 4, 1, "ADC");
	_OP(0x79, OpAdc, addraby, 4, 1, "ADC");
	_OP(0x61, OpAdc, addrizx, 6, 0, "ADC");
	_OP(0x71, OpAdc, addrizy, 5, 1, "ADC");

	//SBC
	_OP(0xe9, OpSbc1, addrimm, 2, 0, "SBC");
	_OP(0xe5, OpSbc1, addrzp, 3, 0, "SBC");
	_OP(0xf5, OpSbc1, addrzpx, 4, 0, "SBC");
	_OP(0xed, OpSbc1, addrabs, 4, 0, "SBC");
	_OP(0xfd, OpSbc1, addrabx, 4, 1, "SBC");
	_OP(0xf9, OpSbc1, addraby, 4, 1, "SBC");
	_OP(0xe1, OpSbc1, addrizx, 6, 0, "SBC");
	_OP(0xf1, OpSbc1, addrizy, 5, 1, "SBC");

	//INC
	_OP(0xe6, OpInc, addrzp, 5, 0, "INC");
	_OP(0xf6, OpInc, addrzpx, 6, 0, "INC");
	_OP(0xee, OpInc, addrabs, 6, 0, "INC");
	_OP(0xfe, OpInc, addrabx, 7, 0, "INC");

	_OP(0xe8, OpInx, addrnone, 2, 0, "INX");
	_OP(0xc8, OpIny, addrnone, 2, 0, "INX");

	//DEC
	_OP(0xc6, OpDec, addrzp, 5, 0, "DEC");
	_OP(0xd6, OpDec, addrzpx, 6, 0, "DEC");
	_OP(0xce, OpDec, addrabs, 6, 0, "DEC");
	_OP(0xde, OpDec, addrabx, 7, 0, "DEC");

	_OP(0xca, OpDex, addrnone, 2, 0, "DEX");
	_OP(0x88, OpDey, addrnone, 2, 0, "DEY");

	//AND
	_OP(0x29, OpAnd, addrimm, 2, 0, "AND");
	_OP(0x25, OpAnd, addrzp, 3, 0, "AND");
	_OP(0x35, OpAnd, addrzpx, 4, 0, "AND");
	_OP(0x2d, OpAnd, addrabs, 4, 0, "AND");
	_OP(0x3d, OpAnd, addrabx, 4, 1, "AND");
	_OP(0x39, OpAnd, addraby, 4, 1, "AND");
	_OP(0x21, OpAnd, addrizx, 6, 0, "AND");
	_OP(0x31, OpAnd, addrizy, 5, 1, "AND");

	//EOR
	_OP(0x49, OpEor, addrimm, 2, 0, "EOR");
	_OP(0x45, OpEor, addrzp, 3, 0, "EOR");
	_OP(0x55, OpEor, addrzpx, 4, 0, "EOR");
	_OP(0x4d, OpEor, addrabs, 4, 0, "EOR");
	_OP(0x5d, OpEor, addrabx, 4, 1, "EOR");
	_OP(0x59, OpEor, addraby, 4, 1, "EOR");
	_OP(0x41, OpEor, addrizx, 6, 0, "EOR");
	_OP(0x51, OpEor, addrizy, 5, 1, "EOR");

	//ORA
	_OP(0x09, OpOra, addrimm, 2, 0, "ORA");
	_OP(0x05, OpOra, addrzp, 3, 0, "ORA");
	_OP(0x15, OpOra, addrzpx, 4, 0, "ORA");
	_OP(0x0d, OpOra, addrabs, 4, 0, "ORA");
	_OP(0x1d, OpOra, addrabx, 4, 1, "ORA");
	_OP(0x19, OpOra, addraby, 4, 1, "ORA");
	_OP(0x01, OpOra, addrizx, 6, 0, "ORA");
	_OP(0x11, OpOra, addrizy, 5, 1, "ORA");

	//BIT
	_OP(0x24, OpBit, addrzp, 3, 0, "BIT");
	_OP(0x2c, OpBit, addrabs, 4, 0, "BIT");

	//ASL
	_OP(0x0a, OpAsl_acc, addracc, 2, 0, "ASL");
	_OP(0x06, OpAsl, addrzp, 5, 0, "ASL");
	_OP(0x16, OpAsl, addrzpx, 6, 0, "ASL");
	_OP(0x0e, OpAsl, addrabs, 6, 0, "ASL");
	_OP(0x1e, OpAsl, addrabx, 7, 0, "ASL");

	//LSR
	_OP(0x4a, OpLsr_acc, addracc, 2, 0, "LSR");
	_OP(0x46, OpLsr, addrzp, 5, 0, "LSR");
	_OP(0x56, OpLsr, addrzpx, 6, 0, "LSR");
	_OP(0x4e, OpLsr, addrabs, 6, 0, "LSR");
	_OP(0x5e, OpLsr, addrabx, 7, 0, "LSR");

	//ROL
	_OP(0x2a, OpRol_acc, addracc, 2, 0, "ROL");
	_OP(0x26, OpRol, addrzp, 5, 0, "ROL");
	_OP(0x36, OpRol, addrzpx, 6, 0, "ROL");
	_OP(0x2e, OpRol, addrabs, 6, 0, "ROL");
	_OP(0x3e, OpRol, addrabx, 7, 0, "ROL");

	//ROR
	_OP(0x6a, OpRor_acc, addracc, 2, 0, "ROR");
	_OP(0x66, OpRor, addrzp, 5, 0, "ROR");
	_OP(0x76, OpRor, addrzpx, 6, 0, "ROR");
	_OP(0x6e, OpRor, addrabs, 6, 0, "ROR");
	_OP(0x7e, OpRor, addrabx, 7, 0, "ROR");

	//LDA
	_OP(0xa9, OpLda, addrimm, 2, 0, "LDA");
	_OP(0xa5, OpLda, addrzp, 3, 0, "LDA");
	_OP(0xb5, OpLda, addrzpx, 4, 0, "LDA");
	_OP(0xad, OpLda, addrabs, 4, 0, "LDA");
	_OP(0xbd, OpLda, addrabx, 4, 1, "LDA");
	_OP(0xb9, OpLda, addraby, 4, 1, "LDA");
	_OP(0xa1, OpLda, addrizx, 6, 0, "LDA");
	_OP(0xb1, OpLda, addrizy, 5, 1, "LDA");

	//LDX
	_OP(0xa2, OpLdx, addrimm, 2, 0, "LDX");
	_OP(0xa6, OpLdx, addrzp, 3, 0, "LDX");
	_OP(0xb6, OpLdx, addrzpy, 4, 0, "LDX");
	_OP(0xae, OpLdx, addrabs, 4, 0, "LDX");
	_OP(0xbe, OpLdx, addraby, 4, 1, "LDX");

	//LDY
	_OP(0xa0, OpLdy, addrimm, 2, 0, "LDY");
	_OP(0xa4, OpLdy, addrzp, 3, 0, "LDY");
	_OP(0xb4, OpLdy, addrzpx, 4, 0, "LDY");
	_OP(0xac, OpLdy, addrabs, 4, 0, "LDY");
	_OP(0xbc, OpLdy, addrabx, 4, 1, "LDY");

	//STA
	_OP(0x85, OpSta, addrzp, 3, 0, "STA");
	_OP(0x95, OpSta, addrzpx, 4, 0, "STA");
	_OP(0x8d, OpSta, addrabs, 4, 0, "STA");
	_OP(0x9d, OpSta, addrabx, 5, 0, "STA");
	_OP(0x99, OpSta, addraby, 5, 0, "STA");
	_OP(0x81, OpSta, addrizx, 6, 0, "STA");
	_OP(0x91, OpSta, addrizy, 6, 0, "STA");

	//STX
	_OP(0x86, OpStx, addrzp, 3, 0, "STX");
	_OP(0x96, OpStx, addrzpy, 4, 0, "STX");
	_OP(0x8e, OpStx, addrabs, 4, 0, "STX");

	//STY
	_OP(0x84, OpSty, addrzp, 3, 0, "STY");
	_OP(0x94, OpSty, addrzpx, 4, 0, "STY");
	_OP(0x8c, OpSty, addrabs, 4, 0, "STY");

	//
	_OP(0xaa, OpTax, addrnone, 2, 0, "TAX");
	_OP(0xa8, OpTay, addrnone, 2, 0, "TAY");
	_OP(0xba, OpTsx, addrnone, 2, 0, "TSX");
	_OP(0x8a, OpTxa, addrnone, 2, 0, "TXA");
	_OP(0x9a, OpTxs, addrnone, 2, 0, "TXS");
	_OP(0x98, OpTya, addrnone, 2, 0, "TYA");

	//CMP
	_OP(0xc9, OpCmp, addrimm, 2, 0, "CMP");
	_OP(0xc5, OpCmp, addrzp, 3, 0, "CMP");
	_OP(0xd5, OpCmp, addrzpx, 4, 0, "CMP");
	_OP(0xcd, OpCmp, addrabs, 4, 0, "CMP");
	_OP(0xdd, OpCmp, addrabx, 4, 1, "CMP");
	_OP(0xd9, OpCmp, addraby, 4, 1, "CMP");
	_OP(0xc1, OpCmp, addrizx, 6, 0, "CMP");
	_OP(0xd1, OpCmp, addrizy, 5, 1, "CMP");

	//CPX
	_OP(0xe0, OpCpx, addrimm, 2, 0, "CPX");
	_OP(0xe4, OpCpx, addrzp, 3, 0, "CPX");
	_OP(0xec, OpCpx, addrabs, 4, 0, "CPX");

	//CPY
	_OP(0xc0, OpCpy, addrimm, 2, 0, "CPY");
	_OP(0xc4, OpCpy, addrzp, 3, 0, "CPY");
	_OP(0xcc, OpCpy, addrabs, 4, 0, "CPY");

	//条件分支
	_OP(0x90, OpBcc, addrrel, 2, 2, "BCC");
	_OP(0xb0, OpBcs, addrrel, 2, 2, "BCS");
	_OP(0xf0, OpBeq, addrrel, 2, 2, "BEQ");
	_OP(0x30, OpBmi, addrrel, 2, 2, "BMI");
	_OP(0xd0, OpBne, addrrel, 2, 2, "BNE");
	_OP(0x10, OpBpl, addrrel, 2, 2, "BPL");
	_OP(0x50, OpBvc, addrrel, 2, 2, "BVC");
	_OP(0x70, OpBvs, addrrel, 2, 2, "BVS");

	//强制转移指令
	_OP(0x00, OpBrk, addrnone, 7, 0, "BRK");
	_OP(0x4c, OpJmp, addrabs, 3, 0, "JMP");
	_OP(0x6c, OpJmp, addrind, 5, 0, "JMP");
	_OP(0x20, OpJsr, addrabs, 6, 0, "JSR");
	_OP(0x40, OpRti, addrnone, 6, 0, "RTI");
	_OP(0x60, OpRts, addrnone, 6, 0, "RTS");

	//设置状态处理器
	_OP(0x18, OpClc, addrnone, 2, 0, "CLC");
	_OP(0xd8, OpCld, addrnone, 2, 0, "CLD");
	_OP(0x58, OpCli, addrnone, 2, 0, "CLI");
	_OP(0xb8, OpClv, addrnone, 2, 0, "CLV");
	_OP(0x38, OpSec, addrnone, 2, 0, "SEC");
	_OP(0xf8, OpSed, addrnone, 2, 0, "SED");
	_OP(0x78, OpSei, addrnone, 2, 0, "SEI");

	//堆栈操作
	_OP(0x48, OpPha, addrnone, 3, 0, "PHA");
	_OP(0x08, OpPhp, addrnone, 3, 0, "PHP");
	_OP(0x68, OpPla, addrnone, 4, 0, "PLA");
	_OP(0x28, OpPlp, addrnone, 4, 0, "PLP");
}


char buffer[1024] = { 0 };
#define DREAD(X) (prgrom == nullptr ? famicom->cpubus->ReadByte(pcaddr+X):prgrom[realaddr+X])

#define _DSRINTF1 "				A:%02x X:%02x Y:%02x SP:%02x \n"
#define _DSRINTF2 REG_A,REG_X,REG_Y,REG_SP

int NesCPU::Disassembly(uint8_t* prgrom, uint16_t pcaddr, uint16_t realaddr, uint8_t opcode,
	OpInfo* opInfo) const {
	int oplen = 1;
	return oplen;
}


/// <summary>
/// 执行指定周期的CPU指令
/// </summary>
/// <param name="requestCycles">指定要执行的CPU周期</param>
/// <returns>返回实际执行的CPU周期</returns>
int32_t NesCPU::Exec(int32_t requestCycles) {
	mDeferCycles = 0;

	while (mDeferCycles < requestCycles) {
		//预更改 绝大多数的P修改都会延迟到下一个周期
		//在这里才实际应用
		REG_P = REG_TMP_P;

		//触发中断
		if (mNmiTiggle) {
			mNmiTiggle = 0;
			NMI();
			//DebugPrintf("TIGGLE NMI\n");
		}
		else if (mIrqTiggle) {
			mIrqTiggle = 0;
			IRQ();
			//DebugPrintf("TIGGLE IRQ\n");
		}

		//获取指令
		mThisPC = REG_PC; //记录当前指令的地址
		mOpcode = ReadByteForPc();
		mPtrOpcode = &mOpcodes[mOpcode];
#if 0
		Disassembly(nullptr, mThisPC, 0, mOpcode, mPtrOpcode);
#endif
		//执行指令
		(this->*(mPtrOpcode->handler))();
		if (mPtrOpcode->morecycle & 0x01) {
			if ((REG_PC & 0xff00) != (mDataAddress & 0xff00))
				mDeferCycles++;
		}
		mDeferCycles += mPtrOpcode->cycle;

		//检测中断
		if (mInterruptFlag & NMI_PENING) {
			mInterruptFlag &= ~NMI_PENING;
			mNmiTiggle = 0xff;
		}
		else if (!IF_SET && (mInterruptFlag & (~NMI_PENING))) {
			mInterruptFlag &= ~IRQ_ONESHOT;//一次性触发的
			mIrqTiggle = 0xff;
		}
	}
	return mDeferCycles;

}


inline void NesCPU::step() {

}

/// <summary>
/// 取消IRQ中断
/// </summary>
void NesCPU::ClrIrq() {
	mInterruptFlag &= ~IRQ_PENING;
}

void NesCPU::ClrIrq(uint8_t tiggle) {
	//DebugPrintf("ClrIrq %x\n", tiggle);
	mInterruptFlag &= ~tiggle;
}

/// <summary>
/// 尝试触发IRQ中断
/// 低?电平触发，也就是不取消则会一直触发
/// </summary>
void NesCPU::TryIrq() {
	mInterruptFlag |= IRQ_PENING;
}

void NesCPU::TryIrq(uint8_t tiggle) {
	//DebugPrintf("TryIrq %x\n",tiggle);
	mInterruptFlag |= tiggle;
}

/// <summary>
/// 尝试触发硬件中断
/// 下降沿?触发，也就是电平降低的一瞬间只触发一次
/// </summary>
void NesCPU::TryNmi() {
	mInterruptFlag |= NMI_PENING;
}

inline void NesCPU::IRQ() {
	PushWord(REG_PC);
	PushByte(REG_P | UF);
	REG_TMP_P |= IF;
	REG_P = REG_TMP_P;//状态更新到实际P标志
    REG_PC = famicom->GetDevices()->bus()->ReadWord(0xFFFE);
}


void NesCPU::NMI() {
	PushWord(REG_PC);
	//保存 P
	PushByte(REG_P & ~BF);
	REG_TMP_P |= IF;
	REG_P = REG_TMP_P;//状态更新到实际P标志
    REG_PC = famicom->GetDevices()->bus()->ReadWord(0xFFFA);
	mDeferCycles += 7;
}


/// <summary>
/// 程序触发中断
/// </summary>
inline void NesCPU::OpBrk() {
	ReadByteForPc(); //多读一个字节
	PushWord(REG_PC);
	PushByte(REG_PC | BF);
	REG_TMP_P |= IF;
	REG_P = REG_TMP_P;//状态更新到实际P标志
    REG_PC = famicom->GetDevices()->bus()->ReadWord(0xFFFE);
	DebugPrintf("OpBrk\n");
}


inline void NesCPU::OpCli() {
	REG_TMP_P &= ~IF;
}

inline void NesCPU::OpRti() {
	REG_TMP_P = PopByte();
	REG_TMP_P &= ~BF; //消除BF的影响
	REG_TMP_P |= UF;
	REG_P = REG_TMP_P;//状态更新到实际P标志
	REG_PC = PopWord();
}


/// <summary>
/// 复位
/// </summary>
void NesCPU::Reset() {
	REG_A = REG_X = REG_Y = 0;
	REG_SP = 0;
	REG_TMP_P = UF;
	REG_P = REG_TMP_P;//状态更新到实际P标志
    REG_PC = famicom->GetDevices()->bus()->ReadWord(0xFFFC);
	//清除中断状态
	mInterruptFlag = 0;
	mIrqTiggle = 0;
	mNmiTiggle = 0;
}


/// <summary>
/// 未知的命令，通常是模拟器问题
/// </summary>
void NesCPU::OPUNK() {
	DebugPrintf("OpNoSupport %x %x\n", mThisPC, mOpcode);
	//assert(!"不支持的命令");
}


inline void NesCPU::OpAdc() {
	GET_ADDRESS();
	READ_BYTE();
	MathAdd(this->mDataByte);
}


void NesCPU::OpSbc1() {
	GET_ADDRESS();
	READ_BYTE();
	MathSub(this->mDataByte);
}

void NesCPU::OpInc() {
	GET_ADDRESS();
	READ_BYTE();
	this->mDataByte++;
    famicom->GetDevices()->bus()->WriteByte(this->mDataAddress, this->mDataByte);
	ASET_NZ(this->mDataByte);
}

void NesCPU::OpInx() {
	++REG_X;
	ASET_NZ(REG_X);
}

void NesCPU::OpDec() {
	GET_ADDRESS();
	READ_BYTE();
	--mDataByte;
	WRITE_BYTE(mDataByte);
	ASET_NZ(mDataByte);
}

void NesCPU::OpDex() {
	--REG_X;
	ASET_NZ(REG_X);
}

void NesCPU::OpDey() {
	--REG_Y;
	ASET_NZ(REG_Y);
}

void NesCPU::OpAnd() {
	GET_ADDRESS();
	READ_BYTE();
	REG_A &= mDataByte;
	ASET_NZ(REG_A);
}

void NesCPU::OpEor() {
	GET_ADDRESS();
	READ_BYTE();
	REG_A ^= mDataByte;
	ASET_NZ(REG_A);
}


void NesCPU::OpOra() {
	GET_ADDRESS();
	READ_BYTE();
	REG_A |= mDataByte;

	ASET_NZ(REG_A);
}

void NesCPU::OpBit() {
	GET_ADDRESS();
	READ_BYTE();
	this->MathBit(this->mDataByte);
}

void NesCPU::OpAsl_acc() {
	REG_A = this->MathASL(REG_A);
}

void NesCPU::OpAsl() {
	GET_ADDRESS();
	READ_BYTE();
	WRITE_BYTE(MathASL(mDataByte));
}

void NesCPU::OpLsr_acc() {
	REG_A = this->MathLSR(REG_A);
}

void NesCPU::OpLsr() {
	GET_ADDRESS();
	READ_BYTE();
	WRITE_BYTE(MathLSR(mDataByte));
}

void NesCPU::OpRol_acc() {

	REG_A = this->MathROL(REG_A);
}

void NesCPU::OpRol() {
	GET_ADDRESS();
	READ_BYTE();
	WRITE_BYTE(MathROL(mDataByte));
}

void NesCPU::OpRor_acc() {
	REG_A = this->MathROR(REG_A);
}

void NesCPU::OpRor() {
	GET_ADDRESS();
	READ_BYTE();
	WRITE_BYTE(MathROR(mDataByte));
}


void NesCPU::OpLda() {
	GET_ADDRESS();
	READ_BYTE();
	REG_A = this->mDataByte;
	ASET_NZ(REG_A);
}

void NesCPU::OpSta() {
	GET_ADDRESS();
	WRITE_BYTE(REG_A);
}

void NesCPU::OpStx() {
	GET_ADDRESS();
	WRITE_BYTE(REG_X);
}

void NesCPU::OpSty() {
	GET_ADDRESS();
	WRITE_BYTE(REG_Y);
}

void NesCPU::OpTax() {
	REG_X = REG_A;
	ASET_NZ(REG_X);
}

void NesCPU::OpTay() {
	REG_Y = REG_A;
	ASET_NZ(REG_Y);
}

void NesCPU::OpTsx() {
	REG_X = REG_SP;
	ASET_NZ(REG_X);
}

void NesCPU::OpTxa() {
	REG_A = REG_X;
	ASET_NZ(REG_A);
}

void NesCPU::OpTxs() {
	REG_SP = REG_X;
}

void NesCPU::OpTya() {
	REG_A = REG_Y;
	ASET_NZ(REG_A);
}


void NesCPU::OpLdx() {
	GET_ADDRESS();
	READ_BYTE();
	REG_X = this->mDataByte;
	ASET_NZ(REG_X);
}

void NesCPU::OpLdy() {
	GET_ADDRESS();
	READ_BYTE();
	REG_Y = this->mDataByte;
	ASET_NZ(REG_Y);
}


void NesCPU::OpCmp() {
	GET_ADDRESS();
	READ_BYTE();
	MathCMP2(REG_A, this->mDataByte);
}

void NesCPU::OpBcc() {
	GET_ADDRESS();

	if (!CF_SET) {
		//判断是否是跨页访问，如果是则增加一个指令周期
		if ((REG_PC & 0xff00) != (this->mDataAddress & 0xff00))
			this->mDeferCycles += 1;

		REG_PC = this->mDataAddress;
		this->mDeferCycles += 1;
	}
}

void NesCPU::OpBcs() {
	GET_ADDRESS();

	if (CF_SET) {

		///判断是否是跨页访问，如果是则增加一个指令周期
		if ((REG_PC & 0xff00) != (this->mDataAddress & 0xff00))
			this->mDeferCycles += 1;

		REG_PC = this->mDataAddress;
		this->mDeferCycles += 1;
	}
}
/// <summary>
/// 
/// </summary>
void NesCPU::OpBeq() {
	GET_ADDRESS();

	if (ZF_SET) {

		///判断是否是跨页访问，如果是则增加一个指令周期
		if ((REG_PC & 0xff00) != (this->mDataAddress & 0xff00))
			++this->mDeferCycles;

		REG_PC = this->mDataAddress;
		++this->mDeferCycles;
	}
}


void NesCPU::OpBmi() {
	GET_ADDRESS();

	if (NF_SET) {

		///判断是否是跨页访问，如果是则增加一个指令周期
		if ((REG_PC & 0xff00) != (this->mDataAddress & 0xff00))
			++this->mDeferCycles;

		REG_PC = this->mDataAddress;
		++this->mDeferCycles;
	}
}

void NesCPU::OpBne() {
	GET_ADDRESS();

	if (!ZF_SET) {

		///判断是否是跨页访问，如果是则增加一个指令周期
		if ((REG_PC & 0xff00) != (this->mDataAddress & 0xff00))
			++this->mDeferCycles;

		REG_PC = this->mDataAddress;
		++this->mDeferCycles;
	}

}

void NesCPU::OpBpl() {

	GET_ADDRESS();

	if (!NF_SET) {

		///判断是否是跨页访问，如果是则增加一个指令周期
		if ((REG_PC & 0xff00) != (this->mDataAddress & 0xff00))
			++this->mDeferCycles;

		REG_PC = this->mDataAddress;
		++this->mDeferCycles;
	}
}

void NesCPU::OpBvc() {
	GET_ADDRESS();
	if (!VF_SET) {
		///判断是否是跨页访问，如果是则增加一个指令周期
		if ((REG_PC & 0xff00) != (this->mDataAddress & 0xff00))
			++this->mDeferCycles;

		REG_PC = this->mDataAddress;
		++this->mDeferCycles;
	}

}

void NesCPU::OpBvs() {
	GET_ADDRESS();
	if (VF_SET) {
		///判断是否是跨页访问，如果是则增加一个指令周期
		if ((REG_PC & 0xff00) != (this->mDataAddress & 0xff00))
			++this->mDeferCycles;

		REG_PC = this->mDataAddress;
		++this->mDeferCycles;
	}
}


void NesCPU::OpJmp() {
	GET_ADDRESS();
	//将新的地址赋值到PC
	REG_PC = this->mDataAddress;
}

void NesCPU::OpJsr() {
	GET_ADDRESS();
	PushWord(mThisPC + 2);
	REG_PC = this->mDataAddress;
	//DebugPrintf("OpJsr\n");
}

void NesCPU::OpRts() {
	//新的地址 赋值给 SP
	REG_PC = this->PopWord() + 1;
}

void NesCPU::OpClc() {
	//REG_TMP_P &= ~CF;
	REG_TMP_P &= ~CF;
}

void NesCPU::OpCld() {
	//REG_TMP_P &= ~DF;
	REG_TMP_P &= ~DF;
}


void NesCPU::OpClv() {
	//REG_TMP_P &= ~VF;
	REG_TMP_P &= ~VF;
}

void NesCPU::OpSec() {
	//REG_TMP_P |= CF;
	REG_TMP_P |= CF;
}

void NesCPU::OpSed() {
	//REG_TMP_P |= DF;
	REG_TMP_P |= DF;
}

void NesCPU::OpSei() {
	REG_TMP_P |= IF;
	//REG_TMP_IF = 0xFF;
}

void NesCPU::OpPha() {
	PushByte(REG_A);
}

void NesCPU::OpPhp() {
	PushByte(REG_P | BF);
}

void NesCPU::OpPla() {
	REG_A = this->PopByte();
	ASET_NZ(REG_A);
}

void NesCPU::OpPlp() {
	REG_TMP_P = PopByte() & 0xef; //消除BF的影响
	REG_TMP_P |= UF;
}

void NesCPU::OpCpx() {
	GET_ADDRESS();
	READ_BYTE();
	this->MathCMP2(REG_X, this->mDataByte);
}

void NesCPU::OpCpy() {
	GET_ADDRESS();
	READ_BYTE();
	this->MathCMP2(REG_Y, this->mDataByte);
}


void NesCPU::OpIny() {
	REG_Y += 1;
	ASET_NZ(REG_Y);
}


inline uint8_t NesCPU::ReadByteForPc() {
    return famicom->GetDevices()->bus()->ReadByte(REG_PC++);
}

inline uint16_t NesCPU::ReadWordForPc() {
    uint16_t val = famicom->GetDevices()->bus()->ReadWord(REG_PC);
	REG_PC += 2;
	return val;
}

inline void NesCPU::PushByte(uint8_t val) {
    famicom->GetDevices()->bus()->WriteByte(0x100 | REG_SP, val);
	REG_SP--;
}

inline void NesCPU::PushWord(uint16_t val) {
	PushByte(val >> 8);
	this->PushByte(val & 0xff);
}

inline uint8_t NesCPU::PopByte() {
	++REG_SP;
    return famicom->GetDevices()->bus()->ReadByte(0x100 | REG_SP);
}

inline uint16_t NesCPU::PopWord() {
	return (uint16_t)PopByte() | ((uint16_t)PopByte() << 8);
}


inline uint8_t NesCPU::GetOpcode() {
    return famicom->GetDevices()->bus()->ReadByte(REG_PC);
}

void NesCPU::MathAdd(uint8_t op2) {
	uint8_t op1 = REG_A;
	uint16_t val = op1 + op2 + (REG_TMP_P & 0x01);

	if (((op1 ^ val) & (op2 ^ val)) & 0x80)
		REG_TMP_P |= VF;
	else
		REG_TMP_P &= ~VF;

	//判断进位
	if (val & 0xFF00)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	ASET_NZ(val);

	REG_A = val & 0xff;
}

void NesCPU::MathSub(uint8_t op2) {
	uint8_t op1 = REG_A;
	short val = REG_A - op2 - (1 - (REG_TMP_P & 0x01));

	//判断溢出两个操作数  符号不同时，如果结果符号和和操作数1不同，则为溢出
	if (((op1 ^ op2) & (op1 ^ val)) & 0x80)
		REG_TMP_P |= VF;
	else
		REG_TMP_P &= ~VF;

	//借位是0表示借位
	if ((val & 0xff00) == 0)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	REG_A = val & 0xff;

	ASET_NZ(REG_A);
}

void NesCPU::MathBit(uint8_t op1) {
	if (op1 & 0x80)
		REG_TMP_P |= NF;
	else
		REG_TMP_P &= ~NF;

	if (op1 & 0x40)
		REG_TMP_P |= VF;
	else
		REG_TMP_P &= ~VF;
	// qDebug("%x",REG_A ^ op1);
	if ((REG_A & op1) == 0)
		REG_TMP_P |= ZF;
	else
		REG_TMP_P &= ~ZF;
}

uint8_t NesCPU::MathASL(uint8_t op1) {
	uint8_t val = op1 << 1;

	//结果影响的标志位
	if (op1 & 0x80)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	ASET_NZ(val);

	return val;
}

uint8_t NesCPU::MathLSR(uint8_t op1) {
	uint8_t val = op1 >> 1;

	//结果影响的标志位
	if (op1 & 0x01)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	ASET_NZ(val);

	return val;
}

uint8_t NesCPU::MathROL(uint8_t op1) {
	uint8_t val = op1 << 1;

	if (CF_SET) //如果设置了CF位,则CF放在最后一位
		val |= 0x01;

	//结果影响的标志位
	if (op1 & 0x80)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	ASET_NZ(val);

	return val;
}

uint8_t NesCPU::MathROR(uint8_t op1) {
	uint8_t val = op1 >> 1;

	if (CF_SET) //如果设置了CF位,则CF放在最后一位
		val |= 0x80;

	//结果影响的标志位
	if (op1 & 0x01)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	ASET_NZ(val);

	return val;
}

void NesCPU::MathCMP2(uint8_t op1, uint8_t op2) {
	short val = op1 - op2;
	//判断借位 第一位小于第二位，最高位一定会发生借位
	if (val >= 0)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	ASET_NZ(val);

}


inline void NesCPU::_addrAbsX() {
	this->mDataAddress = this->ReadWordForPc() + REG_X;
}

inline void NesCPU::_addrAbsY() {
	this->mDataAddress = this->ReadWordForPc() + REG_Y;
}

inline void NesCPU::_addrAbs() {
	this->mDataAddress = this->ReadWordForPc();
}

inline void NesCPU::_addrInd() {
	uint16_t address = this->ReadWordForPc();
	uint16_t newaddress;
	if ((address & 0xff) == 0xff) //模拟NES无法跨页的BUG
	{
        uint8_t b1 = famicom->GetDevices()->bus()->ReadByte(address);
        uint8_t b2 = famicom->GetDevices()->bus()->ReadByte(address & 0xff00);
		newaddress = (b2 << 8) | b1;
	}
	else {
        newaddress = famicom->GetDevices()->bus()->ReadWord(address);
	}
	this->mDataAddress = newaddress;
}

/// <summary>
/// 相对寻址
/// </summary>
inline void NesCPU::_addrRel() {
	auto val = (int8_t)this->ReadByteForPc();//有符号数
	this->mDataAddress = REG_PC + val;
}


/// <summary>
/// 零页寻址
/// </summary>
inline void NesCPU::_addrZp() {
	this->mDataAddress = this->ReadByteForPc() & 0x00ff;
}

/// <summary>
/// 零页Y变址
/// </summary>
inline void NesCPU::_addrZpy() {
	//模拟 BUG ，不能 大于8位数
	this->mDataAddress = (uint16_t)(this->ReadByteForPc() + REG_Y) & 0xff;
}

/// <summary>
/// 零页X变址
/// </summary>
inline void NesCPU::_addrZpx() {
	//模拟 BUG ，不能 大于8位数
	this->mDataAddress = (uint16_t)(this->ReadByteForPc() + REG_X) & 0xff;
}

/// <summary>
/// 累加器寻址，不应该被调用
/// </summary>
inline void NesCPU::_addrAcc() {
}

/// <summary>
/// 立即数寻址
/// </summary>
inline void NesCPU::_addrImm() {
	this->mDataAddress = REG_PC++;
}


inline void NesCPU::_addrIzx() {
	uint8_t val = this->ReadByteForPc();
	// 模拟另一个bug
	uint8_t address = (uint16_t)val + REG_X;

	//uint16_t newaddress;

	//这里不能直接读取world 为的是模拟NES无法 跨页的BUG
    uint8_t b1 = famicom->GetDevices()->bus()->ReadByte(address & 0xff);
    uint8_t b2 = famicom->GetDevices()->bus()->ReadByte((address + 1) & 0xff);

	uint16_t newaddress = (b2 << 8) | b1;
	this->mDataAddress = newaddress;
}

inline void NesCPU::_addrIzy() {
	uint8_t val = this->ReadByteForPc();
    uint8_t b1 = famicom->GetDevices()->bus()->ReadByte(val & 0xff);
    uint8_t b2 = famicom->GetDevices()->bus()->ReadByte((val + 1) & 0xff);
	uint16_t address = (b2 << 8) | b1;
	uint16_t newaddress = (uint16_t)REG_Y + address;
	this->mDataAddress = newaddress;
}

void NesCPU::Save(SaveBundle* bundle) {

	bundle->SaveBlock((uint8_t*)&mRegs, sizeof(mRegs));

	bundle->SaveByte(mInterruptFlag);
	bundle->SaveByte(mNmiTiggle);
	bundle->SaveByte(mIrqTiggle);
}

void NesCPU::Restore(SaveBundle* bundle) {
	bundle->RestoreBlock((uint8_t*)&mRegs, sizeof(mRegs));

	mInterruptFlag = bundle->RestoreByte();
	mNmiTiggle = bundle->RestoreByte();
	mIrqTiggle = bundle->RestoreByte();
}

