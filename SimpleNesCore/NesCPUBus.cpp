#include "common.h"
#include "NesCPUBus.h"
#include "Famicom.h"
#include "Devices.h"

NesCPUBus::NesCPUBus(Famicom * fc)
{
	this->famicom = fc;
}

/// <summary>
/// 向CPU总线写入一个字节
/// </summary>
/// <param name="address"></param>
/// <param name="data"></param>
void NesCPUBus::WriteByte(uint16_t address, uint8_t data)
{
	switch (address >> 13)
	{
	case 0: //0~1FFF
		this->mMainRam[address & 0x07FF] = data;
		break;
	case 1: //2000~3FFF
        famicom->GetDevices()->ppu()->RegWriteViaCpu(address, data);
		break;
	case 2://4000~5FFF
		if (address < 0x4020) {
			if (address == 0x4014)
                famicom->GetDevices()->ppu()->RegWriteViaCpu(address, data);
			else if (address == 0x4016) //此处的4017是apu的寄存器
                famicom->GetDevices()->pad()->WriteRegViaCpu(address, data);
			else
                famicom->GetDevices()->apu()->WriteRegViaCpu(address, data);
		}
		else {
            famicom->GetDevices()->cartridge()->Write6000(address, data);
		}
		break;
	case 3://6000~7FFF
        famicom->GetDevices()->cartridge()->Write6000(address, data);
		break;
	default://8000~FFFF
        famicom->GetDevices()->cartridge()->WriteViaCPU(address, data);
		break;
	}

}

/// <summary>
/// 向CPU总线写入一个字
/// </summary>
/// <param name="address"></param>
/// <param name="data"></param>
void NesCPUBus::WriteWord(uint16_t address, uint16_t data)
{
	WriteByte(address, data & 0xFF);
	WriteByte(address + 1, (data >> 8) & 0xFF);
}

/// <summary>
/// 从CPU总线读取一个字节
/// </summary>
/// <param name="address"></param>
/// <returns></returns>
uint8_t NesCPUBus::ReadByte(uint16_t address)
{
	switch (address >> 13)
	{
	case 0: //0~1FFF
		return this->mMainRam[address & 0x07FF];
		break;
	case 1: //2000~3FFF
        return famicom->GetDevices()->ppu()->RegReadViaCpu(address);
		break;
	case 2://4000~5FFF 
		if (address < 0x4020) {
			if (address == 0x4014)
                return famicom->GetDevices()->ppu()->RegReadViaCpu(address);
			else if (address == 0x4016 || address == 0x4017)
                return famicom->GetDevices()->pad()->ReadRegViaCpu(address);
			else
                return famicom->GetDevices()->apu()->ReadRegViaCpu(address);
		}
		else {
            return famicom->GetDevices()->cartridge()->Read6000(address);
		}
		break;
	case 3://6000~7FFF
        return famicom->GetDevices()->cartridge()->Read6000(address);
		break;
	default://8000~FFFF
        return famicom->GetDevices()->cartridge()->ReadViaCPU(address);
		break;
	}

}

/// <summary>
/// 从CPU总线读取一个字
/// </summary>
/// <param name="address"></param>
/// <returns></returns>
uint16_t NesCPUBus::ReadWord(uint16_t address)
{
	return (ReadByte(address + 1) << 8 | (ReadByte(address) & 0x00ff)) & 0xFFFF;
}

/// <summary>
/// 从CPU总线复制指定字节的数据
/// </summary>
/// <param name="dst">目标缓冲指针</param>
/// <param name="src">CPU总线地址</param>
/// <param name="size">要复制的数据长度</param>
void NesCPUBus::CpuMemcpy(uint8_t* dst, uint16_t src, int size)
{
	for (size_t i = 0; i < size; i++)
	{
		*dst++ = ReadByte(src++);
	}
}

void NesCPUBus::Save(SaveBundle* bundle)
{
	bundle->SaveBlock(mMainRam, sizeof(mMainRam));
}

void NesCPUBus::Restore(SaveBundle* bundle)
{
	bundle->RestoreBlock(mMainRam, sizeof(mMainRam));
}
