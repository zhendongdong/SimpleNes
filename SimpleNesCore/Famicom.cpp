#include "common.h"
#include "Famicom.h"
#include "Devices.h"

#ifndef __GNUC__
#include "atlbase.h"
#include "atlstr.h"
#endif // __GNUC__ 

//Famicom* famicom;


void DebugPrintf(const char* strOutputString, ...) {
#ifndef __GNUC__ 
	char strBuffer[1024] = { 0 };
	va_list vlArgs;
	va_start(vlArgs, strOutputString);
	_vsnprintf_s(strBuffer, sizeof(strBuffer) - 1, strOutputString, vlArgs);
	va_end(vlArgs);
	OutputDebugString(CA2W(strBuffer));
#endif
}




Famicom::Famicom():mDevices(new Devices(this))
{
	this->saveBundle = new SaveBundle(this);
}

Famicom::~Famicom()
{
    delete mDevices;
	delete this->saveBundle;
}

void Famicom::UserInput(uint8_t index, uint8_t state) const
{
    mDevices->pad()->UserInput(index, state);
}

uint8_t* Famicom::GetCanvas() const
{
    return mDevices->ppu()->GetCanvas();
}

int16_t* Famicom::GetSamples(uint32_t* size) const
{
    return mDevices->apu()->GetSamples(size);
}

bool Famicom::LoadFcRom(famicom_rom_t * rom)
{
    if (mDevices->cartridge()->LoadRom(rom)) {
		this->Reset();
		return true;
	}
	return false;
}



#define MASTER_CYCLE_PER_SCANLINE 1364 //每个扫描线所使用的主时钟数
#define MASTER_CYCLE_PER_CPU 12 //CPU一个周期所使用的时钟数（相当于主时钟）
#define MASTER_CYCLE_PER_PPU 4 //CPU一个周期所使用的时钟数（相当于主时钟）


inline void Famicom::ExecCPU(const int masterCycle) {
	mMasterClockCycle += masterCycle;
	//主时钟除以12 减去已经执行的CPU周期，则为本轮应该执行的CPU周期
	uint32_t cpuCyclethisRound = mMasterClockCycle / MASTER_CYCLE_PER_CPU - mCpuCycleCount;
	//执行CPU并且累加实际执行的CPU周期
    mCpuCycleCount += mDevices->cpu()->Exec(cpuCyclethisRound);
}

void Famicom::EmulationFrame() {
    if (!mDevices->cartridge()->IsLoadedRom())
		return;
	//初始化一些变量
    mDevices->ppu()->PreEmulationFrame();
	for (size_t line = 0; line < 262; line++) {
		if (line >= 0 && line < 240)// 0~239 可见扫描线
		{
            mDevices->ppu()->RenderScanline(line);
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 256);
            if (mDevices->ppu()->mPPUMask & (BGEN | SPEN)) {
                mDevices->ppu()->IncVerticalPosition(); //每个扫描线的256点进行一些修改
                mDevices->ppu()->CopyHorizontalBits(); //每个扫描线的257点进行一些修改
			}
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 10);
            mDevices->cartridge()->Hsync(line);
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 75);;
		}
		else if (line == 240)    // 空白扫描线
		{
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 256);
            mDevices->cartridge()->Hsync(line);
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 85);

            mDevices->ppu()->mPPUStatus |= PPU_VBLANK;//空白扫描线之后打开VBLANK
            if (mDevices->ppu()->mPPUCtrl & NMI_EN)
                mDevices->cpu()->TryNmi(); //触发不可屏蔽中断
		}
		else if (line == 261) {
			//结束VBLANK
            mDevices->ppu()->mPPUStatus &= ~PPU_VBLANK;
            mDevices->ppu()->mPPUStatus &= ~PPU_SP0HIT;

			this->ExecCPU(MASTER_CYCLE_PER_PPU * 246);
            if (mDevices->ppu()->mPPUMask & (BGEN | SPEN)) {
				//这里必须先让CPU执行一段时间
				//否则机械战警和赤色要塞会出现画面上下偏移
                mDevices->ppu()->CopyVerticalBits();
			}
			//这里也必须先执行一段时间，否则魂斗罗力量会出现问题
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 10);
            mDevices->cartridge()->Hsync(line);
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 85);
		}
		else
		{
			//20条VBLANK扫描线
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 256);
            mDevices->cartridge()->Hsync(line);
			this->ExecCPU(MASTER_CYCLE_PER_PPU * 85);
		}
		//触发一次帧计数器，总共触发4次 这样1s就是240HZ(60*4)
		if (line % 65 == 64) {
            mDevices->apu()->ProcessFrameCounter();
			//DebugPrintf("ProcessFrameCounter %d\n", line);
		}
		//音频采样
		if (line % 87 == 86) {
            mDevices->apu()->MakeSamples(mCpuCycleCount - mLastCpuCycleCount);
			mLastCpuCycleCount = mCpuCycleCount;
		}
	}

	//归零已经执行的CPU周期数
	mLastCpuCycleCount = 0;
	mMasterClockCycle = 0; //所使用的时钟总数
	mCpuCycleCount = 0; //所使用的时钟总数

}
/*
void Famicom::EmulationFrame() const
{
	if (!this->cartridge->loadedrom())
		return;
	return this->ppu->EmulationFrame();
}*/

void Famicom::RenderPattern(uint8_t* mem) const
{
    return mDevices->ppu()->RenderPattern(mem);
}

void Famicom::Save() const {
    if (!mDevices->cartridge()->IsLoadedRom() || saveBundle == nullptr)
		return;

	if (saveBundle->Open(true)) {
        mDevices->cpu()->Save(saveBundle);
        mDevices->ppu()->Save(saveBundle);
        mDevices->bus()->Save(saveBundle);
        mDevices->pad()->Save(saveBundle);
        mDevices->cartridge()->Save(saveBundle);
        mDevices->apu()->Save(saveBundle);
	}
	saveBundle->Close();
}
void Famicom::Restore() const {
    if (!mDevices->cartridge()->IsLoadedRom() || saveBundle == nullptr)
		return;
	if (saveBundle->Open(false)) {
        mDevices->cpu()->Restore(saveBundle);
        mDevices->ppu()->Restore(saveBundle);
        mDevices->bus()->Restore(saveBundle);
        mDevices->pad()->Restore(saveBundle);
        mDevices->cartridge()->Restore(saveBundle);
        mDevices->apu()->Restore(saveBundle);
	}
	saveBundle->Close();
	//saveBundle.ResetRestore();//恢复后必须重设内部指针
}

void Famicom::Reset() const
{
    mDevices->cartridge()->Reset();
    mDevices->pad()->Reset();
    mDevices->cpu()->Reset();
    mDevices->ppu()->Reset();
    mDevices->apu()->Reset();
	//saveBundle.Reset();
}


void Famicom::SetSavebundle(SaveBundle* isave)
{
    this->saveBundle = isave;
}

void Famicom::SetFcEvent(IFcEvent* event)
{
	this->ifcevent = event;
}

bool Famicom::CreateFcRom(famicom_rom_t** rom, void* data,size_t size)
{
	if (rom == nullptr)
		return false;
    return mDevices->cartridge()->CreateFcRom(rom,data,size);
}
void Famicom::ReleaseFcRom(famicom_rom_t* rom)
{
	if (rom == nullptr)
		return;
    return mDevices->cartridge()->ReleaseFcRom(rom);
}
