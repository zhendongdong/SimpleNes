#pragma once
#include "savebundle.h"


#define REG_A this->mRegs.A
#define REG_X this->mRegs.X
#define REG_Y this->mRegs.Y
#define REG_P this->mRegs.P
#define REG_TMP_P this->mRegs._P
#define REG_SP this->mRegs.SP
#define REG_PC this->mRegs.PC

#define ASET_NF(var) do{if((var) & 0x80) \
                    REG_TMP_P |= NF; \
                    else \
                    REG_TMP_P &= ~NF;}while(0);

#define ASET_ZF(var) do{if(!((var) & 0xff)) \
                    REG_TMP_P |= ZF; \
                    else \
                    REG_TMP_P &= ~ZF;}while(0);

#define ASET_NZ(var) {if((var) & 0x80) \
                    REG_TMP_P |= NF; \
                    else \
                    REG_TMP_P &= ~NF;\
                    if(!((var) & 0xff)) \
                    REG_TMP_P |= ZF; \
                    else \
                    REG_TMP_P &= ~ZF;}

#define ASET_CF(var) {if(var >> 7) \
                    REG_TMP_P |= CF; \
                    else \
                    REG_TMP_P &= ~CF;}



#define BF_SET (REG_P & BF)
#define IF_SET (REG_P & IF)
#define CF_SET (REG_P & CF)
#define ZF_SET (REG_P & ZF)
#define NF_SET (REG_P & NF)
#define VF_SET (REG_P & VF)

#define IS_FLAG_SET(var) (REG_P & var)
#define IS_FLAG_UNSET(var) (!(REG_P & var))

#define CPUBUS_READ_BYTE(address) famicom->GetDevices()->bus()->ReadByte(address)

#define GET_ADDRESS() (this->*(mPtrOpcode->address))()
#define READ_BYTE() this->mDataByte = famicom->GetDevices()->bus()->ReadByte(this->mDataAddress)
#define WRITE_BYTE(VAL) famicom->GetDevices()->bus()->WriteByte(this->mDataAddress,VAL)


class Famicom;
//namespace famicom_cpu {
//	
//}
class NesCPU
{


	enum Flags
	{
		CF = 1 << 0, // Carry
		ZF = 1 << 1, // Zero
		IF = 1 << 2, // Disable interrupt
		DF = 1 << 3, // Decimal Mode ( unused in pNes )
		BF = 1 << 4, // Break
		UF = 1 << 5, // Unused ( always 1 )
		VF = 1 << 6, // Overflow
		NF = 1 << 7, // Negative
	};

	
private:

	Famicom * famicom;

	typedef struct
	{
		void (NesCPU::* handler)();
		void (NesCPU::* address)();
		uint8_t cycle;
		uint8_t morecycle;
		const char* name;
	}OpInfo;



	struct
	{
		uint16_t PC;
		uint8_t SP;
		uint8_t P;
		uint8_t _P; //临时P标志 
		uint8_t A;
		uint8_t X;
		uint8_t Y;
	}mRegs;

	int mDeferCycles = 0;
	//Registers reg;

	uint16_t mThisPC;      // 当前指令的地址（指令所在的地址）

	uint16_t mDataAddress; // 存放当前指令获取数据的地址(根据寻址方式计算指令后1~2两个字节得到)
	uint8_t mDataByte;     // 根据当前指令地址获取的字节
	 
	uint8_t mInterruptFlag = 0;
    OpInfo mOpcodes[256] = {{0}};
	uint8_t mOpcode = 0;
	OpInfo * mPtrOpcode = nullptr;
	uint8_t mNmiTiggle = 0;
	uint8_t mIrqTiggle = 0;

public:

	
	NesCPU(Famicom *fc);

	int32_t Exec(int32_t requestCycles);

	void (* Idisassembly)(char * str) = nullptr;

	int  Disassembly(uint8_t* prgrom, uint16_t pcaddr, uint16_t realaddr, uint8_t opcode, OpInfo* opInfo) const;
	void step();
	void Reset(); //复位中断
	void IRQ();
	void TryNmi();
	//中断
	void NMI();
	void TryIrq();
	void TryIrq(uint8_t tiggle);
	void ClrIrq();
	void ClrIrq(uint8_t tiggle);

	uint8_t ReadByteForPc();
	uint16_t ReadWordForPc();
	void PushByte(uint8_t val);
	void PushWord(uint16_t val);
	uint8_t PopByte();
	uint16_t PopWord();

	

	uint8_t GetOpcode();

	void MathAdd(uint8_t op2);
	void MathSub(uint8_t op2);
	void MathBit(uint8_t op1);
	uint8_t MathASL(uint8_t op1);
	uint8_t MathLSR(uint8_t op1);
	uint8_t MathROL(uint8_t op1);
	uint8_t MathROR(uint8_t op1);

	void MathCMP2(uint8_t op1, uint8_t op2);


	void OpAdc();
	void OpSbc1();
	void OpInc();
	void OpInx();
	void OpIny();
	void OpDec();
	void OpDex();
	void OpDey();
	void OpAnd();
	void OpEor();
	void OpOra();
	void OpBit();
	void OpAsl_acc();
	void OpAsl();
	void OpLsr_acc();
	void OpLsr();
	void OpRol_acc();
	void OpRol();
	void OpRor_acc();
	void OpRor();
	void OpLda();
	void OpSta();
	void OpStx();
	void OpSty();

	void OpTax();
	void OpTay();
	void OpTsx();
	void OpTxa();
	void OpTxs();
	void OpTya();



	void OpLdx();
	void OpLdy();
	void OpCmp();

	void OpCpx();
	void OpCpy();

	void OpBcc();
	void OpBcs();
	void OpBeq();
	void OPUNK();
	void OpBmi();
	void OpBne();
	void OpBpl();
	void OpBvc();
	void OpBvs();

	void OpBrk();
	void OpJmp();
	void OpJsr();
	void OpRti();
	void OpRts();


	void OpClc();
	void OpCld();
	void OpCli();
	void OpClv();
	void OpSec();
	void OpSed();
	void OpSei();

	void OpPha();
	void OpPhp();
	void OpPla();
	void OpPlp();







	void OpSlo();
	void OpNop();
	void _opNop();
	void OpRla();
	void OpSre();
	void OpRra();
	void OpSax();
	void OpLax();
	void OpDcp();
	void OpIsc();
	void OpAnc();
	void OpAlr();
	void OpArr();
	void OpXaa();
	void OpAxs();
	void OpSbc();
	void OpAhx();
	void OpShy();
	void OpShx();
	void OpTas();
	void OpLas();

	void _addrInd();
	void _addrRel();
	void _addrAcc();
	void _addrZp();
	void _addrZpy();
	void _addrZpx();
	void _addrImm();
	void _addrIzx();
	void _addrIzy();

	void _addrAbs();
	void _addrAbsX();
	void _addrAbsY();


	void Save(SaveBundle* bundle);
	void Restore(SaveBundle* bundle);
};

