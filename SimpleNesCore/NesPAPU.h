#pragma once
#include "savebundle.h"
class Famicom;
class NesPAPU
{

    typedef struct sample_t{
		short buf[800];
        short size = 0;
	}Sample;

	/// <summary>
	/// 包络
	/// </summary>
	typedef struct {
		uint8_t     divider;// 时钟分频器
		uint8_t     counter;// 计数器

		uint8_t     reload; //是否重载
		uint8_t     ctrl6;// 控制器低6位
	} Envelope;

	/// <summary>
	/// 噪音数据
	/// </summary>
	typedef struct {
		uint32_t cycle;
		uint8_t volume;
		uint8_t bitsRemaining;
		uint32_t period;
		uint8_t enable;
		Envelope  envelope;// 包络
		uint16_t  lfsr;// 线性反馈移位寄存器
		uint8_t  lengthCounter;// 长度计数器
		uint8_t  shortMode;// 短模式D7
		uint8_t periodIndex;//周期索引 D0~D3
	} Noise;

	/// <summary>
	/// DMC
	/// </summary>
	typedef struct {
		uint8_t enable;
		uint16_t orgaddr;		// 原始地址
		uint16_t address;		// 当前地址
		uint16_t length;		// 原始长度
		uint16_t bytesRemaining;		// 剩余长度
		uint16_t period;		// 周期
		uint16_t clock;		// CPU时钟
		uint8_t  volume;		// 输出
		uint8_t  irqEnable;		// 中断
		uint8_t  irqLoop;		// 中断[D1]/循环[D0]
		uint8_t  bitsRemaining;		// 8步计数
		uint8_t  sampleBuffer;		// 字节数据[8字节位移寄存器]
		uint8_t  interruptFlag;// 字节数据[8字节位移寄存器]
	} Dmc;

	/// <summary>
	/// 三角波寄存器/数据
	/// </summary>
	typedef struct {
		uint8_t enable;
		uint32_t cycle;
		// 当前周期
		uint16_t period;
		// 长度计数器
		uint8_t lengthCounter;
		// 线性计数器
		uint8_t linearCounter;
		// 线性计数器 重载值
		uint8_t reloadValue;
		// 线性计数器 重载标志
		uint8_t reload;
		// 长度计数器/线性计数器暂停值
		uint8_t flagHalt;
		uint8_t seqIndex;		// 当前序列索引
		uint8_t volume; //音量
		uint8_t incMask; //音量
	} Triangle;

	/// <summary>
	/// 线性扫描单元结构体
	/// </summary>
	typedef struct {
		uint8_t enable;   //扫描单元使能 E
		uint8_t period;   //扫描单元周期（分频） PPP
		uint8_t negative; //是否负向扫描 N
		uint8_t shift;    //扫描单元位移次数(位移位数) SSS
		uint8_t divider;
		uint8_t reload;
	}Sweep;

	/// <summary>
	/// 方波结构体
	/// </summary>
	typedef struct {
		uint8_t enable;
		//$4001/$4005 EPPPNSSS
		Sweep sweep;
		Envelope envelope;

		//声道周期 11BIT  $4002/$4006 D7~D0 低8位+
		//$4003/$4007 D2~D0 高3位
		uint16_t period;
		uint16_t curTimer;
		uint8_t lengthCounter;//长度计数器 5BIT
		//$4000	DDLC NNNN
		uint8_t ctrl;//占空比 D
		uint8_t seqIndex; //序列索引
		uint8_t volume; //音量
		uint32_t cycle;
	}Pulse;


	enum {
		DMCEnable = 1 << 4,      // DMC使能
		NoiseEnable = 1 << 3,    // 噪声使能
		TriangleEnable = 1 << 2, // 三角波使能
		Pulse2Enable = 1 << 1,   // 方波2使能
		Pulse1Enable = 1         // 方波1使能
	};

private:
	//制作音频采集样本

	enum {
		NTSC_CPU_RATE = 1789773,
		SAMPLES_PER_SEC = 44100,
		SAMPLES_PER_FRAME = SAMPLES_PER_SEC / 60
	};

	const float mCpuCyclePerSample
		= (float)NTSC_CPU_RATE
		/ (float)SAMPLES_PER_SEC;

	uint32_t mQueueOffset = 0;
	uint32_t sbuffer_index = 0;

    Pulse mPulses[2] = { {0} };
	Triangle mTriangle = { 0 }; //三角波
	Noise mNoise = { 0 }; //噪音
	Dmc mDmc = { 0 }; //噪音



	int8_t mOutputs[5] = { 0 };

	void ProcessEnvelope();
	static void ProcessEnvelope(Envelope* envelope);
	void ProcessSweepUnit();
	static void ProcessPulseSweepUnit(Pulse* pulse, bool isOne);
	void ProcessLengthCounter();
	static void ProcessPulseLengthCounter(Pulse* pulse);
	void ProcessLinearCounter();

	void UpdateTriangleState();
	void UpdatePulseState(Pulse* pulse);
	void UpdateNoiseState();
	static uint16_t lfsrChange(uint16_t v, uint8_t c);
    int16_t SoundMixer();

	void ResetDMC();

	void UpdateDmcBit(Dmc * dmc);
	
	uint8_t MakePulseSamples(Pulse* pulse);
	uint8_t MakeTriangleSamples(Triangle* triangle);
	uint8_t MakeNoiseSamples(Noise* noise);
	uint8_t MakeDmcSamples(Dmc* dmc);
	Famicom * famicom;
public:

	NesPAPU(Famicom* fc);

	uint8_t mFrameClock = 0;
	uint8_t mFrameInterrupt = 0;
	//uint32_t mFrameCounter = 0;
	uint8_t channelCtrl = 0;
	uint8_t mFrame5Step = 0;
	uint8_t mIRQDisable = 0;
    Sample mSamplesQueue[20];
	void Reset();

	void WriteRegViaCpu(uint16_t address, uint8_t sampleBuffer);
	uint8_t ReadRegViaCpu(uint16_t address);
	void ProcessFrameCounter();
	uint8_t MakeSamples(uint32_t cpuCycle);
	uint8_t MakeSamples();
	int GetSamplesQueue();
	int16_t* GetSamples(uint32_t* size);

	void Save(SaveBundle* bundle);
	void Restore(SaveBundle* bundle);
};

