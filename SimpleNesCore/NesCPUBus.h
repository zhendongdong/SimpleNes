#pragma once
#include "common.h"
#include "savebundle.h"

class Famicom;
class NesCPUBus
{
private:
    uint8_t mMainRam[2048] = { 0 }; //2KB�ڴ�
    Famicom * famicom;
public:
    NesCPUBus(Famicom * fc);
    void WriteByte(uint16_t address, uint8_t sampleBuffer);
    void WriteWord(uint16_t address, uint16_t sampleBuffer);
    uint8_t ReadByte(uint16_t address);
    uint16_t ReadWord(uint16_t address);
    void CpuMemcpy(uint8_t* dst, uint16_t src, int size);
    void Save(SaveBundle* bundle);
    void Restore(SaveBundle* bundle);
};

