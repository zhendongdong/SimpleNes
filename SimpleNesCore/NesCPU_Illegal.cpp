#include "common.h"
#include "NesCPU.h"
#include <cassert>
#include "Famicom.h"
#include "Devices.h"

void NesCPU::OpSlo()
{
	GET_ADDRESS();
	READ_BYTE();
	if ((mDataByte >> 7) & 1)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	mDataByte <<= 1;
	REG_A |= mDataByte;
	WRITE_BYTE(mDataByte);

	ASET_NZ(REG_A);
}
void NesCPU::OpNop()
{
	GET_ADDRESS();
	READ_BYTE();
	//DebugPrintf("OpNop %x %x\n", mThisPC, mOpode);
}

void NesCPU::_opNop()
{
	//空操作
}

void NesCPU::OpRla()
{
	GET_ADDRESS();
	READ_BYTE();

	uint8_t val = mDataByte << 1;

	if (IS_FLAG_SET(CF)) //如果设置了CF位,则CF放在最后一位
		val |= 0x01;

	//结果影响的标志位
	if (mDataByte & 0x80)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	REG_A &= val;

	WRITE_BYTE(val);

	ASET_NZ(REG_A);
}

void NesCPU::OpSre() {

	GET_ADDRESS();
	READ_BYTE();

	uint8_t val = mDataByte >> 1;

	if (mDataByte & 1)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	REG_A ^= val;

	WRITE_BYTE(val);
	ASET_NZ(REG_A);
}
void NesCPU::OpRra() {
	GET_ADDRESS();
	READ_BYTE();
	uint8_t val = mDataByte >> 1;

	if (IS_FLAG_SET(CF)) //如果设置了CF位,则CF放在最后一位
		val |= 0x80;

	if (mDataByte & 0x01)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	WRITE_BYTE(val);

	this->MathAdd(val);

	ASET_NZ(REG_A);
}

void NesCPU::OpSax() {
	GET_ADDRESS();
	READ_BYTE();
	uint8_t val = REG_A & REG_X;
	WRITE_BYTE(val);

}

void NesCPU::OpLax() {
	GET_ADDRESS();
	READ_BYTE();
	REG_A = REG_X = mDataByte;
	ASET_NZ(REG_A);
}
void NesCPU::OpDcp() {
	GET_ADDRESS();
	READ_BYTE();
	uint8_t val = --mDataByte;

	WRITE_BYTE(val);

	short val2 = REG_A - val;

	if (val2 >= 0)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	ASET_NZ(val2 & 0xff);

}
void NesCPU::OpIsc() {
	GET_ADDRESS();
	READ_BYTE();
	uint8_t val = ++mDataByte;
	WRITE_BYTE(val);
	this->MathSub(val);

}
void NesCPU::OpAnc() {
	GET_ADDRESS();
	READ_BYTE();
	REG_A &= mDataByte;
	ASET_NZ(REG_A);
	if (IS_FLAG_SET(NF))
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;
}
void NesCPU::OpAlr() {
	//assert(!"no  implement");
}
void NesCPU::OpArr() {
	GET_ADDRESS();
	READ_BYTE();
	REG_A &= mDataByte;

	REG_A = REG_A >> 1;

	if (IS_FLAG_SET(CF))
		REG_A |= 0x80;

	ASET_NZ(REG_A);
	if ((REG_A >> 6) & 1)
		REG_TMP_P |= CF;
	else
		REG_TMP_P &= ~CF;

	if ((REG_A >> 5) & (IS_FLAG_SET(CF) ? 1 : 0))
		REG_TMP_P |= VF;
	else
		REG_TMP_P &= ~VF;


}
void NesCPU::OpXaa() {
	//assert(!"no  implement");
}
void NesCPU::OpAxs() {
	GET_ADDRESS();
	READ_BYTE();
	REG_A = REG_X & mDataByte;
}
void NesCPU::OpSbc() {
	GET_ADDRESS();
	READ_BYTE();
	this->MathSub(mDataByte);
}
void NesCPU::OpAhx() {
	//assert(!"no  implement");
}
void NesCPU::OpShy() {
	//assert(!"no  implement");
}
void NesCPU::OpShx() {
	//assert(!"no  implement");
}

void NesCPU::OpTas() {
	//assert(!"no  implement");
}
void NesCPU::OpLas() {
	//assert(!"no  implement");
}
