#pragma once
#include <stdint.h>
class Mapper;
typedef struct FAMICOM_ROM {
	uint8_t prg16kbCount; //程序块的数量
	uint8_t chr8kbCount; //图形块的数量

	uint32_t prgRomSize = 0;
	uint32_t chrRomSize = 0;

	uint8_t* prgrom = nullptr;
	uint8_t* chrrom = nullptr;

	uint8_t version;

	uint8_t    mapperNumber;	// Mapper 编号
	bool     isVmirroring;	// 是否Vertical Mirroring(否即为水平)
	bool     isFourScreen;	// 是否FourScreen
	bool     hasBatteryBacked;	// 是否有SRAM(电池供电的)
	bool     hasTrainer;
	uint32_t   size;
	bool   isChrRAM;
	Mapper* mapper = nullptr;
} famicom_rom_t;

enum class Mirror {
	SINGLE_SCREEN_LOWER = 0,
	SINGLE_SCREEN_UPPER = 1,
	VERTICAL = 2,
	HORIZONTAL = 3,
	FOUR_SCREEN = 4
};
