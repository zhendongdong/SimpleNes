#pragma once
#include "savebundle.h"

class NesGamePad
{
private:
    //D7表示是否选通（0 选通，1重置
    uint8_t mGamepadEn = 0;
    uint8_t mGamepadIndex_1 = 0;
    uint8_t mGamepadIndex_2 = 0;
    uint8_t mGamepadStatus[16] = {0};
public:

    NesGamePad();
    void UserInput(uint8_t index, unsigned state);
    void WriteRegViaCpu(uint16_t address, uint8_t data);
    uint8_t ReadRegViaCpu(uint16_t address);
    void Reset();
    void Save(SaveBundle* bundle);
    void Restore(SaveBundle* bundle);
};

