#pragma once
#include "savebundle.h"
#include "Rom.h"

static const uint8_t gBitReverseTable256[] = {
	  0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0,
	  0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8,
	  0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4,
	  0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC,
	  0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2,
	  0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
	  0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6,
	  0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
	  0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
	  0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9,
	  0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
	  0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
	  0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,
	  0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
	  0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7,
	  0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF
};
enum PPU_CTRL_Flags
{
	VINC32 = 1 << 2,
	SPUSE1000 = 1 << 3,
	BGUSE1000 = 1 << 4,
	SPH16 = 1 << 5, //精灵是否16像素高
	NMI_EN = 1 << 7
};
enum PPU_STATUS_Flags
{
	PPU_SPOVER = 1 << 5, //精灵溢出标志位
	PPU_SP0HIT = 1 << 6, //精灵0命中标志位
	PPU_VBLANK = 1 << 7  //VBLANK标志位
};

enum PPU_MASK_Flags
{
	BGEN = 1 << 3,
	SPEN = 1 << 4
};
class Famicom;
class NesPPU
{
private:
	Famicom * famicom;
	uint8_t mPreviousData = 0;
public:

	/*D1 D0	确定当前使用的名称表	配合微调滚动使用
-	D2	PPU读写显存增量	0(+1 列模式) 1(+32 行模式)
-	D3	精灵用图样表地址	0($0000) 1($1000)
-	D4	背景用图样表地址	0($0000) 1($1000)
-	D5	精灵尺寸(高度)	0(8x8) 1(8x16)
-	D6	PPU 主/从模式	FC没有用到
-	D7	NMI生成使能标志位	1(在VBlank时触发NMI)*/
	uint8_t mPPUCtrl = 0;

	/*-	D0	显示模式	0(彩色) 1(灰阶)
	-	D1	背景掩码	0(不显示最左边那列, 8像素)的背景
	-	D2	精灵掩码	0(不显示最左边那列, 8像素)的精灵
	-	D3	背景显示使能标志位	1(显示背景)
	-	D4	精灵显示使能标志位	1(显示精灵)
	NTSC	D5 D6 D7	颜色强调使能标志位	5-7分别是强调RGB
	PAL	D5 D6 D7	颜色强调使能标志位	5-7分别是强调GRB
	*/
	uint8_t mPPUMask = 0;

	
	/*
	 *  -	D5	精灵溢出标志位	0(当前扫描线精灵个数小于8)
		-	D6	精灵命中测试标志位	1(#0精灵命中) VBlank之后置0
		-	D7	VBlank标志位	VBlank开始时置1, 结束或者读取该字节($2002)后置0
	*/
	uint8_t mPPUStatus = 0;


	NesPPU(Famicom * fc);


	uint8_t* GetCanvas();

	void IncVerticalPosition();

	void IncHorizontalPosition();

	void CopyHorizontalBits();

	void CopyVerticalBits();
	void RenderPattern(uint8_t* PatternMem);
	void PreEmulationFrame();
	// QPaintDevice* qPaintDevice;


	
	uint8_t RegReadViaCpu(uint16_t address);
	void RegWriteViaCpu(uint16_t address, uint8_t sampleBuffer);
	void Reset();
	void SwitchMirroring(Mirror mode);
	void Save(SaveBundle* bundle);
	void Restore(SaveBundle* bundle);

	void RenderScanline(const uint16_t line);
private:
	typedef struct {
		uint8_t spCount;
		uint8_t spirits[8];
	}Scanline;

    typedef struct spirit_t{
        uint8_t id = 0; //X坐标
        uint8_t x = 0; //X坐标
	//	uint8_t y; //Y坐标
        uint16_t pattern = 0; //图样表地址
	//	uint8_t tile; //图样表编号
        uint8_t info = 0; //优先级、是否反转以及 颜色表高2位
	//	uint8_t offset;//当前扫描线偏移
	}Spirit;

	/*
	* V寄存器
	yyy NN YYYYY XXXXX
	||| || ||||| +++++-- coarse X scroll
	||| || +++++-------- coarse Y scroll
	||| ++-------------- nametable select
	+++----------------- fine Y scroll
	*/
    typedef struct REG_T{
        uint16_t v = 0;    // 当前VRAM寄存器（渲染时则为上面注释的作用）
        uint16_t t = 0;    // 临时VRAM寄存器
        uint8_t w = 0; // 双写寄存器
        uint8_t x = 0;     // 精细X滚动寄存器
	}REG;

    REG mRegs;

	uint8_t mReadBuffer = 0; //缓存数据
	
	uint8_t mScreenMemory[256 * 240];            //画布 用于保存渲染数据
	
	
	uint8_t mOAM[256] = { 0 };             //精灵内存
	uint8_t mSpHeight = 8;                 //精灵高度
	uint8_t mOamaddr;                      //精灵写入地址
    Spirit mSpirits[64];         //临时保存扫描线上的精灵

	uint8_t mScanlineSpiritsCount = 0;     //当前扫描线上精灵的数量

	uint16_t mPatternTables = 0;           //指定的背景图样起始地址
	uint16_t mSpPatternTables;
	uint8_t mBGMask[33 + 1] = { 0 };       //当前扫描线背景透明掩码


    uint8_t mVRAM[2][1024] = { {0} };         //2KB板载显存
	uint8_t* mNametables[4] = { nullptr };  //4个命名表
	uint8_t mPalette[32] = { 0 };           //调色板

	void ScanSpirits(uint16_t line);


	void DrawSpirits(uint16_t line);
	//void drawBackground(uint16_t nametables, uint16_t patterntables, int baseX, int baseY);


	void DrawSpiritsPixelUnit(
		uint8_t p1, uint8_t p2,
		uint8_t x, uint8_t y,uint8_t attr
	);

	void DrawPixelUnit(uint8_t p1, uint8_t p2, uint8_t attrl2bit, uint8_t* pCanvas, int x);
	
	void WriteByte(uint16_t address, uint8_t sampleBuffer);
	uint8_t ReadByte(uint16_t address);
	uint8_t ReadByteViaCPU(uint16_t address);

	
};


