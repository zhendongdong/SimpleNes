#include "common.h"
#include "NesPAPU.h"
#include "Famicom.h"
#include "Devices.h"

// REF: http://nesdev.com/apu_ref.txt


//方波占空比
#if 1
static const uint8_t PL_SEQ[4][8] = {
	{0,1,0,0,0,0,0,0},
	{0,1,1,0,0,0,0,0},
	{0,1,1,1,1,0,0,0},
	{1,0,0,1,1,1,1,1},
};
static const uint8_t TRI_SEQ[] = {
	15, 14, 13, 12, 11, 10,  9,  8,
	7,  6,  5,  4,  3,  2,  1,  0,
	0,  1,  2,  3,  4,  5,  6,  7,
	8,  9, 10, 11, 12, 13, 14, 15
};
#else
static const uint8_t PL_SEQ[4][8] = {
	{-1,1,-1,-1,-1,-1,-1,-1},
	{-1,1,1,-1,-1,-1,-1,-1},
	{-1,1,1,1,1,-1,-1,-1},
	{1,-1,-1,1,1,1,1,1},
};
static const uint8_t TRI_SEQ[] = {
	15, 14, 13, 12, 11, 10,  9,  8,
	7,  6,  5,  4,  3,  2,  1,  0,
	0,  1,  2,  3,  4,  5,  6,  7,
	8,  9, 10, 11, 12, 13, 14, 15
};
#endif



// 长度计数器映射表
static const uint8_t LENGTH_COUNTER_TABLE[] = {
	0x0A, 0xFE, 0x14, 0x02,
	0x28, 0x04, 0x50, 0x06,
	0xA0, 0x08, 0x3C, 0x0A,
	0x0E, 0x0C, 0x1A, 0x0E,
	0x0C, 0x10, 0x18, 0x12,
	0x30, 0x14, 0x60, 0x16,
	0xC0, 0x18, 0x48, 0x1A,
	0x10, 0x1C, 0x20, 0x1E,
};



static const uint16_t DMC_PERIOD_LIST_NP[] = {
	// NTSC
	428, 380, 340, 320, 286, 254, 226, 214, 190, 160, 142, 128, 106, 84, 72, 54
};

static const uint16_t NOISE_PERIOD_LIST[] = {
	4, 8, 16, 32, 64, 96, 128, 160, 202, 254, 380, 508, 762, 1016, 2034, 4068,
};
NesPAPU::NesPAPU(Famicom* fc) :famicom(fc)
{
}
void NesPAPU::WriteRegViaCpu(uint16_t address, uint8_t data)
{
	//DebugPrintf("APU::%x %x\n", address, data);
	switch (address)
	{
	case 0x4000://方波1 寄存器1
	case 0x4004://方波1 寄存器1
	{
		Pulse* pulse = &mPulses[address == 0x4000 ? 0 : 1];
		pulse->ctrl = data;
		//pulse->Volume = data & 0x0f;
		pulse->envelope.ctrl6 = data & 0x3f;
	}
	break;
	case 0x4001://方波1 寄存器2
	case 0x4005://方波2 寄存器2
	{
		Pulse* pulse = &mPulses[address == 0x4001 ? 0 : 1];
		pulse->sweep.enable = data >> 7;
		pulse->sweep.period = (data >> 4) & 0x7;
		pulse->sweep.negative = (data >> 3) & 0x1;
		pulse->sweep.shift = data & 0x7;
		pulse->sweep.reload = 1;
	}
	break;
	case 0x4002://方波1 寄存器3
	case 0x4006://方波2 寄存器3
	{
		Pulse* pulse = &mPulses[address == 0x4002 ? 0 : 1];
		pulse->period &= 0xff00;
		pulse->period |= data;
	}
	break;
	case 0x4003://方波1 寄存器4
	case 0x4007://方波2 寄存器4
	{
		Pulse* pulse = &mPulses[address == 0x4003 ? 0 : 1];
		pulse->period &= 0xff;
		pulse->period |= ((data & 0x7) << 8);
		//方波使能才重置长度计数器
		if (pulse->enable)
			pulse->lengthCounter = LENGTH_COUNTER_TABLE[data >> 3];
		pulse->envelope.reload = 1;
		pulse->seqIndex = 0;
	}
	break;
	case 0x4008:
		// $4008 CRRR RRRR
		mTriangle.reloadValue = data & 0x7F;
		mTriangle.flagHalt = data >> 7;
		break;
	case 0x4009:
		break;
	case 0x400A:
		// $400A TTTT TTTT
		mTriangle.period = (mTriangle.period & 0xff00) | data;
		break;
	case 0x400B:
		// $400B LLLL LTTT
		if (mTriangle.enable)		// 禁止状态不会重置
			mTriangle.lengthCounter = LENGTH_COUNTER_TABLE[data >> 3];
		mTriangle.period = (mTriangle.period & 0x00ff) | ((data & 0x07) << 8);
		mTriangle.reload = 1;
		break;
	case 0x400C:
		// $400C --LC NNNN 
		mNoise.envelope.ctrl6 = data & 0x3F;
		break;
	case 0x400D:
		break;
	case 0x400E:
		// $400E S--- PPPP
		mNoise.shortMode = data >> 7;
		mNoise.periodIndex = data & 0xf;
		break;
	case 0x400F:
		// $400E LLLL L---
		// 禁止状态不会重置
		if (mNoise.enable)
			mNoise.lengthCounter = LENGTH_COUNTER_TABLE[data >> 3];
		mNoise.envelope.reload = 1;
		break;
	case 0x4010:
		// $4010 IL-- RRRR
		mDmc.irqEnable = (data >> 7) & 0x1;
		mDmc.irqLoop = (data >> 6) & 0x1;
		mDmc.period = DMC_PERIOD_LIST_NP[data & 0xF];
		mDmc.clock = 0;
		if (!mDmc.irqEnable) {
			mDmc.interruptFlag = false;
            famicom->GetDevices()->cpu()->ClrIrq(IRQ_DMC_INT);
		}
		break;
	case 0x4011:
		// $4011 -DDD DDDD
		mDmc.volume = data & 0x7F;
		break;
	case 0x4012:
		// $4012 AAAA AAAA
		// 11AAAAAA.AA000000
		mDmc.orgaddr = 0xC000 | ((uint16_t)data << 6);
		break;
	case 0x4013:
		// $4013 LLLL LLLL
		// 0000LLLL.LLLL0001
		mDmc.length = ((uint16_t)data << 4) | 1;
		break;
	case 0x4015:
	{
		this->channelCtrl = data;
		if (channelCtrl & Pulse1Enable) {
			mPulses[0].enable = 1;
		}
		else {
			mPulses[0].lengthCounter = 0;
			mPulses[0].volume = 0;
			mPulses[0].enable = 0;
		}

		if (channelCtrl & Pulse2Enable) {
			mPulses[1].enable = 1;
		}
		else {
			mPulses[1].lengthCounter = 0;
			mPulses[1].volume = 0;
			mPulses[1].enable = 0;
		}
		if (channelCtrl & TriangleEnable)
		{
			mTriangle.enable = 1;
			mTriangle.volume = 1;
		}
		else {
			mTriangle.enable = 0;
			mTriangle.volume = 0;
			mTriangle.lengthCounter = 0;
		}
		if (channelCtrl & NoiseEnable)
		{
			mNoise.enable = 1;
		}
		else {
			mNoise.enable = 0;
			mNoise.volume = 0;
			mNoise.lengthCounter = 0;
		}

		if (channelCtrl & DMCEnable)
		{
			mDmc.enable = 1;
			//重置（重启）DMC
			ResetDMC();
		}
		else {
			mDmc.enable = 0;
			mDmc.bytesRemaining = 0;
            famicom->GetDevices()->cpu()->ClrIrq(IRQ_DMC_INT);
		}
		mDmc.interruptFlag = false;
	}
	break;
	case 0x4017://帧计数器(Frame Counter) 
	{
		//DebugPrintf("0x4017 address %x\n", data);
		mFrame5Step = data >> 7;  //是否五步模式
		mIRQDisable = !!(data & 0x40);

		if (mIRQDisable) {
            famicom->GetDevices()->cpu()->ClrIrq(IRQ_FRAME_INT);
			mFrameInterrupt = 0;
		}

		if (data == 0)
		{
			mFrameInterrupt = 1;
            famicom->GetDevices()->cpu()->TryIrq(IRQ_FRAME_INT);
		}

		if (mFrame5Step)
		{
			ProcessLengthCounter();
			ProcessSweepUnit();
			ProcessEnvelope();
			ProcessLinearCounter();
		}
	}
	break;
	default:
		DebugPrintf("unknown address %x\n", address);
		break;
	}
}

void NesPAPU::Reset() {
	mNoise.lfsr = 1;
	mDmc.period = DMC_PERIOD_LIST_NP[0];
}

uint8_t NesPAPU::ReadRegViaCpu(uint16_t address)
{
	if (address == 4015)
	{
		uint8_t state = 0;
		if (mPulses[0].lengthCounter)
			state |= 0x1;
		if (mPulses[1].lengthCounter)
			state |= 0x2;
		if (mTriangle.lengthCounter)
			state |= 0x4;
		if (mNoise.lengthCounter)
			state |= 0x8;
		if (mDmc.bytesRemaining)
			state |= 0x10;
		if (mFrameInterrupt)
			state |= 0x40;
		if (mDmc.interruptFlag)
			state |= 0x80;


		// 清除中断标记
		mFrameInterrupt = 0;
        famicom->GetDevices()->cpu()->ClrIrq(IRQ_FRAME_INT);

		return state;
	}
	return 0;
}



//int16_t sbuffer[10 * 800];



int remnantsCycle = 0;


inline void NesPAPU::ProcessSweepUnit() {
	ProcessPulseSweepUnit(&mPulses[0], true);
	ProcessPulseSweepUnit(&mPulses[1], false);
}


void NesPAPU::ProcessFrameCounter()
{
	//FrameCounter++;
	if (mFrame5Step) {
		// l - l - - ->   ++%5   ->   1 2 3 4 0
		// e e e e - ->   ++%5   ->   1 2 3 4 0
		switch (mFrameClock % 5)
		{
		case 0:
			ProcessEnvelope();
			ProcessLinearCounter();
			break;
		case 1:
			ProcessLengthCounter(); //120hz
			ProcessSweepUnit();
			ProcessEnvelope();
			ProcessLinearCounter();
			break;
		case 2:
			ProcessEnvelope();
			ProcessLinearCounter();
			break;
		case 3:
			ProcessLengthCounter(); //120hz
			ProcessSweepUnit();
			ProcessEnvelope();
			ProcessLinearCounter();
			break;
		case 4:
			break;
		}
	}
	else {

		switch (mFrameClock % 4)
		{
		case 0:
			ProcessEnvelope();	//240HZ（伪）的包络信号
			ProcessLinearCounter();
			break;
		case 1:
			ProcessLengthCounter(); //120hz
			ProcessSweepUnit();
			ProcessEnvelope();	//240HZ（伪）的包络信号
			ProcessLinearCounter();
			break;
		case 2:
			ProcessEnvelope();	//240HZ（伪）的包络信号
			ProcessLinearCounter();
			break;
		case 3:
			if (!mIRQDisable) {
				//DebugPrintf("IRQ_FRAME_INT\n");
				mFrameInterrupt = 1;
                famicom->GetDevices()->cpu()->TryIrq(IRQ_FRAME_INT);
			}
			ProcessLengthCounter(); //120hz
			ProcessSweepUnit();
			ProcessEnvelope();	//240HZ（伪）的包络信号
			ProcessLinearCounter();
			break;
		}
	}
	++mFrameClock;
}




uint16_t NesPAPU::lfsrChange(uint16_t v, uint8_t c) {
	const uint16_t bit = ((v >> 0) ^ (v >> c)) & 1;
	return (uint16_t)(v >> 1) | (uint16_t)(bit << 14);
}

int16_t NesPAPU::SoundMixer()
{
#if 1
	//混合
	float square_out = 95.88f / ((8128.f / (mOutputs[0] + mOutputs[1])) + 100.f);
	float tnd_out = 159.79f / (1.f / (mOutputs[2] / 8227.f + mOutputs[3] / 12241.f + mOutputs[4] / 22638.f) + 100.f);
	//DebugPrintf("square_out + tnt_output:%f\n", square_out + tnt_output);
	float outputf = (float)(square_out + tnd_out);
#else 
	//线性近似 
	float pulse_out = 0.00752 * ((int64_t)mOutputs[0] + mOutputs[1]);
	float tnd_out = 0.00851 * mOutputs[2] + 0.00494 * mOutputs[3] + 0.00335 * mOutputs[4];
	//if ((float)(pulse_out + tnd_out) > 1.f) 
	//{
	//	DebugPrintf("square_out + tnt_output:%f\n", pulse_out + tnd_out);
	//}
	float outputf = (float)(pulse_out + tnd_out);

#endif
	//int16_t output = outputf * 32767.f;
	int16_t output = ((outputf * 2.f) - 1.f) * 32767.f;
	//if (output > 0x7FFF)
	//	output = 0x7FFF;
	//else if (output < -0x8000)
	//	output = -0x8000;
	return output;
}

/// <summary>
/// 重置DMC
/// </summary>
void NesPAPU::ResetDMC() {
	mDmc.address = mDmc.orgaddr;
	mDmc.bytesRemaining = mDmc.length;
}

/// <summary>
/// 更新当前DMC播放的BIT
/// </summary>
void NesPAPU::UpdateDmcBit(Dmc* dmc) {
	if (!dmc->enable)
		return;
	//如果当前BIT已经播放完毕
	if (dmc->bitsRemaining <= 0) {
		//如果还有BYTE长度剩余
		if (dmc->bytesRemaining) {
			dmc->bitsRemaining = 8;
            dmc->sampleBuffer = famicom->GetDevices()->bus()->ReadByte(dmc->address);
			dmc->address = (uint16_t)(dmc->address + 1) | (uint16_t)0x8000;
			--dmc->bytesRemaining;
			//读取完毕
			if (dmc->bytesRemaining <= 0) {
				if (dmc->irqLoop) {
					//设置了循环播放的标志，则重置DMC
					ResetDMC();
				}
				else {
					if (dmc->irqEnable) {
						dmc->interruptFlag = true;
                        famicom->GetDevices()->cpu()->TryIrq(IRQ_DMC_INT);
					}
				}
			}
		}
	}
	if (dmc->bitsRemaining) {
		if (dmc->sampleBuffer & 1) {
			if (dmc->volume <= 125) dmc->volume += 2;
		}
		else {
			if (dmc->volume >= 2) dmc->volume -= 2;
		}
		// 位移寄存器
		dmc->sampleBuffer >>= 1;
		--dmc->bitsRemaining;
	}
}




/// <summary>
/// 方波采样
/// </summary>
/// <returns></returns>
uint8_t NesPAPU::MakePulseSamples(Pulse* pulse)
{
	pulse->cycle += mCpuCyclePerSample * 0.5f;
	const int cycle = (pulse->cycle / (pulse->period + 1));
	pulse->cycle -= cycle * (pulse->period + 1);
	pulse->seqIndex += cycle;
	pulse->seqIndex &= 7;
	return pulse->volume * (PL_SEQ[pulse->ctrl >> 6][pulse->seqIndex]);
}

/// <summary>
/// 三角波采样
/// </summary>
/// <param name="triangle"></param>
/// <returns></returns>
uint8_t NesPAPU::MakeTriangleSamples(Triangle* triangle)
{
	triangle->cycle += mCpuCyclePerSample;
	const uint16_t cycle = triangle->cycle / (triangle->period + 1);
	triangle->cycle -= cycle * (triangle->period + 1);
	const uint8_t inc = triangle->incMask & cycle;
	//const uint8_t inc = (uint8_t)count;
	triangle->seqIndex += inc;
	triangle->seqIndex &= 31;

	return triangle->volume * TRI_SEQ[triangle->seqIndex];
}

/// <summary>
/// 噪声采样
/// </summary>
/// <param name="noise"></param>
/// <returns></returns>
uint8_t NesPAPU::MakeNoiseSamples(Noise* noise) {
	const uint16_t cycle = mCpuCyclePerSample;
	noise->cycle += cycle;
	const uint16_t period = mNoise.period;
	while (noise->cycle >= period) {
		noise->cycle -= period;
		noise->lfsr = lfsrChange(noise->lfsr, mNoise.bitsRemaining);
	}
	// 为0输出
	uint8_t mask = noise->lfsr & 1;
	--mask;
	return noise->volume & mask;
}

/// <summary>
/// DMC采样
/// </summary>
/// <param name="dmc"></param>
/// <returns></returns>
uint8_t NesPAPU::MakeDmcSamples(Dmc* dmc)
{
	dmc->clock += mCpuCyclePerSample;
	const uint16_t period = dmc->period;
	while (dmc->clock >= period) {
		dmc->clock -= period;
		UpdateDmcBit(dmc);
	}
	return dmc->volume;
}

uint8_t NesPAPU::MakeSamples() 
{
	return this->MakeSamples(famicom->mCpuCycleCount);
}
uint8_t NesPAPU::MakeSamples(uint32_t cpuCycle)
{

	UpdatePulseState(&mPulses[0]);
	UpdatePulseState(&mPulses[1]);
	UpdateTriangleState();
	UpdateNoiseState();
	//if(X2A)
	mQueueOffset = mQueueOffset % 20;
#if 0
	const uint32_t nSampleSize = (remnantsCycle + cpuCycle) / mCpuCyclePerSample;
	remnantsCycle = (remnantsCycle + cpuCycle) - nSampleSize * mCpuCyclePerSample;
	Sample* sample = &mSamplesQueue[mQueueOffset++];
	sample->size = nSampleSize;
#else
	const uint32_t nSampleSize = 245;
	Sample* sample = &mSamplesQueue[mQueueOffset++];
	sample->size = nSampleSize;
#endif
	//float * buffer = 
	for (size_t i = 0; i < nSampleSize; i++)
	{
		this->mOutputs[0] = this->MakePulseSamples(&mPulses[0]);
		this->mOutputs[1] = this->MakePulseSamples(&mPulses[1]);
		this->mOutputs[2] = this->MakeTriangleSamples(&mTriangle);
		this->mOutputs[3] = this->MakeNoiseSamples(&mNoise);
		this->mOutputs[4] = this->MakeDmcSamples(&mDmc);

		//非线性声道混合
		sample->buf[i] = this->SoundMixer();

	}
	if (famicom->ifcevent != nullptr)
		famicom->ifcevent->OnAudioWrite(sample->buf, sample->size);
	return 0;
}

/// <summary>
/// 更新三角波的状态
/// </summary>
void NesPAPU::UpdateTriangleState() {
	// mTriangle.volume = mTriangle.enable;
	// 递增掩码: 长度计数器/线性计数器 有效
	mTriangle.incMask = (mTriangle.lengthCounter && mTriangle.linearCounter) ? 0xff : 0;
}

/// <summary>
/// 更新脉冲（方波）的状态
/// </summary>
/// <param name="pulse"></param>
void NesPAPU::UpdatePulseState(Pulse* pulse) {
	// 使能
	if (!(pulse->enable)) {
		pulse->volume = 0;
		return;
	}
	// 长度计数器为0
	if (pulse->lengthCounter == 0)
	{
		pulse->volume = 0;
		return;
	}
	// 输出频率
	if (pulse->period < 8 || pulse->period > 0x7ff)
	{
		pulse->volume = 0;
	}
	// state.duty = famicom->apu.square1.ctrl >> 6;
	if (pulse->envelope.ctrl6 & 0x10)	// 固定音量
		pulse->volume = pulse->envelope.ctrl6 & 0xf;
	else	// 包络音量
		pulse->volume = pulse->envelope.counter;
}


/// <summary>
/// 更新噪音的状态
/// </summary>
void NesPAPU::UpdateNoiseState() {
	// 短模式
	mNoise.bitsRemaining = mNoise.shortMode ? 6 : 1;
	// 周期
	mNoise.period = NOISE_PERIOD_LIST[mNoise.periodIndex & 0xF];
	// 使能
	if (!mNoise.enable) {
		mNoise.volume = 0;
		return;
	}
	// 长度计数器有效
	if (!mNoise.lengthCounter) {
		mNoise.volume = 0;
		return;
	}
	// 固定音量
	if (mNoise.envelope.ctrl6 & 0x10)
		mNoise.volume = mNoise.envelope.ctrl6 & 0xf;
	else			// 包络音量
		mNoise.volume = mNoise.envelope.counter;


}

/// <summary>
/// 长度计数(组)
/// </summary>
inline void NesPAPU::ProcessLengthCounter()
{
	ProcessPulseLengthCounter(&mPulses[0]);
	ProcessPulseLengthCounter(&mPulses[1]);

	// 三角波 长度计数器
	if (!(mTriangle.flagHalt) && mTriangle.lengthCounter)
		--mTriangle.lengthCounter;

	// 噪音-- 长度计数器
	if (!(mNoise.envelope.ctrl6 & (uint8_t)0x20) && mNoise.lengthCounter)
		--mNoise.lengthCounter;

}

/// <summary>
/// 线性计数单元
/// </summary>
inline void NesPAPU::ProcessLinearCounter() {
	if (mTriangle.reload) {
		mTriangle.linearCounter = mTriangle.reloadValue;
	}
	else if (mTriangle.linearCounter) {
		--mTriangle.linearCounter;
	}
	if (!mTriangle.flagHalt)
		mTriangle.reload = 0;
}

/// <summary>
/// 包络
/// </summary>
inline void NesPAPU::ProcessEnvelope()
{
	ProcessEnvelope(&mPulses[0].envelope);
	ProcessEnvelope(&mPulses[1].envelope);
	ProcessEnvelope(&mNoise.envelope);
}

/// <summary>
/// 长度计数器单元
/// </summary>
/// <param name="pulse"></param>
void NesPAPU::ProcessPulseLengthCounter(Pulse* pulse) {
	if (!(pulse->ctrl & 0x20) && pulse->lengthCounter)
		pulse->lengthCounter--;
}

/// <summary>
/// 扫描单元
/// </summary>
/// <param name="pulse"></param>
/// <param name="isOne"></param>
void NesPAPU::ProcessPulseSweepUnit(Pulse* pulse, bool isOne)
{
	if (pulse->sweep.reload) {
		pulse->sweep.reload = 0;
		const uint8_t oldDivider = pulse->sweep.divider;
		pulse->sweep.divider = pulse->sweep.period;
		if (pulse->sweep.enable && !oldDivider)
			goto _sweep_reload;
		return;
	}

	if (!pulse->sweep.enable)
		return;


	if (pulse->sweep.divider)
		pulse->sweep.divider--;
	else {
		pulse->sweep.divider = pulse->sweep.period;
	_sweep_reload:

		if (!pulse->sweep.shift)
			return;

		uint16_t val1 = pulse->period >> pulse->sweep.shift;
		if (pulse->sweep.negative) {
			pulse->period -= val1;
			if (isOne)
				pulse->period -= 1; //额外减去1
		}
		else
			pulse->period += val1;
	}
}


/// <summary>
/// 包络单元
/// </summary>
/// <param name="envelope"></param>
void NesPAPU::ProcessEnvelope(Envelope* envelope) {
	// 写入了第四个寄存器(START 标记了)
	if (envelope->reload) {
		envelope->reload = 0;
		envelope->counter = 15;
		envelope->divider = envelope->ctrl6 & 0x0F;
	}
	// 对时钟分频器发送一个时钟信号
	else {
		if (envelope->divider)
			envelope->divider--;
		else {		// 时钟分频器输出一个信号
			envelope->divider = envelope->ctrl6 & 0x0F;
			//设置了循环包络位
			const uint8_t loop = envelope->ctrl6 & 0x20;
			if (envelope->counter | loop) {
				--envelope->counter;
				envelope->counter &= 0xf;
			}
		}
	}
}


int NesPAPU::GetSamplesQueue()
{
	return sbuffer_index - mQueueOffset;
}
int16_t* NesPAPU::GetSamples(uint32_t* size)
{
	if (sbuffer_index == mQueueOffset)
		return nullptr;

	sbuffer_index %= 20;
	*size = mSamplesQueue[sbuffer_index].size;
	return mSamplesQueue[sbuffer_index++].buf;
}

void NesPAPU::Save(SaveBundle* bundle)
{
	bundle->SaveBlock((uint8_t*)&mPulses[0], sizeof(Pulse));
	bundle->SaveBlock((uint8_t*)&mPulses[1], sizeof(Pulse));
	bundle->SaveBlock((uint8_t*)&mNoise, sizeof(Noise));
	bundle->SaveBlock((uint8_t*)&mTriangle, sizeof(Triangle));
	bundle->SaveBlock((uint8_t*)&mDmc, sizeof(Dmc));

	bundle->SaveByte(mFrameClock);
	bundle->SaveByte(mFrameInterrupt);
	bundle->SaveByte(channelCtrl);
	bundle->SaveByte(mFrame5Step);
	bundle->SaveByte(mIRQDisable);
}

void NesPAPU::Restore(SaveBundle* bundle)
{
	bundle->RestoreBlock((uint8_t*)&mPulses[0], sizeof(Pulse));
	bundle->RestoreBlock((uint8_t*)&mPulses[1], sizeof(Pulse));
	bundle->RestoreBlock((uint8_t*)&mNoise, sizeof(Noise));
	bundle->RestoreBlock((uint8_t*)&mTriangle, sizeof(Triangle));
	bundle->RestoreBlock((uint8_t*)&mDmc, sizeof(Dmc));

	mFrameClock = bundle->RestoreByte();
	mFrameInterrupt = bundle->RestoreByte();
	channelCtrl = bundle->RestoreByte();
	mFrame5Step = bundle->RestoreByte();
	mIRQDisable = bundle->RestoreByte();
}
