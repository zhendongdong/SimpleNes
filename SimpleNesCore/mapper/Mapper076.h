#pragma once
#include "Mapper.h"

class Mapper076 : public Mapper {
    //Rom* m_Rom;
    uint8_t *pPRGBlank[4] = {0}; //  8 KB * 4 $8000/$AFFF/$C000/$EFFF
    uint8_t *pCHRBlank[4] = {0}; //  2 KB * 4 $0000/$0800/$1000/$1800
    uint8_t map76_reg = 0;
public:

    Mapper076(Famicom* fc, famicom_rom_t* rom) :Mapper(fc, rom) {

    }
    virtual uint8_t ReadViaCPU(uint16_t address) override;;
    virtual void WriteViaCPU(uint16_t address, uint8_t sampleBuffer) override;
    virtual uint8_t ReadViaPPU(uint16_t address) override;
    virtual void OnReset() override;
};

