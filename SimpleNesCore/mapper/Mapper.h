#pragma once
#include "../common.h"
#include "../Rom.h"
#include "../savebundle.h"
#include "assert.h"
class Famicom;
class Mapper
{
protected:
	Famicom* famicom;
    famicom_rom_t* mROM;


	enum BANKSIZE {
		BANKSIZE_1KB = 1024,
		BANKSIZE_2KB = 2048,
		BANKSIZE_4KB = 4096,
        BANKSIZE_8KB =  8192,
		BANKSIZE_16KB = 16384,
		BANKSIZE_32KB = 32768
	};



	uint8_t* mPrgBank[4] = { nullptr };
	uint8_t* mChrBank[8] = { nullptr };

	uint8_t mSaveRAM[BANKSIZE_8KB] = { 0 };
	uint8_t mExRAM[BANKSIZE_4KB] = { 0 };
	uint8_t mBusRAM[512] = { 0 };


	/// <summary>
	/// 格式化Bank大小
	/// </summary>
	/// <param name="prgBlankSize">PRG BANK大小</param>
	/// <param name="chrBlankSize">CHR BANK大小</param>
	inline void FormatBank(uint16_t prgBankSize, uint16_t chrBankSize) {

	}

	/// <summary>
	/// 以8KB为单位 加载ROM到系统的Prg BLANK
	/// </summary>
	/// <param name="pos"></param>
	/// <param name="blank"></param>
	inline void SetCPUBank_8KB(const uint8_t pos, const uint8_t bank) {
		mPrgBank[pos] = mROM->prgrom + ((uint32_t)bank % (mROM->prg16kbCount << 1)) * BANKSIZE_8KB;
	}

	/// <summary>
	/// 以16KB为单位 加载ROM到系统的Prg BLANK
	/// </summary>
	/// <param name="pos"></param>
	/// <param name="blank"></param>
	inline void SetCPUBank_16KB(const uint8_t pos, const uint8_t bank) {
		uint8_t** prgbank = &mPrgBank[pos << 1];
		uint16_t bank8kb = ((uint32_t)bank % mROM->prg16kbCount) << 1;

		prgbank[0] = mROM->prgrom + (bank8kb + 0) * BANKSIZE_8KB;
		prgbank[1] = mROM->prgrom + (bank8kb + 1) * BANKSIZE_8KB;
	}

	/// <summary>
	/// 以32KB为单位 加载ROM到系统的Prg BLANK
	/// </summary>
	/// <param name="pos"></param>
	/// <param name="blank"></param>
	inline void SetCPUBank_32KB(const uint8_t bank) {
		uint16_t bank8kb = ((uint32_t)bank % (mROM->prg16kbCount >> 1)) << 2;

		mPrgBank[0] = mROM->prgrom + (bank8kb + 0) * BANKSIZE_8KB;
		mPrgBank[1] = mROM->prgrom + (bank8kb + 1) * BANKSIZE_8KB;
		mPrgBank[2] = mROM->prgrom + (bank8kb + 2) * BANKSIZE_8KB;
		mPrgBank[3] = mROM->prgrom + (bank8kb + 3) * BANKSIZE_8KB;
	}

	/// <summary>
	/// 以1KB为单位 加载ROM到系统的Chr BLANK
	/// </summary>
	/// <param name="pos"></param>
	/// <param name="blank"></param>
	inline void SetPPUBank_1KB(const uint8_t pos, const uint8_t bank) {
		mChrBank[pos] = mROM->chrrom + (((uint32_t)bank) * BANKSIZE_1KB);
	}

	/// <summary>
	/// 以2KB为单位 加载ROM到系统的Chr BLANK
	/// </summary>
	/// <param name="pos"></param>
	/// <param name="blank"></param>
    void SetPPUBank_2KB(const uint8_t pos, const uint8_t bank) {
        uint8_t** chrbank = mChrBank + (pos << 1);
        uint16_t bank1kb = ((uint32_t)bank % (mROM->chr8kbCount << 2)) << 1;

        chrbank[0] = mROM->chrrom + (bank1kb + 0) * BANKSIZE_1KB;
        chrbank[1] = mROM->chrrom + (bank1kb + 1) * BANKSIZE_1KB;
    }

	/// <summary>
	/// 以4KB为单位 加载ROM到系统的Chr BLANK
	/// </summary>
	/// <param name="pos"></param>
	/// <param name="blank"></param> 
	inline void SetPPUBank_4KB(const uint8_t pos, const uint8_t bank) {
		uint8_t** chrbank = mChrBank + (pos << 2);
		uint16_t bank1kb = ((uint32_t)bank % (mROM->chr8kbCount << 1)) << 2;

		chrbank[0] = mROM->chrrom + (bank1kb + 0) * BANKSIZE_1KB;
		chrbank[1] = mROM->chrrom + (bank1kb + 1) * BANKSIZE_1KB;
		chrbank[2] = mROM->chrrom + (bank1kb + 2) * BANKSIZE_1KB;
		chrbank[3] = mROM->chrrom + (bank1kb + 3) * BANKSIZE_1KB;
	}

	/// <summary>
	/// 以8KB为单位 加载ROM到系统的Chr BLANK
	/// </summary>
	/// <param name="pos"></param>
	/// <param name="blank"></param>
    void SetPPUBank_8KB(const uint8_t bank);
    virtual void OnReset();;
public:
    Mapper() = delete;
    Mapper(Famicom* fc, famicom_rom_t* rom) :famicom(fc), mROM(rom)
	{

	}
	virtual ~Mapper()
	{

	}

	virtual void Hsync(uint32_t line) {

	}
    void Reset();;



    virtual void Write6000(uint16_t address, uint8_t data);;
	/// <summary>
	/// 实际上的地址范围是0x4020~0x7FFF
	/// </summary>
	/// <param name="address"></param>
	/// <returns></returns>
    virtual  uint8_t Read6000(uint16_t address);;

	/// <summary>
	/// CPU$8000-$FFFF地址空间写入
	/// </summary>
	/// <param name="address"></param>
	/// <param name="data"></param>
    virtual void WriteViaCPU(uint16_t address, uint8_t data);;

	/// <summary>
	/// CPU$8000-$FFFF地址空间读取
	/// </summary>
	/// <param name="address"></param>
	/// <returns></returns>
	virtual  uint8_t ReadViaCPU(uint16_t address) {
		return mPrgBank[(address & 0x7FFF) >> 13][address & 0x1FFF];
	};


    virtual uint8_t ReadViaPPU(uint16_t address);;


    virtual void WriteViaPPU(uint16_t address, uint8_t data);;

    void Save(SaveBundle* bundle);

    void Restore(SaveBundle* bundle);

    virtual void OnSave(SaveBundle* bundle);

    virtual void OnRestore(SaveBundle* bundle);
};
