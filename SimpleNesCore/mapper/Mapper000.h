#pragma once
#include "Mapper.h"
#include "../NesCartridge.h"
#include <assert.h>

/// <summary>
/// 测试游戏：
/// 超级马里奥
/// 坦克大战
/// 西游记(日版)
/// 
/// 备注：
/// 基本实现，神马都没得 但是游戏很经典耐玩
/// </summary>
class Mapper000 :public Mapper
{
	
public:

    Mapper000(Famicom *fc,famicom_rom_t* rom):Mapper(fc,rom)
	{

	}

    void WriteViaCPU(uint16_t address, uint8_t data)override;
    virtual void Write6000(uint16_t address, uint8_t data) override;
    virtual uint8_t Read6000(uint16_t address) override;
    virtual void OnSave(SaveBundle* bundle) override;
    virtual void OnRestore(SaveBundle* bundle) override;
};

