#pragma once
#include "Mapper.h"

/// <summary>
/// 
/// </summary>
class Mapper004 :public Mapper
{
private:
    uint8_t mRegid = 0;
    uint8_t mPrmode = 0;
    uint8_t mCrmode = 0;
    uint8_t mRegs[8] = { 0,2,4,5,6,7,0,1 };
    uint8_t latch = 0xFF;
    uint8_t counter = 0;
    uint8_t mIrqEnable = 0;
    uint8_t mIrqReload = 0;

public:

    Mapper004(Famicom* fc, famicom_rom_t* rom) :Mapper(fc, rom) {
	

	}
    virtual void OnReset() override;
    virtual void Hsync(uint32_t line) override;
    void SelectBank(uint8_t data);
    void UpdateBankData(uint8_t bank);
    void UpdateCPUBanks();
    void UpdatePPUBanks();
    void UpdateMirroring(uint8_t data);
    virtual void WriteViaCPU(uint16_t address, uint8_t data) override;
    virtual void OnSave(SaveBundle* bundle) override;
    virtual void OnRestore(SaveBundle* bundle) override;

};
