#pragma once
#include "Mapper.h"
#include "../Rom.h"
/// <summary>
/// ������Ϸ��
/// ˫����1
/// ѩ���ֵ�
/// �������ս 
/// �ж�ս��(����)
/// ��еս��
/// </summary>
class Famicom;
class Mapper001 :public Mapper
{
private:
	uint8_t mShifter = 0x10; //5BITλ�ƼĴ���
	uint8_t mControl = 0;
	uint8_t mPRGMode = 0;
	uint8_t mIsCHR8kb = FALSE;
	uint16_t mLastWriteAddr;

	enum {
		MMC1_1024K,
		MMC1_512K,
		MMC1_SMALL
	};
private:
    void UpdatePrgbank();
    void WriteRegister(uint16_t address);
    void UpdateControl(uint8_t data);
    void UpdateChrbank0();
    void UpdateChrbank1();

public:
    Mapper001(Famicom* fc, famicom_rom_t* rom) :Mapper(fc, rom)
	{

	}
    virtual void OnReset()override;
    virtual void WriteViaCPU(uint16_t address, uint8_t data) override;;
    virtual void OnSave(SaveBundle* bundle) override;
    virtual void OnRestore(SaveBundle* bundle) override;
};

