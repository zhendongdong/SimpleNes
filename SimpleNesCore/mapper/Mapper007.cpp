#include "Mapper007.h"
#include "../Famicom.h"
#include "../Devices.h"
/// <summary>
/// 测试游戏：
/// 时间统治者 未通过测试
/// 
/// 备注：
/// </summary>

Mapper007::Mapper007(Famicom *fc, famicom_rom_t *rom) : Mapper(fc, rom) {}

 void Mapper007::OnReset() {
  SetCPUBank_32KB(0);
  SetPPUBank_8KB(0);
  famicom->GetDevices()->ppu()->SwitchMirroring(Mirror::SINGLE_SCREEN_LOWER);
}

 void Mapper007::WriteViaCPU(uint16_t address, uint8_t data){
  SetCPUBank_32KB(data & 0x07);
  if (data & 0x10)
    famicom->GetDevices()->ppu()->SwitchMirroring(Mirror::SINGLE_SCREEN_UPPER);
  else
    famicom->GetDevices()->ppu()->SwitchMirroring(Mirror::SINGLE_SCREEN_LOWER);
}
