#include "Mapper002.h"
void Mapper002::WriteViaPPU(uint16_t address, uint8_t data) {
    Mapper::WriteViaPPU(address, data);
}

void Mapper002::WriteViaCPU(uint16_t address, uint8_t data) {
    SetCPUBank_16KB(0, data);
}

void Mapper002::OnSave(SaveBundle *bundle) {
    if (mROM->isChrRAM) {
        bundle->SaveBlock(mROM->chrrom, 8192);
    }
}

void Mapper002::OnRestore(SaveBundle *bundle) {
    if (mROM->isChrRAM) {
        bundle->RestoreBlock(mROM->chrrom, 8192);
    }
}
