#include "Mapper001.h"
#include "../Rom.h"
#include "../Famicom.h"
#include "../Devices.h"

/// <summary>
/// 测试游戏：
/// 双截龙1
/// 雪人兄弟
/// 松鼠大作战 
/// 中东战争(美版)
/// 机械战警
/// </summary>

void Mapper001::OnReset() {
    //CMapper::Reset();

    mShifter = 0x10;
    mControl = 0;
    mPRGMode = 0;
    mIsCHR8kb = false;


    SetCPUBank_16KB(0, 0);
    SetCPUBank_16KB(1, mROM->prg16kbCount - 1);

    SetPPUBank_4KB(0, 0);
    SetPPUBank_4KB(1, 1);
}

void Mapper001::UpdateControl(uint8_t data) {
    this->mControl = data;
    switch (mControl & 0x3)
    {
    case 0x0:
        famicom->GetDevices()->ppu()->SwitchMirroring(Mirror::SINGLE_SCREEN_LOWER);
        break;
    case 0x1:
        famicom->GetDevices()->ppu()->SwitchMirroring(Mirror::SINGLE_SCREEN_UPPER);
        break;
    case 0x2:
        famicom->GetDevices()->ppu()->SwitchMirroring(Mirror::VERTICAL);
        break;
    case 0x3:
        famicom->GetDevices()->ppu()->SwitchMirroring(Mirror::HORIZONTAL);
        break;
    }

    mPRGMode = (mControl >> 2) & 0x3;
    mIsCHR8kb = !((mControl >> 4) & 0x1);
}

void Mapper001::UpdateChrbank0()
{
    if (mIsCHR8kb)
    {
        //const uint8_t bank = mShifter;//8KB模式最大支持16个
        SetPPUBank_4KB(0, mShifter);
        SetPPUBank_4KB(1, mShifter + 1);
    }
    else
        SetPPUBank_4KB(0, mShifter);
}

void Mapper001::UpdateChrbank1()
{
    if (mIsCHR8kb)return; //8K模式忽略此寄存器
    SetPPUBank_4KB(1, mShifter);
}

void Mapper001::UpdatePrgbank()
{
#if 0
    DebugPrintf("UpdatePrgbank %x %x\n", mPRGMode, mShifter);
#endif

    if (mPRGMode & 0x2)
    {
        if (mPRGMode & 0x1)
        {
            // 16K of Rom at $8000
            SetCPUBank_16KB(0, mShifter);
            SetCPUBank_16KB(1, mROM->prg16kbCount - 1);
        }
        else {
            // 16K of Rom at $C000
            SetCPUBank_16KB(0, 0);
            SetCPUBank_16KB(1, mShifter);
        }
    }
    else {
        // 32K of Rom at $8000
        SetCPUBank_16KB(0, mShifter);
        SetCPUBank_16KB(1, mShifter + 1);
    }
}

void Mapper001::WriteRegister(uint16_t address)
{
    switch ((address & 0x7FFF) >> 13)
    {
    case 0:
        UpdateControl(mShifter);
        break;
    case 1:
        UpdateChrbank0();
        break;
    case 2:
        UpdateChrbank1();
        break;
    case 3:
        UpdatePrgbank();
        break;
    }
}

void Mapper001::WriteViaCPU(uint16_t address, uint8_t data) {

    //当本次写入的地址与上次不同时需要重置位移寄存器
    //否则雪人兄弟等游戏会出现问题
    if ((address & 0x6000) != (mLastWriteAddr & 0x6000))
    {
        mShifter = 0x10;
        UpdateControl(mControl | 0x0c);
    }

    mLastWriteAddr = address; //保存地址

    if (data & 0x80) {
        mShifter = 0x10;
        UpdateControl(mControl | 0x0c);
    }
    else {// D7 = 0 -> 写入D0到移位寄存器
        const uint8_t finished = mShifter & 1;
        mShifter >>= 1;
        mShifter |= (data & 1) << 4;
        if (finished) {
            WriteRegister(address);
            mShifter = 0x10;
        }
    }
}

void Mapper001::OnSave(SaveBundle *bundle)
{
    bundle->SaveByte(mShifter);
    bundle->SaveByte(mControl);
    bundle->SaveByte(mPRGMode);
    bundle->SaveByte(mIsCHR8kb);
    bundle->SaveWord(mLastWriteAddr);
}

void Mapper001::OnRestore(SaveBundle *bundle)
{
    mShifter = bundle->RestoreByte();
    mControl = bundle->RestoreByte();
    mPRGMode = bundle->RestoreByte();
    mIsCHR8kb = bundle->RestoreByte();
    mLastWriteAddr = bundle->RestoreWord();
}
