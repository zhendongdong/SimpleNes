#pragma once
#include "Mapper.h"
class Mapper066 :public Mapper
{
public:
    Mapper066(Famicom* fc, famicom_rom_t* rom) :Mapper(fc, rom) {


    }
    void OnReset()override;
    virtual void Write6000(uint16_t address, uint8_t data) override;
    virtual void WriteViaCPU(uint16_t address, uint8_t data) override;
};
