#pragma once
#include "Mapper.h"
#include "Mapper000.h"
#include "Mapper001.h"
#include "Mapper002.h"
#include "Mapper003.h"
#include "Mapper004.h"
#include "Mapper007.h"
#include "Mapper066.h"
#include "Mapper074.h"
#include "Mapper076.h"
#include "Mapper245.h"


class MapperFactory {
public:
    static Mapper* Factory(Famicom * famicom, famicom_rom_t *rom)
	{
		switch (rom->mapperNumber)
		{
		case 0:
			return (Mapper*)new Mapper000(famicom,rom);
		case 1:
			return (Mapper*)new Mapper001(famicom, rom);
		case 2: 
			return (Mapper*)new Mapper002(famicom, rom);
		case 3: 
			return (Mapper*)new Mapper003(famicom, rom);
		case 4: 
			return (Mapper*)new Mapper004(famicom, rom);
		case 7: 
			return (Mapper*)new Mapper007(famicom, rom);
		case 66: 
			return (Mapper*)new Mapper066(famicom, rom);
		case 74:
			return (Mapper*)new Mapper074(famicom, rom);
		case 76:
			return (Mapper*)new Mapper076(famicom, rom);
		case 245:
			return (Mapper*)new Mapper245(famicom, rom);
		default:

			return nullptr;
		}

	}

};
