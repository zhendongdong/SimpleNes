#include "Mapper076.h"
#include <assert.h>
uint8_t Mapper076::ReadViaCPU(uint16_t address) {
    const uint16_t realAddress = address & 0x7FFF;
    return pPRGBlank[realAddress >> 13][realAddress & 0x1ff];
}

void Mapper076::WriteViaCPU(uint16_t address, uint8_t sampleBuffer) {
    if (address == 0x8000) {
        map76_reg = sampleBuffer;
    } else if (address == 0x8001) {
        switch (map76_reg & 0x07) {
        case 0x02:
            //pCHRBlank[0] = FindBlank_2KB(m_Rom->pChrrom, data);
            break;
        case 0x03:
            //pCHRBlank[1] = FindBlank_2KB(m_Rom->pChrrom, data);
            break;
        case 0x04:
            //pCHRBlank[2] = FindBlank_2KB(m_Rom->pChrrom, data);
            break;
        case 0x05:
            //pCHRBlank[3] = FindBlank_2KB(m_Rom->pChrrom, data);
            break;
        case 0x06:
            //pPRGBlank[0] = FindBlank_8KB(m_Rom->pPrgrom, data);
            break;
        case 0x07:
            //pPRGBlank[1] = FindBlank_8KB(m_Rom->pPrgrom, data);
            break;
        }
    }
}

uint8_t Mapper076::ReadViaPPU(uint16_t address) {
    return pCHRBlank[address >> 11][address & 0x7ff];
}

void Mapper076::OnReset() {
    //pCHRBlank[0] = FindBlank_2KB(m_Rom->pChrrom, 0);
    //pCHRBlank[1] = FindBlank_2KB(m_Rom->pChrrom, 1);
    //pCHRBlank[2] = FindBlank_2KB(m_Rom->pChrrom, 2);
    //pCHRBlank[3] = FindBlank_2KB(m_Rom->pChrrom, 3);

    //const uint8_t prgLastBlank = (m_Rom->nPRGBlankCount - 1) * 2;
    //pPRGBlank[0] = FindBlank_8KB(m_Rom->pPrgrom, 0);
    //pPRGBlank[1] = FindBlank_8KB(m_Rom->pPrgrom, 1);
    //pPRGBlank[2] = FindBlank_8KB(m_Rom->pPrgrom, prgLastBlank);
    //pPRGBlank[3] = FindBlank_8KB(m_Rom->pPrgrom, prgLastBlank + 1);
}
