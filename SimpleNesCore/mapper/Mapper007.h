#pragma once
#include "Mapper.h"

/// <summary>
/// 测试游戏：
/// 时间统治者 未通过测试
/// 
/// 备注：
/// </summary>
class Mapper007 :public Mapper
{
public:
    Mapper007(Famicom* fc, famicom_rom_t* rom) ;
    virtual void OnReset();
    virtual void WriteViaCPU(uint16_t address, uint8_t data);
};

