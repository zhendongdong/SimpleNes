#pragma once
#include "Mapper.h"

/// <summary>
/// 测试游戏：
/// 魂斗罗（美版） 
/// 
/// 备注：
/// 此Mapper算是非常简单了
/// </summary>
class Mapper002 : public Mapper {
public:
    Mapper002(Famicom* fc, famicom_rom_t* rom) :Mapper(fc, rom) {

    }
    virtual void WriteViaPPU(uint16_t address, uint8_t data) override;
    virtual void WriteViaCPU(uint16_t address, uint8_t data) override;
    virtual void OnSave(SaveBundle *bundle) override;
    virtual void OnRestore(SaveBundle *bundle) override;
};
