#pragma once
#include "Mapper.h"
#include "../NesCartridge.h"
#include <assert.h>

class Mapper003 : public Mapper {
public:
    Mapper003(Famicom* fc, famicom_rom_t* rom) :Mapper(fc, rom) {

	}

    virtual void WriteViaPPU(uint16_t address, uint8_t data) override;

    virtual void WriteViaCPU(uint16_t address, uint8_t data) override;
};
