#include "Mapper066.h"
#include <assert.h>

void Mapper066::OnReset() {
    FormatBank(BANKSIZE_16KB, BANKSIZE_8KB);
    SetCPUBank_16KB(0, 0);
    SetCPUBank_16KB(1, mROM->prg16kbCount - 1);
    SetPPUBank_8KB(0);
}

void Mapper066::Write6000(uint16_t address, uint8_t data)
{
    //LoadPrgBank_32KB(0,(data & 0xF0) >> 4);
    uint8_t prgbank = (data & 0xF0) >> 4 << 1;
    SetCPUBank_16KB(0, prgbank);
    SetCPUBank_16KB(1, prgbank + 1);
    SetPPUBank_8KB(data & 0x0F);
}

void Mapper066::WriteViaCPU(uint16_t address, uint8_t data) {
    uint8_t prgbank = (data & 0xF0) >> 4 << 1;
    SetCPUBank_16KB(0, prgbank);
    SetCPUBank_16KB(1, prgbank + 1);
    SetPPUBank_8KB(data & 0x0F);
}
