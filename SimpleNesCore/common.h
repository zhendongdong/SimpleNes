#pragma once
#include "framework.h"
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#if defined(WIN32) || defined(WIN64)
#else
#define sprintf_s sprintf
#endif
#if !defined(FALSE)
#define FALSE false
#endif
#if !defined(TRUE)
#define TRUE true
#endif

extern void DebugPrintf(const char* strOutputString, ...);

//typedef struct {
//	int(*open)();
//	int(*close)();
//	void(*write)(char* buf, int size);
//	void(*read)(char* buf, int size);
//}savebundle_t;

