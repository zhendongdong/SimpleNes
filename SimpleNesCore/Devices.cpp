#include "Devices.h"

Devices::Devices(Famicom* famicom):
        fcCPU(new NesCPU(famicom)),
        fcCPUBus(new NesCPUBus(famicom)),
        fcPPU(new NesPPU(famicom)),
        fcCartridge(new NesCartridge(famicom)),
        fcGamePad(new NesGamePad()),
        fcAPU(new NesPAPU(famicom))
{

}

Devices::~Devices()
{
   delete this->fcCPU;
   delete this->fcPPU;
   delete this->fcCPUBus;
   delete this->fcGamePad;
   delete this->fcAPU;
}
