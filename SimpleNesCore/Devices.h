#pragma once
#include "NesCPU.h"
#include "NesCPUBus.h"
#include "NesPPU.h"
#include "NesGamePad.h"
#include "NesCartridge.h"
#include "NesPAPU.h"
class Famicom;
class Devices{
private:
    NesCPU* fcCPU = nullptr;
    NesCPUBus* fcCPUBus = nullptr;
    NesPPU* fcPPU = nullptr;
    NesCartridge* fcCartridge = nullptr;
    NesGamePad* fcGamePad = nullptr;
    NesPAPU* fcAPU = nullptr;
public:
    Devices(Famicom* famicom);
    ~Devices();
    inline NesCPU* cpu()
    {
        return fcCPU;
    }
    inline NesCPUBus* bus()
    {
        return fcCPUBus;
    }
    inline NesPPU* ppu()
    {
        return fcPPU;
    }
    inline NesCartridge* cartridge()
    {
        return fcCartridge;
    }
    inline NesGamePad* pad()
    {
        return fcGamePad;
    }
    inline NesPAPU* apu()
    {
        return fcAPU;
    }
};
