#pragma once
#include "common.h"
#include "Rom.h"
#include <stdio.h>
#include "savebundle.h"
enum TiggleType {
	NMI_PENING = 0x1,
	IRQ_PENING = 0x2,
	IRQ_ONESHOT = 0x4,
	IRQ_DMC_INT = 0x2,
	IRQ_HSYNC_INT = 0x2,
	IRQ_FRAME_INT = 0x2,
};
void DebugPrintf(const char* strOutputString, ...);

class IFcEvent
{
public:
	virtual bool OnSaveBundleOpen(bool issave) = 0;
	virtual bool OnSaveBundleClose() = 0;
	virtual void OnSaveBundleWrite(char* buf, int size) = 0;
	virtual void OnSaveBundleRead(char* buf, int size) = 0;
	virtual void OnAudioWrite(short* buf, int size) = 0;
};
class Devices;
class Famicom
{
private:
//	savebundle_t* savebundle_api = nullptr;

    Devices* mDevices;
    void ExecCPU(const int masterCycle);
    void RenderPattern(uint8_t* mem) const;

public:
	uint32_t mMasterClockCycle = 0; //所使用的时钟总数
	uint32_t mCpuCycleCount = 0;
	uint32_t mLastCpuCycleCount = 0;


	SaveBundle* saveBundle = nullptr;
	IFcEvent* ifcevent = nullptr;

	Famicom();
	~Famicom();
	void UserInput(uint8_t index, uint8_t state) const;
	uint8_t* GetCanvas() const;
	int16_t* GetSamples(uint32_t* size) const;
    bool LoadFcRom(famicom_rom_t* rom);
	void EmulationFrame();

	void Save() const;
	void Restore() const;
	void Reset() const;
//	void SetSavebundle(savebundle_t* save);
	void SetSavebundle(SaveBundle* isave);
	void SetFcEvent(IFcEvent* isave);

    bool CreateFcRom(famicom_rom_t** rom, void* data, size_t size);
    void ReleaseFcRom(famicom_rom_t* rom);
    inline Devices* GetDevices() const
    {
        return mDevices;
    }
};

