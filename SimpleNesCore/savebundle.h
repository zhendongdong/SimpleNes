#pragma once
#include "common.h"
#define MAX_SAVE_BYTES 40960
class Famicom;
class SaveBundle
{
private:
	//uint32_t savepos = 0;
	//uint32_t restorepos = 0;
//	savebundle_t * api = nullptr;
	Famicom* famicom;
public:
	SaveBundle(Famicom* fc);
    virtual ~SaveBundle();
	void Reset();

	
	void ResetRestore();

	void ResetSave();

	/// <summary>
	/// 保存一个字节的数据
	/// 注意：内部实现是一个简单队列，
	/// 遵循先进先出原则
	/// 因此保存与恢复的顺序必须相同
	/// </summary>
	/// <param name="data">要保存的数据</param>
	void SaveByte(uint8_t data);

	/// <summary>
	/// 保存一个字(两个字节)的数据
	/// 注意：内部实现是一个简单队列，
	/// 遵循先进先出原则
	/// 因此保存与恢复的顺序必须相同 
	/// </summary>
	/// <param name="data">要保存的数据</param>
	void SaveWord(uint16_t data);

	/// <summary>
	/// 保存一个整型（四个字节）的数据
	/// 注意：内部实现是一个简单队列，
	/// 遵循先进先出原则
	/// 因此保存与恢复的顺序必须相同
	/// </summary>
	/// <param name="data">要保存的数据</param>
	void SavePtr(uint32_t data);

	/// <summary>
	/// 保存指定字节数量的数据
	/// 注意：内部实现是一个简单队列，
	/// 遵循先进先出原则
	/// 因此保存与恢复的顺序必须相同 
	/// </summary>
	/// <param name="data">要保存的数据指针</param>
	/// <param name="size">要保存的数据字节数</param>
	void SaveBlock(uint8_t* data, size_t size);

	/// <summary>
	/// 恢复一个字节的数据
	/// 注意：内部实现是一个简单队列，
	/// 遵循先进先出原则
	/// 因此保存与恢复的顺序必须相同
	/// </summary>
	/// <returns>返回恢复的数据</returns>
	uint8_t RestoreByte();

	/// <summary>
	/// 恢复一个字(两个字节)的数据
	/// 注意：内部实现是一个简单队列，
	/// 遵循先进先出原则
	/// 因此保存与恢复的顺序必须相同 
	/// </summary>
	/// <returns>返回恢复的数据</returns>
	uint16_t RestoreWord();

	/// <summary>
	/// 恢复一个整型（四个字节）的数据
	/// 注意：内部实现是一个简单队列，
	/// 遵循先进先出原则
	/// 因此保存与恢复的顺序必须相同
	/// </summary>
	/// <returns>返回恢复的数据</returns>
	uint32_t RestorePtr();

	/// <summary>
	/// 恢复指定字节数量的数据
	/// 注意：内部实现是一个简单队列，
	/// 遵循先进先出原则
	/// 因此保存与恢复的顺序必须相同 
	/// </summary>
	/// <param name="data">要恢复的目标指针</param>
	/// <param name="size">要求恢复的字节数</param>
	void RestoreBlock(uint8_t* dst, size_t size);


	virtual bool Open(bool issave);
	virtual bool Close();
	virtual void Write(char* buf, int size);
	virtual void Read(char* buf, int size);
};
