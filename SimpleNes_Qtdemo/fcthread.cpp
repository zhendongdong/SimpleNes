#include "fcthread.h"
#include "qdebug.h"

#include <QAudioFormat>
#include <QAudioOutput>
#include <QFile>
#include <fstream>

#include <QWidget>
#include <sys/time.h>

const union PaletteData {
  struct {
    unsigned char r, g, b, a;
  };
  unsigned int data;
} stdpalette[64] = {{{0x7F, 0x7F, 0x7F, 0xFF}}, {{0x20, 0x00, 0xB0, 0xFF}},
                    {{0x28, 0x00, 0xB8, 0xFF}}, {{0x60, 0x10, 0xA0, 0xFF}},
                    {{0x98, 0x20, 0x78, 0xFF}}, {{0xB0, 0x10, 0x30, 0xFF}},
                    {{0xA0, 0x30, 0x00, 0xFF}}, {{0x78, 0x40, 0x00, 0xFF}},
                    {{0x48, 0x58, 0x00, 0xFF}}, {{0x38, 0x68, 0x00, 0xFF}},
                    {{0x38, 0x6C, 0x00, 0xFF}}, {{0x30, 0x60, 0x40, 0xFF}},
                    {{0x30, 0x50, 0x80, 0xFF}}, {{0x00, 0x00, 0x00, 0xFF}},
                    {{0x00, 0x00, 0x00, 0xFF}}, {{0x00, 0x00, 0x00, 0xFF}},

                    {{0xBC, 0xBC, 0xBC, 0xFF}}, {{0x40, 0x60, 0xF8, 0xFF}},
                    {{0x40, 0x40, 0xFF, 0xFF}}, {{0x90, 0x40, 0xF0, 0xFF}},
                    {{0xD8, 0x40, 0xC0, 0xFF}}, {{0xD8, 0x40, 0x60, 0xFF}},
                    {{0xE0, 0x50, 0x00, 0xFF}}, {{0xC0, 0x70, 0x00, 0xFF}},
                    {{0x88, 0x88, 0x00, 0xFF}}, {{0x50, 0xA0, 0x00, 0xFF}},
                    {{0x48, 0xA8, 0x10, 0xFF}}, {{0x48, 0xA0, 0x68, 0xFF}},
                    {{0x40, 0x90, 0xC0, 0xFF}}, {{0x00, 0x00, 0x00, 0xFF}},
                    {{0x00, 0x00, 0x00, 0xFF}}, {{0x00, 0x00, 0x00, 0xFF}},

                    {{0xFF, 0xFF, 0xFF, 0xFF}}, {{0x60, 0xA0, 0xFF, 0xFF}},
                    {{0x50, 0x80, 0xFF, 0xFF}}, {{0xA0, 0x70, 0xFF, 0xFF}},
                    {{0xF0, 0x60, 0xFF, 0xFF}}, {{0xFF, 0x60, 0xB0, 0xFF}},
                    {{0xFF, 0x78, 0x30, 0xFF}}, {{0xFF, 0xA0, 0x00, 0xFF}},
                    {{0xE8, 0xD0, 0x20, 0xFF}}, {{0x98, 0xE8, 0x00, 0xFF}},
                    {{0x70, 0xF0, 0x40, 0xFF}}, {{0x70, 0xE0, 0x90, 0xFF}},
                    {{0x60, 0xD0, 0xE0, 0xFF}}, {{0x60, 0x60, 0x60, 0xFF}},
                    {{0x00, 0x00, 0x00, 0xFF}}, {{0x00, 0x00, 0x00, 0xFF}},

                    {{0xFF, 0xFF, 0xFF, 0xFF}}, {{0x90, 0xD0, 0xFF, 0xFF}},
                    {{0xA0, 0xB8, 0xFF, 0xFF}}, {{0xC0, 0xB0, 0xFF, 0xFF}},
                    {{0xE0, 0xB0, 0xFF, 0xFF}}, {{0xFF, 0xB8, 0xE8, 0xFF}},
                    {{0xFF, 0xC8, 0xB8, 0xFF}}, {{0xFF, 0xD8, 0xA0, 0xFF}},
                    {{0xFF, 0xF0, 0x90, 0xFF}}, {{0xC8, 0xF0, 0x80, 0xFF}},
                    {{0xA0, 0xF0, 0xA0, 0xFF}}, {{0xA0, 0xFF, 0xC8, 0xFF}},
                    {{0xA0, 0xFF, 0xF0, 0xFF}}, {{0xA0, 0xA0, 0xA0, 0xFF}},
                    {{0x00, 0x00, 0x00, 0xFF}}, {{0x00, 0x00, 0x00, 0xFF}}};
int screenmem[256 * 240] = {0};
FcThread::FcThread(QWidget *main) : uiwindow(main) {
  QAudioFormat audioFormat;

  //设置采样率
  audioFormat.setSampleRate(44100);
  //设置通道数
  audioFormat.setChannelCount(1);
  //设置采样大小，一般为8位或16位
  audioFormat.setSampleSize(16);
  //设置编码方式
  audioFormat.setCodec("audio/pcm");
  //设置字节序
  audioFormat.setByteOrder(QAudioFormat::LittleEndian);
  //设置样本数据类型
  audioFormat.setSampleType(QAudioFormat::SignedInt);

  audio = new QAudioOutput(audioFormat, 0);
  // audio->start(&inputFile);
  streamOut = audio->start();

  this->famicom = new Famicom();
  famicom->SetFcEvent(this);
}

void FcThread::SyncLoadRom(QString strFilename) {
  this->filename.clear();
  this->filename.append(strFilename);
  EVENT_FLAG |= EVENT_RELOADROM;
}

void FcThread::GetFrameBuffer(int32_t *dstbuffer, int size) {
  memcpy(dstbuffer, screenmem, size);
}

void FcThread::Input(uint8_t index, uint8_t state) {
  famicom->UserInput(index, state);
}

inline uint64_t getTimeUs() {
  timeval tv;
  gettimeofday(&tv, NULL);
  return (uint64_t)1000000 * tv.tv_sec + tv.tv_usec;
}

void FcThread::run() {
  double nextFrameStartTime = 0;
  float framecount = 0;

  auto markTime = getTimeUs();
  //准备休眠
  long fp = 1000 * 1000 / 60;
  nextFrameStartTime = markTime;

  while (!stop) {

    if (EVENT_FLAG) {

      if (EVENT_FLAG & EVENT_RELOADROM) {
        EVENT_FLAG &= ~EVENT_RELOADROM;

        QFile file(filename);

        if (!file.open(QFile::ReadOnly)) {
          return;
        }

        int size = file.size();
        char *mem = new char[size];

        int readbytes = file.read(mem, size);
        if (mRom) {
          famicom->ReleaseFcRom(mRom);
        }
        famicom->CreateFcRom(&mRom, mem, readbytes);
        famicom->LoadFcRom(mRom);
        delete[] mem;

        file.close();
      }

      if (EVENT_FLAG & EVENT_SAVE_STATE) {
        EVENT_FLAG &= ~EVENT_SAVE_STATE;
        famicom->Save();
      }

      if (EVENT_FLAG & EVENT_RESTORE_STATE) {
        EVENT_FLAG &= ~EVENT_RESTORE_STATE;
        famicom->Restore();
      }

      if (EVENT_FLAG & EVENT_PAUSE) {
        QThread::msleep(20);
      }

      //更新下一帧的基准时间
      nextFrameStartTime = getTimeUs();
      //基准时间都改了干脆从新计算帧率好了
      framecount = 0;
      markTime = nextFrameStartTime;
      continue;
    }

    famicom->EmulationFrame();
    int size = 256 * 240;
    uint8_t *gameframe = famicom->GetCanvas();
    drawLock.lock();
    for (int i = 0; i < size; i++) {
      PaletteData p = stdpalette[gameframe[i]];
      screenmem[i] = qRgb(p.r, p.g, p.b);
    }
    drawLock.unlock();
    uiwindow->update();
    // nesmain->InvalidateRect(nullptr, false);
    nextFrameStartTime += fp;

    framecount++;

    auto nowtime = getTimeUs();
    auto sleepTime = nextFrameStartTime - nowtime;
    if (sleepTime > 1) {
      QThread::usleep(sleepTime); //少休眠两毫秒~ 这样速度更贴近60Hz貌似
    }

    if (nowtime - markTime > 1000 * 1000) {
      FPS = (double)framecount / (double)((nowtime - markTime) / 1000000.0);
      markTime = nowtime;
      framecount = 0;
      //qDebug("fps:%d\n",FPS);
    }
  }
}

bool FcThread::OnSaveBundleOpen(bool issave) { return false; }

bool FcThread::OnSaveBundleClose() { return false; }

void FcThread::OnSaveBundleWrite(char *buf, int size) {}

void FcThread::OnSaveBundleRead(char *buf, int size) {}

void FcThread::OnAudioWrite(short *buf, int size) {
  //   qDebug()<<"bytesFree"<< audio->bytesFree() << "write bytes "<<size*2 <<
  //   audio->periodSize();
  memcpy(_buffer + _bufferSize, (char *)buf, size * 2);
  _bufferSize += size * 2;
  if (_bufferSize >= audio->periodSize()) {
    streamOut->write(_buffer, audio->periodSize());
    auto r = _bufferSize - audio->periodSize();
    if (r > 0) {
      memcpy(_buffer, _buffer + audio->periodSize(), r);
      _bufferSize = r;
    } else {
      _bufferSize = 0;
    }
  }
}
