#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gameview.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


protected:
    virtual void resizeEvent(QResizeEvent *re) override;
private slots:
    void on_pushButton_clicked();

    void on_actionOpen_Game_triggered();


private:
    GameView *mGameView;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
