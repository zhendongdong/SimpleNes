#include "mainwindow.h"
#include "qdebug.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMutex>
#include <QPainter>
#include <QRgb>

#include <QKeyEvent>

bool loadrom = false;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{

  ui->setupUi(this);

  mGameView = new GameView(this);

//  mGameView->setGeometry(0, ui->menubar->height(), width(),
//                         height() - ui->menubar->height() -
//                             ui->statusbar->height());
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_pushButton_clicked() {}

void MainWindow::on_actionOpen_Game_triggered()
{
  QString filename = QFileDialog::getOpenFileName(this, tr("选择文件"), "",
                                                  tr("Nes Files (*.nes)"));
  qDebug("file:%s\n", filename.toStdString().c_str());
  mGameView->SyncLoadRom(filename);
}

void MainWindow::resizeEvent(QResizeEvent *re)
{
  // ui中 先画一个QLable ， 这里直接用
  auto size = re->size();
  mGameView->setGeometry(0, ui->menubar->height(), size.width(),
                         size.height() - ui->menubar->height() - ui->statusbar->height());
}
