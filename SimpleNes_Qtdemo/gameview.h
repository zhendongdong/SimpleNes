#ifndef GAMEVIEW_H
#define GAMEVIEW_H

#include <QWidget>
#include "fcthread.h"
#include "../SimpleNesCore/Famicom.h"

class GameView : public QWidget
{
    Q_OBJECT
private:
    FcThread *fc;
    int32_t screen[256 * 240] = {0};
    QRgb* pixels = new QRgb[512*480];
    uint64_t markTime = 0;
    int framecount = 0;
    int drawFps = 0;
    const int mKeyMap[8] = {
            // A, B, Select, Start, Up, Down, Left, Right
            Qt::Key_J, Qt::Key_K, Qt::Key_U, Qt::Key_I, Qt::Key_W, Qt::Key_S, Qt::Key_A, Qt::Key_D,
    };
public:
    explicit GameView(QWidget *parent = nullptr);
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;
    virtual void keyReleaseEvent(QKeyEvent *event) override;

    void SyncLoadRom(QString filename);
signals:

};

#endif // GAMEVIEW_H
