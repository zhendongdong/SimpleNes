#ifndef FCTHREAD_H
#define FCTHREAD_H

#include <../../SimpleNesCore/Famicom.h>
#include <QAudioOutput>
#include <QThread>
#include <mutex>

class FcThread:public QThread,public IFcEvent
{
private:
    char _buffer[35280];
    size_t _bufferSize = 0;
public:
    enum
        {
            EVENT_RELOADROM = 0x1,
            EVENT_SAVE_STATE = 0x2,
            EVENT_RESTORE_STATE = 0x4,
            EVENT_PAUSE = 0x8,
        };
    int EVENT_FLAG = 0;
    int FPS = 0;
    bool stop = false;
    QWidget * uiwindow = nullptr;
    QString filename = "";
    std::fstream* savefs = nullptr;
    QAudioOutput *audio;
    QIODevice *streamOut;
    Famicom* famicom = nullptr;
    famicom_rom_t* mRom = nullptr;
    std::mutex drawLock;


    FcThread(QWidget * main);
    void SyncLoadRom(QString strFilename);
    void GetFrameBuffer(int32_t* dstbuffer, int size);
    void Input(uint8_t index, uint8_t state);
    //IFcEvent
    virtual bool OnSaveBundleOpen(bool issave)override;
    virtual bool OnSaveBundleClose()override;
    virtual void OnSaveBundleWrite(char* buf, int size)override;
    virtual void OnSaveBundleRead(char* buf, int size)override;
    virtual void OnAudioWrite(short* buf, int size)override;
    void run() override;
};

#endif // FCTHREAD_H
