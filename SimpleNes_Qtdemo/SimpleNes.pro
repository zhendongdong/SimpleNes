QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../SimpleNesCore/NesCPU.cpp \
    ../SimpleNesCore/NesCPUBus.cpp \
    ../SimpleNesCore/NesCPU_Illegal.cpp \
    ../SimpleNesCore/NesCartridge.cpp \
    ../SimpleNesCore/Famicom.cpp \
    ../SimpleNesCore/NesGamePad.cpp \
    ../SimpleNesCore/NesPAPU.cpp \
    ../SimpleNesCore/NesPPU.cpp \
    ../SimpleNesCore/Devices.cpp \
    ../SimpleNesCore/mapper/Mapper.cpp \
    ../SimpleNesCore/savebundle.cpp \
    ../SimpleNesCore/mapper/Mapper000.cpp \
    ../SimpleNesCore/mapper/Mapper001.cpp \
    ../SimpleNesCore/mapper/Mapper002.cpp \
    ../SimpleNesCore/mapper/Mapper003.cpp \
    ../SimpleNesCore/mapper/Mapper004.cpp \
    ../SimpleNesCore/mapper/Mapper007.cpp \
    ../SimpleNesCore/mapper/Mapper023.cpp \
    ../SimpleNesCore/mapper/Mapper066.cpp \
    ../SimpleNesCore/mapper/Mapper074.cpp \
    ../SimpleNesCore/mapper/Mapper076.cpp \
    ../SimpleNesCore/mapper/Mapper245.cpp \
    fcthread.cpp \
    gameview.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    ../SimpleNesCore/NesCPU.h \
    ../SimpleNesCore/NesCPUBus.h \
    ../SimpleNesCore/NesCartridge.h \
    ../SimpleNesCore/Famicom.h \
    ../SimpleNesCore/NesGamePad.h \
    ../SimpleNesCore/NesPAPU.h \
    ../SimpleNesCore/NesPPU.h \
    ../SimpleNesCore/Rom.h \
    ../SimpleNesCore/common.h \
    ../SimpleNesCore/compatible.h \
    ../SimpleNesCore/Devices.h \
    ../SimpleNesCore/framework.h \
    ../SimpleNesCore/helper.h \
    ../SimpleNesCore/mapper/Mapper.h \
    ../SimpleNesCore/mapper/Mapper000.h \
    ../SimpleNesCore/mapper/Mapper001.h \
    ../SimpleNesCore/mapper/Mapper002.h \
    ../SimpleNesCore/mapper/Mapper003.h \
    ../SimpleNesCore/mapper/Mapper004.h \
    ../SimpleNesCore/mapper/Mapper007.h \
    ../SimpleNesCore/mapper/Mapper023.h \
    ../SimpleNesCore/mapper/Mapper066.h \
    ../SimpleNesCore/mapper/Mapper074.h \
    ../SimpleNesCore/mapper/Mapper076.h \
    ../SimpleNesCore/mapper/Mapper245.h \
    ../SimpleNesCore/mapper/MapperFactory.h \
    ../SimpleNesCore/savebundle.h \
    fcthread.h \
    gameview.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

#INCLUDEPATH += . ../SimpleNesCore/

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

