package cn.dingd.simplenes;

public class SimpleNes {
    static {
        System.loadLibrary("simplenes");
    }

    public SimpleNes()
    {
        this.init();
    }
    private native void init();
    public native void emulationFrame();
    public native byte[] getCanvas();
    public native int validate(byte[] bytes);
    public native boolean loadRom(byte[] bytes);
    public native short[] getSamples();
    public native void userinput(int index, int state);
    public native void reset();
    public native void play();
    public native void pause();
}
