package cn.dingd.simplenes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class GameScreen extends View {
    Bitmap bitmap = Bitmap.createBitmap(256, 240, Bitmap.Config.ARGB_8888);

    private final int[] palette = new int[]{
            Color.rgb(0x7F, 0x7F, 0x7F), Color.rgb(0x20, 0x00, 0xB0), Color.rgb(0x28, 0x00, 0xB8), Color.rgb(0x60, 0x10, 0xA0),
            Color.rgb(0x98, 0x20, 0x78), Color.rgb(0xB0, 0x10, 0x30), Color.rgb(0xA0, 0x30, 0x00), Color.rgb(0x78, 0x40, 0x00),
            Color.rgb(0x48, 0x58, 0x00), Color.rgb(0x38, 0x68, 0x00), Color.rgb(0x38, 0x6C, 0x00), Color.rgb(0x30, 0x60, 0x40),
            Color.rgb(0x30, 0x50, 0x80), Color.rgb(0x00, 0x00, 0x00), Color.rgb(0x00, 0x00, 0x00), Color.rgb(0x00, 0x00, 0x00),

            Color.rgb(0xBC, 0xBC, 0xBC), Color.rgb(0x40, 0x60, 0xF8), Color.rgb(0x40, 0x40, 0xFF), Color.rgb(0x90, 0x40, 0xF0),
            Color.rgb(0xD8, 0x40, 0xC0), Color.rgb(0xD8, 0x40, 0x60), Color.rgb(0xE0, 0x50, 0x00), Color.rgb(0xC0, 0x70, 0x00),
            Color.rgb(0x88, 0x88, 0x00), Color.rgb(0x50, 0xA0, 0x00), Color.rgb(0x48, 0xA8, 0x10), Color.rgb(0x48, 0xA0, 0x68),
            Color.rgb(0x40, 0x90, 0xC0), Color.rgb(0x00, 0x00, 0x00), Color.rgb(0x00, 0x00, 0x00), Color.rgb(0x00, 0x00, 0x00),

            Color.rgb(0xFF, 0xFF, 0xFF), Color.rgb(0x60, 0xA0, 0xFF), Color.rgb(0x50, 0x80, 0xFF), Color.rgb(0xA0, 0x70, 0xFF),
            Color.rgb(0xF0, 0x60, 0xFF), Color.rgb(0xFF, 0x60, 0xB0), Color.rgb(0xFF, 0x78, 0x30), Color.rgb(0xFF, 0xA0, 0x00),
            Color.rgb(0xE8, 0xD0, 0x20), Color.rgb(0x98, 0xE8, 0x00), Color.rgb(0x70, 0xF0, 0x40), Color.rgb(0x70, 0xE0, 0x90),
            Color.rgb(0x60, 0xD0, 0xE0), Color.rgb(0x60, 0x60, 0x60), Color.rgb(0x00, 0x00, 0x00), Color.rgb(0x00, 0x00, 0x00),

            Color.rgb(0xFF, 0xFF, 0xFF), Color.rgb(0x90, 0xD0, 0xFF), Color.rgb(0xA0, 0xB8, 0xFF), Color.rgb(0xC0, 0xB0, 0xFF),
            Color.rgb(0xE0, 0xB0, 0xFF), Color.rgb(0xFF, 0xB8, 0xE8), Color.rgb(0xFF, 0xC8, 0xB8), Color.rgb(0xFF, 0xD8, 0xA0),
            Color.rgb(0xFF, 0xF0, 0x90), Color.rgb(0xC8, 0xF0, 0x80), Color.rgb(0xA0, 0xF0, 0xA0), Color.rgb(0xA0, 0xFF, 0xC8),
            Color.rgb(0xA0, 0xFF, 0xF0), Color.rgb(0xA0, 0xA0, 0xA0), Color.rgb(0x00, 0x00, 0x00), Color.rgb(0x00, 0x00, 0x00)
    };

    Paint mPaint;
    Rect srcRect =  new Rect(8, 8, 248, 232);
    Rect dstRect = new Rect(8, 8, 248, 232);

    public GameScreen(Context context) {
        super(context);
    }

    /**
     *
     * @param context
     * @param attrs
     */
    public GameScreen(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        //获取屏幕的宽带和高度
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        if (widthMode == MeasureSpec.AT_MOST) {
            float scale = heightSize / 224.f;
            widthSize = (int) (240 * scale);
        }

        setMeasuredDimension(widthSize, heightSize);//设置我们View的高度和宽度

    }

    private byte[] bytes = null;

    public void update(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (bytes != null) {
            //Color.rgb();
            for (int i = 0; i < 240; i++) {
                for (int j = 0; j < 256; j++) {
                    bitmap.setPixel(j, i, palette[bytes[i * 256 + j]]);
                }
            }
            dstRect.left = 0;
            dstRect.right = getWidth();
            dstRect.top = 0;
            dstRect.bottom = getHeight();
            canvas.drawBitmap(bitmap,srcRect,dstRect, mPaint);
        }
    }

}
