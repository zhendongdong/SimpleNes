#pragma once
#include "FasWindow.h"
class FMainWindow:public FasWindow
{
public:
	FMainWindow();
	FMainWindow(FasWindow* parent);
protected:
	LRESULT Dispatch(UINT message, WPARAM wParam, LPARAM lParam);
	virtual void OnDestroy();
};

