#include "FasApplication.h"
#include "FasControll.h"
#include <WinUser.h>
#include <windowsx.h>

ATOM FasControll::MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = this->mCsStyle;
	wcex.lpfnWndProc = &FasControll::StaticWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = nullptr;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = this->mMenuName;
	wcex.lpszClassName = this->mWindowsClass;
	wcex.hIconSm = mICON;

	return RegisterClassExW(&wcex);
}

LRESULT FasControll::StaticWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	FasControll* win = NULL;

	if (message == WM_NCCREATE)
	{
		LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
		win = static_cast<FasControll*>(lpcs->lpCreateParams);
		win->mWnd = hWnd;
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LPARAM>(win));
	}
	else {
		win = reinterpret_cast<FasControll*>(::GetWindowLongPtr(hWnd, GWLP_USERDATA));
	}
	if (win != NULL) {
		return win->Dispatch(message, wParam, lParam);
	}
	return ::DefWindowProc(hWnd, message, wParam, lParam);
}

LRESULT FasControll::DefWndProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		GetClientRect(mWnd, &mClientRect);
		this->OnCreate();
		break;
	case WM_PAINT:
	{
		if (!this->mUserPaint) {
			return ::DefWindowProc(mWnd, message, wParam, lParam);
		}
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(mWnd, &ps);
		PaintEvent paintEvent;
		paintEvent.hdc = hdc;
		this->OnPaint(&paintEvent);
		EndPaint(mWnd, &ps);
		//DeleteObject();
	}
	break;
	case WM_KEYDOWN://按键按下
		this->OnKeyDown(wParam);
		break;
	case WM_RBUTTONDOWN: {
		MouseEvent e;
		e.button = MouseButton_Right;
		this->OnMouseBtnDwon(&e);
	}break;
	case WM_LBUTTONDOWN: {
		MouseEvent e;
		e.button = MouseButton_Left;
		this->OnMouseBtnDwon(&e);
		break;
	}
	case WM_RBUTTONUP: {
		MouseEvent e;
		e.button = MouseButton_Right;
		this->OnMouseBtnUp(&e);
		
	}break; 
	case WM_LBUTTONUP: {
		MouseEvent e;
		e.button = MouseButton_Left;
		this->OnMouseBtnUp(&e);
		this->OnClick();
	}break;
	case WM_MOUSEMOVE:
		this->OnMouseMove(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		break;
	case WM_KEYUP://按键松开
		this->OnKeyUp(wParam);
		break;
	case WM_SIZE:
		GetClientRect(mWnd, &mClientRect);
		this->mWidth = LOWORD(lParam);
		this->mHeight = HIWORD(lParam);
		this->OnResize();
		break;
	case WM_DESTROY:
		this->OnDestroy();
		break;
	default:
		return DefWindowProc(mWnd, message, wParam, lParam);
	}
	return DefWindowProc(mWnd, message, wParam, lParam);;
}

LRESULT FasControll::Dispatch(UINT message, WPARAM wParam, LPARAM lParam)
{
	return this->DefWndProc(message, wParam, lParam);
}

FasControll::FasControll(FasControll* parnet) :mParent(parnet)
{
	this->mInstance = FasApplication::mAppInstance;
}
void FasControll::SetSize(int w, int h)
{
	this->mWidth = w;
	this->mHeight = h;
}
void FasControll::InvalidateRect(const RECT* lpRect,
	BOOL bErase) {
	::InvalidateRect(mWnd, lpRect, bErase);
	//::UpdateWindow
}
void FasControll::SetPosition(int x, int y)
{
	this->mPosX = x;
	this->mPosY = y;
}
void FasControll::SetMenu(LONG resId) {
	if (!resId)
		this->mMenuName = nullptr;
	else
		this->mMenuName = MAKEINTRESOURCEW(resId);
}
void FasControll::SetIcon(LONG resId) {
	if (!resId)
		this->mICON = nullptr;
	else
		this->mICON = LoadIcon(mInstance, MAKEINTRESOURCE(resId));
}
void FasControll::Show(int x, int y)
{
	if (mWnd == nullptr) {
		ATOM regclass = MyRegisterClass(mInstance);
		mWnd = CreateWindowW(mWindowsClass, mTitle, mStyle,
			x, y, mWidth, mHeight,
			mParent != nullptr ? mParent->mWnd : nullptr, nullptr, mInstance, this);
		if (!mWnd)
		{
			return;
		}
	}

}

void FasControll::Show() {
	Show(CW_USEDEFAULT, 0);
}