#pragma once
#include "framework.h"
#include "FamicomThread.h"
#include <Mmsystem.h>
#include "SD2D.h"
#include "FMainWindow.h"

class MainWindow :public FMainWindow {
private:

	const unsigned mKeyMap[16] = {
		// A, B, Select, Start, Up, Down, Left, Right
		'J', 'K', 'U', 'I', 'W', 'S', 'A', 'D',
		// A, B, Select, Start, Up, Down, Left, Right
		VK_NUMPAD2, VK_NUMPAD3, VK_NUMPAD5, VK_NUMPAD6,
		VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT,
	};
	BOOL recreate = false;
	
	int32_t mFrameMem[256 * 240] = { 0 };

	BITMAPINFO pbmi;

	FamicomThread* famicomThread = nullptr;

	//����
	MMRESULT timerId = 0;
	bool mMouseHide = false;
	bool mMouseTrack = false;
	int mMouseHoverPoint_x = 0;
	int mMouseHoverPoint_y = 0;
	static void Disassembly(char* line);

	
protected:

	void OnCreate()override;
	void CalcSize();
	void OnPaint(PaintEvent* paintEvent)override;
	void Render(int* gameframe);
	void TarckMouse();
	virtual void OnMouseMove(int x, int y)override;
	bool OnKeyUp(unsigned int keyCode)override;
	bool OnKeyDown(unsigned int keyCode)override;
	void OnResize()override;
	void OnDestroy()override;
	LRESULT Dispatch(UINT message, WPARAM wParam, LPARAM lParam)override;
	bool OnCommand(WPARAM wParam, LPARAM lParam);
public:
	SD2D* d2d;

	int mCurstHideTimer = 360;
	bool mActivited = false;
	MainWindow(FasWindow* parent);
	~MainWindow();
};