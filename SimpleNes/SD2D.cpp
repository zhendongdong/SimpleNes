#include "SD2D.h"
#include "framework.h"
#include <exception>

SD2D::SD2D(HWND hd) :mWnd(hd)
{
	DX::ThrowIfFailed(D2D1CreateFactory(
		D2D1_FACTORY_TYPE_SINGLE_THREADED,
		//D2D1_FACTORY_TYPE_MULTI_THREADED,
		IID_ID2D1Factory1,
		reinterpret_cast<void**>(&this->factory)
	));
	DX::ThrowIfFailed(DWriteCreateFactory(
		DWRITE_FACTORY_TYPE_SHARED,
		__uuidof(IDWriteFactory),
		reinterpret_cast<IUnknown**>(&writeFactory)
	));


	DX::ThrowIfFailed(writeFactory->CreateTextFormat(
		L" Microsoft YaHei",                   // Font family name
		NULL,                          // Font collection(NULL sets it to the system font collection)
		DWRITE_FONT_WEIGHT_REGULAR,    // Weight
		DWRITE_FONT_STYLE_NORMAL,      // Style
		DWRITE_FONT_STRETCH_NORMAL,    // Stretch
		20.0f,                         // Size    
		L"en-us",                      // Local
		&textFormat                 // Pointer to recieve the created object
	));
	HRESULT hr;
	UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT | D3D11_CREATE_DEVICE_DEBUG;
	D3D_FEATURE_LEVEL featureLevels[] = {
			D3D_FEATURE_LEVEL_11_1,
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
			D3D_FEATURE_LEVEL_9_3,
			D3D_FEATURE_LEVEL_9_2,
			D3D_FEATURE_LEVEL_9_1
	};
	DXGI_SWAP_CHAIN_DESC sd = { 0 };

	//	RECT rc;
	//	GetClientRect(mWnd, &rc);
	sd.BufferCount = 2;
	sd.BufferDesc.Width = 0;
	sd.BufferDesc.Height = 0;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	//sd.BufferDesc.RefreshRate.Numerator = 60;
	//sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = mWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	DX::ThrowIfFailed(::D3D11CreateDeviceAndSwapChain(
		nullptr,                    // specify null to use the default adapter
		D3D_DRIVER_TYPE_HARDWARE,
		0,
		creationFlags,              // optionally set debug and Direct2D compatibility flags
		featureLevels,              // list of feature levels this app can support
		ARRAYSIZE(featureLevels),   // number of possible feature levels
		D3D11_SDK_VERSION,
		&sd,
		&this->swapChain,
		&this->d3dDevice,                    // returns the Direct3D device created
		nullptr,            // returns feature level of device created
		&this->d3dContext                    // returns the device immediate context
	));

	//查询dxgi设备接口
	DX::ThrowIfFailed(this->d3dDevice->QueryInterface(
		IID_IDXGIDevice1,
		reinterpret_cast<void**>(&this->dxgidevice)
	));

	//创建D2D设备
	DX::ThrowIfFailed(this->factory->CreateDevice(
		this->dxgidevice,
		&this->device
	));

}

SD2D::~SD2D() {
	
	this->Release();
	::SafeRelease(this->device);
	::SafeRelease(this->dxgidevice);

	::SafeRelease(this->writeFactory);
	::SafeRelease(this->factory);
	::SafeRelease(this->swapChain);
	::SafeRelease(this->d3dDevice);
	::SafeRelease(this->d3dContext);

}
void SD2D::Release()
{

	::SafeRelease(this->primaryBrush);
	::SafeRelease(this->buffer);
	::SafeRelease(this->target);
	::SafeRelease(this->dxgiSurface);
	::SafeRelease(this->context);

	//::SafeRelease(this->swapChain);
	//::SafeRelease(this->d3dDevice);
	//::SafeRelease(this->d3dContext);
}
void SD2D::ReCreate(int width, int height)
{
	this->Release();
	swapChain->ResizeBuffers(2
		, width
		, height
		, DXGI_FORMAT_R8G8B8A8_UNORM
		, 0L);
	this->Create(width, height);
}
void SD2D::Create(int width, int height)
{
	this->targetWidth = width;
	this->targetHeight = height;

	DX::ThrowIfFailed(swapChain->GetBuffer(
		0
		, IID_IDXGISurface
		, (void**)&this->dxgiSurface));

	//创建D2D上下文
	DX::ThrowIfFailed(this->device->CreateDeviceContext(
		D2D1_DEVICE_CONTEXT_OPTIONS_NONE,
		&this->context
	));
	
	D2D1_BITMAP_PROPERTIES1 properties = D2D1::BitmapProperties1(
		D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
		D2D1::PixelFormat(DXGI_FORMAT_R8G8B8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED)
	);

	DX::ThrowIfFailed(this->context->CreateBitmapFromDxgiSurface(
		this->dxgiSurface,
		&properties,
		&this->target
	));
	this->context->SetUnitMode(D2D1_UNIT_MODE_PIXELS);
	this->context->SetTarget(this->target);


	D2D1_BITMAP_PROPERTIES1 bitmap_properties2 = D2D1::BitmapProperties1(
		D2D1_BITMAP_OPTIONS_NONE,
		D2D1::PixelFormat(DXGI_FORMAT_R8G8B8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED)
	);
	DX::ThrowIfFailed(this->context->CreateBitmap(
		D2D1_SIZE_U{ 256, 240 },
		nullptr,
		0,
		&bitmap_properties2,
		&this->buffer
	));

	
	this->context->CreateSolidColorBrush(
		D2D1::ColorF(D2D1::ColorF(0.93f, 0.94f, 0.96f, 1.0f)),
		&primaryBrush
	);
}

