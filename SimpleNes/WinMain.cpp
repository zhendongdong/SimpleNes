﻿// nvs.cpp : 定义应用程序的入口点。
//

#include "framework.h"
#include "WinMain.h"
#include <iostream>
#include <cassert>
#include <sys/timeb.h>
#include "FasApplication.h"
#include "MainWindow.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow) {
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	FasApplication app(hInstance);
	MainWindow *w = new MainWindow(nullptr);
	w->Show();
	return app.Loop();
}
