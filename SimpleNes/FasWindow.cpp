#include "FasApplication.h"
#include "FasWindow.h"
FasWindow::FasWindow() :FasWindow(nullptr)
{
}

FasWindow::FasWindow(FasWindow* parent) : FasControll(parent)
{
}


LRESULT FasWindow::Dispatch(UINT message, WPARAM wParam, LPARAM lParam)
{
	return FasControll::DefWndProc(message, wParam, lParam);
}


void FasWindow::SetClientSize(int width, int height)
{
	RECT rc;
	SetRect(&rc, 0, 0, width, height);
	AdjustWindowRect(&rc, mStyle, !(mMenuName == nullptr));
	this->mWidth = rc.right - rc.left;
	this->mHeight = rc.bottom - rc.top;
}

void FasWindow::Hide() {
	if (mWnd != nullptr)
		ShowWindow(mWnd, SW_HIDE);
}

void  FasWindow::Send(UINT Msg,
	WPARAM wParam,
	LPARAM lParam)
{
	::SendMessage(mWnd, Msg, wParam, lParam);
}


