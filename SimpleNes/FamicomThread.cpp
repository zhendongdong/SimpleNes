#include "FamicomThread.h"

#include "MainWindow.h"
#pragma comment(lib, "Winmm.lib")
#include "../SimpleNesCore/Famicom.h"
#include "palette.h"

int screenmem[256 * 240];

FamicomThread::FamicomThread(MainWindow* nm) :nesmain(nm)
{

	xa2 = new XAudio2();
	xa2->Initialize();
	xa2->Play();

	famicom = new Famicom();
	famicom->SetFcEvent(this);
}

FamicomThread::~FamicomThread()
{
	xa2->Release();
	delete xa2;
	delete famicom;
}


void FamicomThread::GetFrameBuffer(int32_t* dstbuffer, int size) {
	//this->WaitSyncLock();
	memcpy(dstbuffer, screenmem, size);
	//this->ReleaseSyncLock();
}

void FamicomThread::Input(UINT8 index, UINT8 state)
{
	if(famicom != nullptr)
		famicom->UserInput(index, state);
}

void FamicomThread::WaitSyncLock()
{
	WaitForSingleObject(mSyncLock, INFINITE);
}

void FamicomThread::ReleaseSyncLock()
{
	ReleaseMutex(mSyncLock);
}

void FamicomThread::Play()
{
	EVENT_FLAG &= ~EVENT_PAUSE;
}

void FamicomThread::Pause()
{
	EVENT_FLAG |= EVENT_PAUSE;
}
void FamicomThread::Save()
{
	EVENT_FLAG |= EVENT_SAVE_STATE;
}

void FamicomThread::Restore()
{
	EVENT_FLAG |= EVENT_RESTORE_STATE;
}

void FamicomThread::SyncLoadRom(WCHAR* filename) {
	wcscpy_s(this->filename, filename);
	EVENT_FLAG |= EVENT_RELOADROM;
}

double nextFrameStartTime = 0;
long markTime = 0;
float framecount = 0;

int FamicomThread::Run()
{

	markTime = ::timeGetTime();
	//准备休眠
	static double fp = 1000.f / 60.f;

	nextFrameStartTime = markTime;
	famicom_rom_t* rom = nullptr;
	while (!RequestedStop())
	{

		//if (::timeGetTime() < nextFrameStartTime) 
		//{
		//	Sleep(0);
		//	continue;
		//}

		if (EVENT_FLAG)
		{

			if (EVENT_FLAG & EVENT_RELOADROM)
			{
				EVENT_FLAG &= ~EVENT_RELOADROM;
				int ret;
				std::fstream romfile;
				romfile.open(this->filename, std::ios::in | std::ios::binary);
				romfile.seekg(0, romfile.end);
				int len = romfile.tellg();
				romfile.seekg(0, romfile.beg);
				char* mem = new char[len];
				romfile.read(mem, len);
				if (romfile.gcount() != len)
				{
					ret = -100;
				}
				if (rom != nullptr) {
					famicom->ReleaseFcRom(rom);
					rom = nullptr;
					//delete famicom;
					//famicom = new Famicom();
					//famicom->SetFcEvent(this);
				}
				famicom->CreateFcRom(&rom, mem, romfile.gcount());
				famicom->LoadFcRom(rom);
				delete[] mem;
			}

			if (EVENT_FLAG & EVENT_SAVE_STATE)
			{
				EVENT_FLAG &= ~EVENT_SAVE_STATE;
				famicom->Save();
			}

			if (EVENT_FLAG & EVENT_RESTORE_STATE)
			{
				EVENT_FLAG &= ~EVENT_RESTORE_STATE;
				famicom->Restore();
			}

			if (EVENT_FLAG & EVENT_PAUSE) {
				::Sleep(20);//游戏暂停 休眠20毫秒
			}

			//更新下一帧的基准时间
			nextFrameStartTime = ::timeGetTime();
			//基准时间都改了干脆从新计算帧率好了
			framecount = 0;
			markTime = nextFrameStartTime;
			continue;
		}

		famicom->EmulationFrame();
		int size = 256 * 240;
		uint8_t* gameframe = famicom->GetCanvas();
		for (int i = 0; i < size; i++)
		{
			screenmem[i] = stdpalette[gameframe[i]].data;
		}
		nesmain->InvalidateRect(nullptr, false);
		nextFrameStartTime += fp;

		framecount++;
#if 1
		long sleepTime = nextFrameStartTime - ::timeGetTime();
		if (sleepTime > 1)
		{
			::Sleep(sleepTime - 2);//少休眠两毫秒~ 这样速度更贴近60Hz貌似
		}
		else {
			::Sleep(0);
		}
#endif
		if (framecount == 60)
		{
			long elapsedTime = ::timeGetTime();
			FPS = 1000.f / (float)((float)(elapsedTime - markTime) / framecount);
			markTime = elapsedTime;
			framecount = 0;
		}

	}
	if (rom != nullptr) {
		famicom->ReleaseFcRom(rom);
		rom = nullptr;
	}
	return 0;
}

bool FamicomThread::OnSaveBundleOpen(bool issave)
{
	if (this->filename == nullptr)
		return false;
	WCHAR buffer[1024];
	wsprintf(buffer, _T("%s.save"), this->filename);
	savefs = new std::fstream();
	if (issave)
		savefs->open(buffer, std::ios::out | std::ios::binary | std::ios::trunc);
	else {
		savefs->open(buffer, std::ios::in | std::ios::binary | std::ios::_Nocreate);
	}
	return savefs->is_open();
}

bool FamicomThread::OnSaveBundleClose()
{
	if (savefs != nullptr) {
		savefs->close();
		delete savefs;
		savefs = nullptr;
	}
	return true;
}

void FamicomThread::OnSaveBundleWrite(char* buf, int size)
{
	savefs->write(buf, size);
}

void FamicomThread::OnSaveBundleRead(char* buf, int size)
{
	savefs->read(buf, size);
}

void FamicomThread::OnAudioWrite(short* buf, int size)
{
	const UINT8 queued = xa2->BuffersQueued();
	if (queued <= 15)
		xa2->SubmitSample(buf, size);
}
