#pragma once
#include "framework.h"
class FasApplication
{
private:
	ULONG_PTR mGdiplusToken;

public:
	static HINSTANCE mAppInstance;
public:
	FasApplication(HINSTANCE instance);
	~FasApplication();
	int Loop();
};

