#include "framework.h"
#include "SThread.h"
#include <process.h>


unsigned int __stdcall SThread::__Simple__ThreadCallback(void* pParam)
{
    SThread* st = (SThread*)pParam;
    int code = 0;
    if (st != nullptr) {
        code = st->Run();
    }
    return code;
}

HANDLE SThread::GetHandle()
{
    return __Simple__hThread;
}

int SThread::Run()
{
    return 0;
}

void SThread::Start()
{
    __Simple__hThread = (HANDLE)::_beginthreadex(NULL, 0, &SThread::__Simple__ThreadCallback, (LPVOID)this, 0, nullptr);
    if (__Simple__hThread == NULL)
        return;
}

BOOL SThread::RequestedStop()
{
    return __Simple__stop;
}

/// <summary>
/// 尝试终止线程，调用此方法并不会强制终止线程
/// 而需要在线程中调用RequestedStop方法判断是否
/// 已经要求停止后主动退出
/// </summary>
void SThread::TryStop()
{
    __Simple__stop = true;
}

void SThread::Wait()
{
    ::WaitForSingleObject(__Simple__hThread, INFINITE);//等待线程退出
}

SThread::~SThread()
{
    ::CloseHandle(__Simple__hThread);
}
