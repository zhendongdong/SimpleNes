#pragma once
#include <windows.h>


class FasControll
{
private:
	
public:
	HWND mWnd;
	HINSTANCE mInstance = nullptr;
	RECT mClientRect = { 0 };
protected:

	typedef struct {
		HDC hdc;
	}PaintEvent;

	enum MouseButton{
		MouseButton_Left,
		MouseButton_Right,
		MouseButton_Center
	};
	typedef struct {
		MouseButton button;
	}MouseEvent;

	HICON mICON = nullptr;
	LPCWSTR mMenuName = nullptr;
	bool mUserPaint = false;
	int mWidth = CW_USEDEFAULT;
	int mHeight = 0;
	int mPosX = 0;
	int mPosY = 0;
	LONG mStyle = WS_OVERLAPPEDWINDOW;
	LONG mCsStyle = CS_HREDRAW | CS_VREDRAW;
	
	WCHAR mTitle[100] = _T("测试窗口");                  // 标题栏文本
	WCHAR mWindowsClass[100] = _T("WinDebug");            // 主窗口类名
	FasControll* mParent = nullptr;
	ATOM MyRegisterClass(HINSTANCE hInstance);
	
public:
	FasControll(FasControll *parnet);
	static LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	virtual LRESULT DefWndProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual LRESULT Dispatch(UINT message, WPARAM wParam, LPARAM lParam);
	void SetSize(int w, int h);
	void InvalidateRect(const RECT* lpRect, BOOL bErase);
	void SetPosition(int x, int y);
	void SetMenu(LONG resId);
	void SetIcon(LONG resId);
	virtual void Show(int x, int y);
	virtual void Show();
protected:
	virtual void OnCreate() {

	}
	virtual void OnPaint(PaintEvent* paintEvent) {

	}
	virtual void OnMouseMove(int x, int y) {

	}
	virtual void OnMouseBtnUp(MouseEvent * event) {

	}
	virtual void OnMouseBtnDwon(MouseEvent* event) {

	}
	virtual void OnClick() {

	}
	virtual void OnResize() {

	}
	virtual bool OnKeyUp(unsigned int keyCode) {
		return true;
	};
	virtual bool OnKeyDown(unsigned int keyCode) {
		return true;
	}
	virtual void OnDestroy() {

	}
};

