#pragma once
#include "framework.h"
class SThread
{
private:
	HANDLE __Simple__hThread;
	BOOL __Simple__stop = false;
	static unsigned int __stdcall __Simple__ThreadCallback(void* pParam);

public:
	virtual int Run();
	HANDLE GetHandle();
	void Start();
	BOOL RequestedStop();
	void TryStop();
	void Wait();
	virtual ~SThread();
};

