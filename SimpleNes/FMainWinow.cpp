#include "FasWindow.h"
#include "FMainWindow.h"

FMainWindow::FMainWindow() :FasWindow()
{
}
FMainWindow::FMainWindow(FasWindow* parent):FasWindow(parent)
{
}

LRESULT FMainWindow::Dispatch(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY) 
	{
		this->OnDestroy();
		::PostQuitMessage(0);
		return 0;
	}
	else {
		return FasWindow::Dispatch(message, wParam, lParam);
	}
}
void FMainWindow::OnDestroy()
{
	
}
