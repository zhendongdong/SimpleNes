#pragma once
#include <XAudio2.h>
#ifdef _XBOX //Big-Endian
#define fourccRIFF 'RIFF'
#define fourccDATA 'data'
#define fourccFMT 'fmt '
#define fourccWAVE 'WAVE'
#define fourccXWMA 'XWMA'
#define fourccDPDS 'dpds'
#endif

#ifndef _XBOX //Little-Endian
#define fourccRIFF 'FFIR'
#define fourccDATA 'atad'
#define fourccFMT ' tmf'
#define fourccWAVE 'EVAW'
#define fourccXWMA 'AMWX'
#define fourccDPDS 'sdpd'
#endif
class XAudio2 {

private:
	IXAudio2* mXAudio2 = nullptr;
	IXAudio2MasteringVoice* mMasterVoice = nullptr;
	IXAudio2SourceVoice* mSourceVoice = nullptr;

	HRESULT FindChunk(HANDLE hFile, DWORD fourcc, DWORD& dwChunkSize, DWORD& dwChunkDataPosition);
	HRESULT ReadChunkData(HANDLE hFile, void* buffer, DWORD buffersize, DWORD bufferoffset);

	template<class Interface>
	inline void SafeRelease(Interface*& pInterfaceToRelease);

	HRESULT CreateSource();
	
	HRESULT Create();
public:
	void Release() noexcept;
	HRESULT Initialize();
	HRESULT Play();
	UINT32 BuffersQueued();
	HRESULT SubmitSample(SHORT* buffer, UINT32 len);
};