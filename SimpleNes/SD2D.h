#pragma once
#include <d3d11.h>
#include <d2d1.h>
#include <d2d1_1.h>
#include <dwrite.h>
#include <d2d1helper.h>
#include <exception>
#pragma comment (lib,"d2d1.lib")
#pragma comment (lib,"dwrite.lib")
#pragma comment (lib,"d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "dxguid.lib")
namespace DX
{
	inline void ThrowIfFailed(HRESULT hr)
	{
		if (FAILED(hr))
		{
			// Set a breakpoint on this line to catch DirectX API errors
			throw std::exception();
		}
	}
}
class SD2D
{
private:
	HWND mWnd;
	void Release();

public:
	int targetHeight = 0;
	int targetWidth = 0;
	ID3D11Device* d3dDevice = nullptr;
	ID3D11DeviceContext* d3dContext = nullptr;
	IDXGISwapChain* swapChain = nullptr;

	ID2D1Factory1* factory = nullptr;
	IDWriteFactory* writeFactory = nullptr;
	IDWriteTextFormat* textFormat = nullptr;

	ID2D1Device* device = nullptr;
	ID2D1DeviceContext* context = nullptr;
	IDXGIDevice1* dxgidevice = nullptr;
	ID2D1Bitmap1* target = nullptr;
	ID2D1Bitmap1* buffer = nullptr;
	IDXGISurface* dxgiSurface = nullptr;

	ID2D1SolidColorBrush* primaryBrush = nullptr;

	SD2D(HWND hd);
	~SD2D();
	void ReCreate(int width, int height);
	void Create(int width, int height);

	inline void BeginDraw()
	{
		this->context->BeginDraw();
	}
	inline void EndDraw()
	{
		this->context->EndDraw();
	}
};

