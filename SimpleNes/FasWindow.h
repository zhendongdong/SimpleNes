#pragma once
#include "windowsx.h"
#include "framework.h"
#include "FasControll.h"
#include <cassert>
#include <WinUser.h>
#include <wtypes.h>
#include <tchar.h>
class FasWindow:public FasControll
{

public:

	FasWindow();
	FasWindow(FasWindow *parent);
	virtual LRESULT Dispatch(UINT message, WPARAM wParam, LPARAM lParam) override;

	void SetClientSize(int width, int height);
	virtual void Hide();
	void Send(UINT Msg,
		WPARAM wParam,
		LPARAM lParam);
	virtual void Show(int x, int y)override {
		FasControll::Show(x, y);
		ShowWindow(mWnd, SW_SHOWNORMAL);
		UpdateWindow(mWnd);
	}
	virtual void Show()override {
		this->Show(CW_USEDEFAULT, 0);
	}
protected:


	virtual void OnCreate() {

	}
	virtual void OnPaint(PaintEvent* paintEvent) {

	}
	
	virtual void OnMouseMove(int x, int y) {

	}
	virtual void OnResize() {

	}
	virtual bool OnKeyUp(unsigned int keyCode) {
		return true;
	};
	virtual bool OnKeyDown(unsigned int keyCode) {
		return true;
	}
	virtual void OnDestroy() {

	}
};

